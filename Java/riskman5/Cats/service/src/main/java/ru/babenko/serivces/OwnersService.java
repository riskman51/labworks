package ru.babenko.serivces;

import ru.babenko.dtos.owners.FullOwnerDto;
import ru.babenko.dtos.owners.InitialOwnerDto;

import java.util.List;

public interface OwnersService {
    FullOwnerDto findOwnerById(Long id);

    List<FullOwnerDto> findAllOwners();

    FullOwnerDto createOwner(InitialOwnerDto owner);

    void deleteOwner(Long ownerId);

    void addOwnersCat(Long ownerId, Long catId);

    void removeOwnersCat(Long ownerId, Long catId);
}
