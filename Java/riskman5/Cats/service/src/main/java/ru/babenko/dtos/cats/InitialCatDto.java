package ru.babenko.dtos.cats;

import ru.babenko.models.Color;

public record InitialCatDto(String breed, Color color) implements CatDto { }
