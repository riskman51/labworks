package ru.babenko.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.babenko.dtos.owners.FullOwnerDto;
import ru.babenko.models.Cat;
import ru.babenko.models.Owner;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface OwnerMapper {
    @Mapping(source = "cats", target = "catsIds")
    @Mapping(source = "birthDate", target = "dateOfBirth")
    FullOwnerDto ownerToFullOwnerDto(Owner owner);

    List<FullOwnerDto> ownersToFullOwnerDtos(List<Owner> owners);

    default Long catToId(Cat cat) {
        return cat == null ? null : cat.getId();
    }

    default List<Long> catsToIds(List<Cat> cats) {
        if (cats == null) {
            return null;
        }
        return cats.stream()
                .map(this::catToId)
                .collect(Collectors.toList());
    }
}