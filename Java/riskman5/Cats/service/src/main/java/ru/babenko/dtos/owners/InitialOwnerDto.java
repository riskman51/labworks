package ru.babenko.dtos.owners;

import java.time.LocalDate;

public record InitialOwnerDto(String name, String surname, LocalDate dateOfBirth) implements OwnerDto { }