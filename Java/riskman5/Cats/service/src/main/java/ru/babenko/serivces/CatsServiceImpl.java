package ru.babenko.serivces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.babenko.dtos.cats.FullCatDto;
import ru.babenko.dtos.cats.InitialCatDto;
import ru.babenko.mappers.CatMapper;
import ru.babenko.dao.CatsDao;
import ru.babenko.models.Cat;
import ru.babenko.models.Color;

import java.util.List;
import java.util.Optional;

@Service
public class CatsServiceImpl implements CatsService {
    private final CatMapper catMapper;
    private final CatsDao catsDao;

    @Autowired
    public CatsServiceImpl(CatMapper catMapper, CatsDao catsDao) {
        this.catMapper = catMapper;
        this.catsDao = catsDao;
    }

    public FullCatDto findCatById(Long id) {
        Optional<Cat> optionalCat = catsDao.findById(id);
        return optionalCat.map(catMapper::catToFullCatDto).orElse(null);
    }

    @Override
    public List<FullCatDto> findAllCats() {
        return catMapper.catsToFullCatDtos(catsDao.findAll());
    }

    @Override
    public FullCatDto createCat(InitialCatDto cat) {
        Cat newCat = Cat.builder()
                .breed(cat.breed())
                .color(cat.color())
                .friends(List.of())
                .build();
        catsDao.save(newCat);
        return catMapper.catToFullCatDto(newCat);
    }

    @Override
    public void friendCats(Long firstCatId, Long secondCatId) {
        Optional<Cat> optionalFirstCat = catsDao.findById(firstCatId);
        Optional<Cat> optionalSecondCat = catsDao.findById(secondCatId);
        if (optionalFirstCat.isEmpty() || optionalSecondCat.isEmpty()) {
            throw new IllegalArgumentException("One of the cats does not exist");
        }
        Cat firstCat = optionalFirstCat.get();
        Cat secondCat = optionalSecondCat.get();
        firstCat.getFriends().add(secondCat);
        secondCat.getFriends().add(firstCat);
        catsDao.save(firstCat);
        catsDao.save(secondCat);
    }

    @Override
    public void unfriendCats(Long firstCatId, Long secondCatId) {
        Optional<Cat> optionalFirstCat = catsDao.findById(firstCatId);
        Optional<Cat> optionalSecondCat = catsDao.findById(secondCatId);
        if (optionalFirstCat.isEmpty() || optionalSecondCat.isEmpty()) {
            throw new IllegalArgumentException("One of the cats does not exist");
        }
        Cat firstCat = optionalFirstCat.get();
        Cat secondCat = optionalSecondCat.get();
        firstCat.getFriends().remove(secondCat);
        secondCat.getFriends().remove(firstCat);
        catsDao.save(firstCat);
        catsDao.save(secondCat);
    }

    @Override
    public void deleteCat(Long catId) {
        catsDao.deleteById(catId);
    }

    @Override
    public List<FullCatDto> findCatsByColor(Color color) {
        return catsDao.findByColor(color).stream().map(catMapper::catToFullCatDto).toList();
    }

    @Override
    public List<FullCatDto> findCatsByBreed(String breed) {
        return catsDao.findByBreed(breed).stream().map(catMapper::catToFullCatDto).toList();
    }
}
