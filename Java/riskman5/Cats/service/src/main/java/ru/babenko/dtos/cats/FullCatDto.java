package ru.babenko.dtos.cats;

import ru.babenko.models.Color;

import java.util.List;

public record FullCatDto(Long id, String breed, Color color, Long ownerId, List<Long> friendsIds) implements CatDto { }