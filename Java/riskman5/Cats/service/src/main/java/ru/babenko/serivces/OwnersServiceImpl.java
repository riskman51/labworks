package ru.babenko.serivces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.babenko.dtos.owners.FullOwnerDto;
import ru.babenko.dtos.owners.InitialOwnerDto;
import ru.babenko.mappers.OwnerMapper;
import ru.babenko.dao.CatsDao;
import ru.babenko.dao.OwnersDao;
import ru.babenko.models.Cat;
import ru.babenko.models.Owner;

import java.util.List;
import java.util.Optional;

@Service
public class OwnersServiceImpl implements OwnersService {
    private final OwnerMapper ownerMapper;
    private final OwnersDao ownersDao;
    private final CatsDao catsDao;

    @Autowired
    public OwnersServiceImpl(OwnerMapper ownerMapper, OwnersDao ownersDao, CatsDao catsDao) {
        this.ownerMapper = ownerMapper;
        this.ownersDao = ownersDao;
        this.catsDao = catsDao;
    }

    @Override
    public FullOwnerDto findOwnerById(Long id) {
        Optional<Owner> optionalOwner = ownersDao.findById(id);
        return optionalOwner.map(ownerMapper::ownerToFullOwnerDto).orElse(null);
    }

    @Override
    public List<FullOwnerDto> findAllOwners() {
        return ownerMapper.ownersToFullOwnerDtos(ownersDao.findAll());
    }

    @Override
    public FullOwnerDto createOwner(InitialOwnerDto owner) {
        Owner newOwner = Owner.builder()
                .name(owner.name())
                .surname(owner.surname())
                .birthDate(owner.dateOfBirth())
                .cats(List.of())
                .build();
        ownersDao.save(newOwner);
        return ownerMapper.ownerToFullOwnerDto(newOwner);
    }

    @Override
    public void deleteOwner(Long ownerId) {
        ownersDao.deleteById(ownerId);
    }

    @Override
    public void addOwnersCat(Long ownerId, Long catId) {
        Optional<Owner> optionalOwner = ownersDao.findById(ownerId);
        Optional<Cat> optionalCat = catsDao.findById(catId);
        if (optionalOwner.isEmpty() || optionalCat.isEmpty()) {
            throw new IllegalArgumentException("Owner or cat does not exist");
        }
        Owner owner = optionalOwner.get();
        Cat cat = optionalCat.get();
        owner.getCats().add(cat);
        cat.setOwner(owner);
        ownersDao.save(owner);
        catsDao.save(cat);
    }

    @Override
    public void removeOwnersCat(Long ownerId, Long catId) {
        Optional<Owner> optionalOwner = ownersDao.findById(ownerId);
        Optional<Cat> optionalCat = catsDao.findById(catId);
        if (optionalOwner.isEmpty() || optionalCat.isEmpty()) {
            throw new IllegalArgumentException("Owner or cat does not exist");
        }
        Owner owner = optionalOwner.get();
        Cat cat = optionalCat.get();
        owner.getCats().remove(cat);
        ownersDao.save(owner);
    }
}
