package ru.babenko.serivces;

import ru.babenko.dtos.cats.FullCatDto;
import ru.babenko.dtos.cats.InitialCatDto;
import ru.babenko.models.Color;

import java.util.List;

public interface CatsService {
    FullCatDto findCatById(Long id);

    List<FullCatDto> findAllCats();

    FullCatDto createCat(InitialCatDto cat);

    void friendCats(Long firstCatId, Long secondCatId);

    void unfriendCats(Long firstCatId, Long secondCatId);

    void deleteCat(Long catId);

    List<FullCatDto> findCatsByColor(Color color);

    List<FullCatDto> findCatsByBreed(String breed);
}
