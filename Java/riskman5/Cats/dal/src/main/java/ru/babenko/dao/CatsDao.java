package ru.babenko.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.babenko.models.Cat;
import ru.babenko.models.Color;

import java.util.List;

@Repository
public interface CatsDao extends JpaRepository<Cat, Long> {
    List<Cat> findByColor(Color color);
    List<Cat> findByBreed(String breed);
}
