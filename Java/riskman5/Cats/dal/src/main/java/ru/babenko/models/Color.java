package ru.babenko.models;

public enum Color {
    BLACK,
    WHITE,
    GRAY,
    BROWN,
    RED,
    CREAM,
    GOLDEN,
    YELLOW
}