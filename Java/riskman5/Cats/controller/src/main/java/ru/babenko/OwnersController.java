package ru.babenko;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import ru.babenko.dtos.owners.FullOwnerDto;
import ru.babenko.dtos.owners.InitialOwnerDto;
import ru.babenko.serivces.OwnersService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/owners")
public class OwnersController {
    private final OwnersService ownersService;

    @Autowired
    public OwnersController(OwnersService ownersService) {
        this.ownersService = ownersService;
    }

    @GetMapping("/{ownerId}")
    public FullOwnerDto findOwnerById(@PathVariable("ownerId") Long ownerId) {
        return ownersService.findOwnerById(ownerId);
    }

    @GetMapping()
    public List<FullOwnerDto> findAllOwners() {
        return ownersService.findAllOwners();
    }

    @PostMapping()
    public FullOwnerDto createOwner(@RequestBody InitialOwnerDto ownerDto) {
        return ownersService.createOwner(ownerDto);
    }

    @DeleteMapping("/{ownerId}")
    public void deleteOwnerById(@PathVariable("ownerId") Long ownerId) {
        ownersService.deleteOwner(ownerId);
    }

    @PostMapping("/cat")
    public void addOwnersCat(@RequestParam("ownerId") Long ownerId, @RequestParam("catId") Long catId) {
        ownersService.addOwnersCat(ownerId, catId);
    }

    @DeleteMapping("/cat")
    public void removeOwnersCat(@RequestParam("ownerId") Long ownerId, @RequestParam("catId") Long catId) {
        ownersService.removeOwnersCat(ownerId, catId);
    }
}
