package ru.babenko;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.babenko.dtos.cats.FullCatDto;
import ru.babenko.dtos.cats.InitialCatDto;
import ru.babenko.models.Color;
import ru.babenko.serivces.CatsService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cats")
public class CatsController {
    private final CatsService catsService;

    @Autowired
    public CatsController(CatsService catsService) {
        this.catsService = catsService;
    }

    @GetMapping("/{catId}")
    public FullCatDto findCatById(@PathVariable("catId") Long catId) {
        return catsService.findCatById(catId);
    }

    @GetMapping()
    public List<FullCatDto> findAllCats() {
        return catsService.findAllCats();
    }

    @PostMapping()
    public FullCatDto createCat(@RequestBody InitialCatDto catDto) {
        return catsService.createCat(catDto);
    }

    @DeleteMapping("/{catId}")
    public void deleteCatById(@PathVariable("catId") Long catId) {
        catsService.deleteCat(catId);
    }

    @PutMapping("/friend")
    public void friendCats(@RequestParam("firstCatId") Long firstCatId, @RequestParam("secondCatId") Long secondCatId) {
        catsService.friendCats(firstCatId, secondCatId);
    }

    @DeleteMapping("/unfriend")
    public void unfriendCats(@RequestParam("firstCatId") Long firstCatId, @RequestParam("secondCatId") Long secondCatId) {
        catsService.unfriendCats(firstCatId, secondCatId);
    }

    @GetMapping("/color")
    public List<FullCatDto> findCatsByColor(@RequestParam("color") Color color) {
        return catsService.findCatsByColor(color);
    }

    @GetMapping("/breed")
    public List<FullCatDto> findCatsByBreed(@RequestParam("breed") String breed) {
        return catsService.findCatsByBreed(breed);
    }
}
