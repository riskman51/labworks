﻿using System.Collections.Generic;
using System.Linq;
using Itmo.ObjectOrientedProgramming.Lab2.Computers;
using Itmo.ObjectOrientedProgramming.Lab2.HardDisks;
using Itmo.ObjectOrientedProgramming.Lab2.PowerSupplies;
using Itmo.ObjectOrientedProgramming.Lab2.ProcessorCoolingSystems;
using Itmo.ObjectOrientedProgramming.Lab2.Rams;
using Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;
using Xunit;

namespace Itmo.ObjectOrientedProgramming.Lab2.Tests;

public class Tests
{
    [Fact]
    public void Attempt_To_Assemble_Computer_From_Compatible_Components()
    {
        ComputerAssemblyResultTypes result = new ComputerBuilder()
            .WithMotherBoard(new MotherboardRepositorySmallFactory().Create().GetItem(1))
            .WithProcessor(new ProcessorRepositorySmallFactory().Create().GetItem(10001))
            .WithCorpus(new CorpusRepositorySmallFactory().Create().GetItem(100001))
            .WithProcessor(new ProcessorRepositorySmallFactory().Create().GetItem(10001))
            .WithRams(new List<Ram>() { new RamRepositorySmallFactory().Create().GetItem(43201) })
            .WithHardDisks(new List<HardDisk>() { new HardDiskRepositorySmallFactory().Create().GetItem(90001) })
            .WithPowerSupply(new PowerSupplyRepositorySmallFactory().Create().GetItem(200002))
            .WithProcessorCoolingSystem(new ProcessorCoolingSystemRepositorySmallFactory().Create().GetItem(12321))
            .WithVideoCard(new VideocardRepositorySmallFactory().Create().GetItem(70001))
            .Build();

        Assert.True(result is ComputerAssemblyResultTypes.SuccessfulAssembly);
    }

    [Fact]
    public void Trying_To_Build_Computer_With_Big_Power_Consumption()
    {
        ComputerAssemblyResultTypes result = new ComputerBuilder()
            .WithMotherBoard(new MotherboardRepositorySmallFactory().Create().GetItem(1))
            .WithProcessor(new ProcessorRepositorySmallFactory().Create().GetItem(10001))
            .WithCorpus(new CorpusRepositorySmallFactory().Create().GetItem(100001))
            .WithProcessor(new ProcessorRepositorySmallFactory().Create().GetItem(10001))
            .WithRams(new List<Ram>() { new RamRepositorySmallFactory().Create().GetItem(43201) })
            .WithHardDisks(new List<HardDisk>() { new HardDiskRepositorySmallFactory().Create().GetItem(90001) })
            .WithPowerSupply(new PowerSupplyRepositorySmallFactory().Create().GetItem(200001))
            .WithProcessorCoolingSystem(new ProcessorCoolingSystemRepositorySmallFactory().Create().GetItem(12321))
            .WithVideoCard(new VideocardRepositorySmallFactory().Create().GetItem(70001))
            .Build();

        var successfulResultWithWarning = result as ComputerAssemblyResultTypes.SuccessfulAssembly;
        Assert.True(successfulResultWithWarning != null && successfulResultWithWarning.Warnings.First().FirstComponent is PowerSupply);
    }

    [Fact]
    public void Trying_To_Build_Computer_Big_Processor_Tdp()
    {
        ComputerAssemblyResultTypes result = new ComputerBuilder()
            .WithMotherBoard(new MotherboardRepositorySmallFactory().Create().GetItem(1))
            .WithProcessor(new ProcessorRepositorySmallFactory().Create().GetItem(10001))
            .WithCorpus(new CorpusRepositorySmallFactory().Create().GetItem(100001))
            .WithProcessor(new ProcessorRepositorySmallFactory().Create().GetItem(10001))
            .WithRams(new List<Ram>() { new RamRepositorySmallFactory().Create().GetItem(43201) })
            .WithHardDisks(new List<HardDisk>() { new HardDiskRepositorySmallFactory().Create().GetItem(90001) })
            .WithPowerSupply(new PowerSupplyRepositorySmallFactory().Create().GetItem(200002))
            .WithProcessorCoolingSystem(new ProcessorCoolingSystemRepositorySmallFactory().Create().GetItem(12322))
            .WithVideoCard(new VideocardRepositorySmallFactory().Create().GetItem(70001))
            .Build();

        var successfulResultWithWarning = result as ComputerAssemblyResultTypes.SuccessfulAssembly;
        Assert.True(successfulResultWithWarning != null && successfulResultWithWarning.Warnings.First().FirstComponent is ProcessorCoolingSystem);
    }

    [Fact]
    public void Trying_To_Build_Computer_With_Incompatible_Components()
    {
        ComputerAssemblyResultTypes result = new ComputerBuilder()
            .WithMotherBoard(new MotherboardRepositorySmallFactory().Create().GetItem(2))
            .WithProcessor(new ProcessorRepositorySmallFactory().Create().GetItem(10001))
            .WithCorpus(new CorpusRepositorySmallFactory().Create().GetItem(100001))
            .WithProcessor(new ProcessorRepositorySmallFactory().Create().GetItem(10001))
            .WithRams(new List<Ram>() { new RamRepositorySmallFactory().Create().GetItem(43201) })
            .WithHardDisks(new List<HardDisk>() { new HardDiskRepositorySmallFactory().Create().GetItem(90001) })
            .WithPowerSupply(new PowerSupplyRepositorySmallFactory().Create().GetItem(200002))
            .WithProcessorCoolingSystem(new ProcessorCoolingSystemRepositorySmallFactory().Create().GetItem(12321))
            .WithVideoCard(new VideocardRepositorySmallFactory().Create().GetItem(70001))
            .Build();

        Assert.True(result is ComputerAssemblyResultTypes.FailedAssembly);
    }
}