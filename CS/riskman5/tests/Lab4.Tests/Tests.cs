﻿using System.Threading.Tasks;
using Abstractions.Repositories;
using Lab4.Application.BankAccounts;
using Lab4.Application.Contracts;
using Lab4.Application.Models.BankAccounts;
using NSubstitute;
using Xunit;

namespace Itmo.ObjectOrientedProgramming.Lab4.Tests;
public class Tests
{
    [Theory]
    [InlineData(1, 100, 12)]
    [InlineData(2, 1000, 102)]
    [InlineData(4, 41, 12.23)]
    public async Task
        Withdraw_ShouldReturnSuccessfulResultTypeAndBankAccountBalanceShouldBeSuccessfulUpdated_EnoughBalanceOnAccount(long id, decimal userAmount, decimal amountToWithdraw)
    {
        IBankAccountsRepository accountsRepository = Substitute.For<IBankAccountsRepository>();
        IOperationsHistoryRepository operationsRepository = Substitute.For<IOperationsHistoryRepository>();
        var currentBankAccountManager = new CurrentBankAccountManager();
        currentBankAccountManager.BankAccount = new BankAccount(id, "asdjfioasfpa");

        accountsRepository.FindBankAccountById(id).Returns(currentBankAccountManager.BankAccount);
        accountsRepository.FindMoneyAmountById(id).Returns(userAmount);
        accountsRepository.UpdateMoneyAmountInAccountById(id, userAmount - amountToWithdraw)
            .Returns(1);

        var service = new BankAccountService(
            accountsRepository,
            operationsRepository,
            currentBankAccountManager);

        OperationsResultTypes result = await service.Withdraw(amountToWithdraw).ConfigureAwait(true);

        Assert.IsType<OperationsResultTypes.Success>(result);
        await accountsRepository.Received(1).UpdateMoneyAmountInAccountById(id, userAmount - amountToWithdraw)
            .ConfigureAwait(true);
    }

    [Theory]
    [InlineData(1, 12, 100)]
    [InlineData(2,  102, 1000)]
    [InlineData(4, 12.23, 41)]
    public async Task Withdraw_ShouldReturnFailedResultType_NotEnoughBalanceOnAccount(long id, decimal userAmount, decimal amountToWithdraw)
    {
        IBankAccountsRepository accountsRepository = Substitute.For<IBankAccountsRepository>();
        IOperationsHistoryRepository operationsRepository = Substitute.For<IOperationsHistoryRepository>();
        var currentBankAccountManager = new CurrentBankAccountManager();
        currentBankAccountManager.BankAccount = new BankAccount(id, "fasdjpaf");

        accountsRepository.FindBankAccountById(id).Returns(currentBankAccountManager.BankAccount);
        accountsRepository.FindMoneyAmountById(id).Returns(userAmount);
        accountsRepository.UpdateMoneyAmountInAccountById(id, userAmount - amountToWithdraw)
            .Returns(1);

        var service = new BankAccountService(
            accountsRepository,
            operationsRepository,
            currentBankAccountManager);

        OperationsResultTypes result = await service.Withdraw(amountToWithdraw).ConfigureAwait(true);

        Assert.IsType<OperationsResultTypes.Fail>(result);
        accountsRepository.DidNotReceive();
    }

    [Theory]
    [InlineData(1, 100, 12)]
    [InlineData(2,  1000, 32)]
    [InlineData(4, 41, 12.23)]
    public async Task Deposit_ShouldReturnSuccessfulResultTypeAndBankAccountBalanceShouldBeSuccessfulUpdated_CorrectAmount(long id, decimal userAmount, decimal amountToDeposit)
    {
        IBankAccountsRepository accountsRepository = Substitute.For<IBankAccountsRepository>();
        IOperationsHistoryRepository operationsRepository = Substitute.For<IOperationsHistoryRepository>();
        var currentBankAccountManager = new CurrentBankAccountManager();
        currentBankAccountManager.BankAccount = new BankAccount(id, "adsfadsge");

        accountsRepository.FindBankAccountById(id).Returns(currentBankAccountManager.BankAccount);
        accountsRepository.FindMoneyAmountById(id).Returns(userAmount);
        accountsRepository.UpdateMoneyAmountInAccountById(id, userAmount + amountToDeposit)
            .Returns(1);

        var service = new BankAccountService(
            accountsRepository,
            operationsRepository,
            currentBankAccountManager);

        OperationsResultTypes result = await service.Deposit(amountToDeposit).ConfigureAwait(true);

        Assert.IsType<OperationsResultTypes.Success>(result);
        await accountsRepository.Received(1).UpdateMoneyAmountInAccountById(id, userAmount + amountToDeposit)
            .ConfigureAwait(true);
    }
}