﻿using System.Collections.Generic;
using System.Linq;
using Itmo.ObjectOrientedProgramming.Lab3.Addressees;
using Itmo.ObjectOrientedProgramming.Lab3.Filters;
using Itmo.ObjectOrientedProgramming.Lab3.Loggers;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;
using Itmo.ObjectOrientedProgramming.Lab3.Messages.ReadingStatuses;
using Itmo.ObjectOrientedProgramming.Lab3.Messages.ReadResults;
using Itmo.ObjectOrientedProgramming.Lab3.Messengers;
using Itmo.ObjectOrientedProgramming.Lab3.Topics;
using Itmo.ObjectOrientedProgramming.Lab3.Users;
using NSubstitute;
using Xunit;

namespace Itmo.ObjectOrientedProgramming.Lab3.Tests;

public class Tests
{
    [Fact]
    public void ReceiveMessage_ReadableMessage_NotRead()
    {
        Message message = Message.Builder
            .WithTitle("MessageTitle")
            .WithBody("Hi, everyone!")
            .WithImportanceLevel(new ImportanceLevel(1))
            .Build();

        var user = new User();
        IAddressee userAddressee = UserAdapter.Builder
            .WithUser(user)
            .Build();

        var topic = new Topic(userAddressee, "topic");
        topic.AddMessage(message);

        Assert.IsType<ReadingStatus.NotRead>(user.Messages.First().ReadingStatus);
    }

    [Fact]
    public void ReadMessage_NotReadReadableMessage_Read()
    {
        Message message = Message.Builder
            .WithTitle("MessageTitle")
            .WithBody("Hello!")
            .WithImportanceLevel(new ImportanceLevel(1))
            .Build();

        var user = new User();
        IAddressee userAddressee = UserAdapter.Builder
            .WithUser(user)
            .Build();

        var topic = new Topic(userAddressee, "topic");
        topic.AddMessage(message);

        user.Messages.First().ReadMessage();

        Assert.IsType<ReadingStatus.Read>(user.Messages.First().ReadingStatus);
    }

    [Fact]
    public void ReadMessage_ReadReadableMessage_ResultFailed()
    {
        Message message = Message.Builder
            .WithTitle("CoolMessageTitle")
            .WithBody("Hello!")
            .WithImportanceLevel(new ImportanceLevel(1))
            .Build();

        var user = new User();
        IAddressee userAddressee = UserAdapter.Builder
            .WithUser(user)
            .Build();

        var topic = new Topic(userAddressee, "topic");
        topic.AddMessage(message);

        foreach (ReadableMessageDecorator readableMessage in user.Messages)
            readableMessage.ReadMessage();

        Assert.IsType<ReadResult.Fail>(user.Messages.First().ReadMessage());
    }

    [Fact]
    public void ReceiveMessage_MessageWithLowImportanceLevel_NotReceived()
    {
        IAddressee addresseeMock = Substitute.For<IAddressee>();

        var filteringAddresseeProxy = new FilteringAddresseeProxy(
            addresseeMock,
            new Filter(new HashSet<ImportanceLevel>()
            {
                new ImportanceLevel(1),
                new ImportanceLevel(2),
            }));

        Message message = Message.Builder
            .WithTitle("MessageTitle")
            .WithBody("Hi, everyone!")
            .WithImportanceLevel(new ImportanceLevel(3))
            .Build();

        var topic = new Topic(filteringAddresseeProxy, "topic");
        topic.AddMessage(message);

        addresseeMock.DidNotReceive().ReceiveMessage(message);
    }

    [Fact]
    public void Log_Message_SuccessfulLogged()
    {
        IAddressee addresseeMock = Substitute.For<IAddressee>();
        ILogger logger = Substitute.For<ILogger>();

        var loggingAddresseeDecorator = new LoggingAddresseeDecorator(
            addresseeMock,
            logger);

        Message message = Message.Builder
            .WithTitle("MessageTitle")
            .WithBody("Hi, everyone!")
            .WithImportanceLevel(new ImportanceLevel(3))
            .Build();

        var topic = new Topic(loggingAddresseeDecorator, "topic");
        topic.AddMessage(message);

        logger.Received(1).Log(message.Render());
    }

    [Fact]
    public void MessengerPrint_Message_SuccessfulPrinted()
    {
        Messenger messenger = Substitute.For<Messenger>("CoolMessenger");

        IAddressee messengerAdapter = MessengerAdapter.Builder
            .WithMessenger(messenger)
            .Build();

        Message message = Message.Builder
            .WithTitle("MessageTitle")
            .WithBody("Hi, everyone!")
            .WithImportanceLevel(new ImportanceLevel(3))
            .Build();

        var topic = new Topic(messengerAdapter, "topic");
        topic.AddMessage(message);

        messenger.Received(1).Print(message.Render());
    }
}