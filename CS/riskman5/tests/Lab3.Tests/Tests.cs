﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Parsers;
using Xunit;

namespace Itmo.ObjectOrientedProgramming.Lab4.Tests;

public class Tests
{
    [Theory]
    [InlineData("connect D:\\ -m local")]
    [InlineData("file rename D:\\1-oop-basics.pdf cool.pdf")]
    [InlineData("file move D:\\cool.pdf D:\\workshop3")]
    [InlineData("file show D:\\somefile.txt")]
    [InlineData("file show D:\\newfolder\\somefile.txt")]
    [InlineData("tree list -d 10")]
    [InlineData("tree goto D:\\")]
    public void Parse_ShouldReturnSuccessResultType_RightCommands(string command)
    {
        IParser parser = new Parser();

        Assert.IsType<CommandBuildResultType.Success>(parser.Parse(command));
    }

    [Theory]
    [InlineData(" ")]
    [InlineData("hack D:\\workshop3")]
    [InlineData("file movement D:\\cool.pdf D:\\workshop3")]
    [InlineData("folder rename D:\\ C:\\")]
    [InlineData("tree goback D:\\")]
    [InlineData("file lookup D:\\goodfile.txt")]
    public void Parse_ShouldReturnFailureResultType_NotSupportedCommands(string command)
    {
        IParser parser = new Parser();

        Assert.IsType<CommandBuildResultType.Failure>(parser.Parse(command));
    }

    [Theory]
    [InlineData("connect D:\\ -m remote")]
    [InlineData("file show C:\\strangefile.txt -m web")]
    [InlineData("tree list -d aaa")]
    public void Parse_ShouldReturnFailureResultType_NotSupportedModes(string command)
    {
        IParser parser = new Parser();

        Assert.IsType<CommandBuildResultType.Failure>(parser.Parse(command));
    }
}