﻿using Abstractions.Repositories;
using Itmo.Dev.Platform.Postgres.Connection;
using Itmo.Dev.Platform.Postgres.Extensions;
using Lab4.Application.Models.BankAccounts;
using Npgsql;

namespace Lab4.Infrastructure.DataAccess.Repositories;

public class BankAccountsRepository : IBankAccountsRepository
{
    private readonly IPostgresConnectionProvider _connectionProvider;

    public BankAccountsRepository(IPostgresConnectionProvider connectionProvider)
    {
        _connectionProvider = connectionProvider;
    }

    public async Task<BankAccount?> FindBankAccountById(long id)
    {
        const string sqlRequest = """
                                  SELECT bank_account_id, pin_code_hash, balance_amount
                                  FROM bank_accounts
                                  WHERE bank_accounts.bank_account_id = :id;
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);
        command.AddParameter("id", id);

        using NpgsqlDataReader reader = await command.ExecuteReaderAsync().ConfigureAwait(true);

        return await reader.ReadAsync().ConfigureAwait(true) is false
            ? null
            : new BankAccount(
                BankAccountId: reader.GetInt64(0),
                PinCodeHash: reader.GetString(1));
    }

    public async Task<decimal?> FindMoneyAmountById(long id)
    {
        const string sqlRequest = """
                                  SELECT balance_amount
                                  FROM bank_accounts
                                  WHERE bank_accounts.bank_account_id = :id;
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);
        command.AddParameter("id", id);

        using NpgsqlDataReader reader = await command.ExecuteReaderAsync().ConfigureAwait(true);

        return await reader.ReadAsync().ConfigureAwait(true) is false
            ? null
            : reader.GetDecimal(0);
    }

    public async Task<int> UpdateMoneyAmountInAccountById(long id, decimal amount)
    {
        const string sqlRequest = """
                                  UPDATE bank_accounts
                                  SET balance_amount = :amount
                                  WHERE bank_account_id = :id;
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);
        command.AddParameter("amount", amount);
        command.AddParameter("id", id);

        return await command.ExecuteNonQueryAsync().ConfigureAwait(true);
    }

    public async Task<int> CreateBankAccount(long id, string pinCodeHash)
    {
        const string sqlRequest = """
                                  INSERT INTO bank_accounts (bank_account_id, pin_code_hash, balance_amount)
                                  VALUES (:bankAccountId, :pinCodeHash, :moneyAmount);
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);

        command.AddParameter("bankAccountId", id);
        command.AddParameter("pinCodeHash", pinCodeHash);
        command.AddParameter("moneyAmount", 0);

        return await command.ExecuteNonQueryAsync().ConfigureAwait(true);
    }
}