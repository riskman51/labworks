﻿using Abstractions.Repositories;
using Itmo.Dev.Platform.Postgres.Connection;
using Itmo.Dev.Platform.Postgres.Extensions;
using Lab4.Application.Models.BankAccountOperationsHistories;
using Npgsql;

namespace Lab4.Infrastructure.DataAccess.Repositories;

public class OperationsHistoryRepository : IOperationsHistoryRepository
{
    private readonly IPostgresConnectionProvider _connectionProvider;

    public OperationsHistoryRepository(IPostgresConnectionProvider connectionProvider)
    {
        _connectionProvider = connectionProvider;
    }

    public async Task<int> UpdateOperationsHistoryRepository(
        OperationType operationType,
        long bankAccountId,
        decimal amountChanged)
    {
        const string sqlRequest = """
                                  INSERT INTO bank_account_operations_history (operation_type, bank_account_id, money_amount)
                                  VALUES (:operationType, :bankAccountId, :amountChanged);
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);
        command.AddParameter("operationType", operationType);
        command.AddParameter("bankAccountId", bankAccountId);
        command.AddParameter("amountChanged", amountChanged);

        return await command.ExecuteNonQueryAsync().ConfigureAwait(true);
    }

    public async Task<IEnumerable<BankAccountOperation>> FindOperationsByBankAccountId(long id)
    {
        const string sqlRequest = """
                                  SELECT bank_account_operations_history_id, operation_type, bank_account_id, money_amount
                                  FROM bank_account_operations_history
                                  WHERE bank_account_operations_history.bank_account_id = :id;
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);
        command.AddParameter("id", id);

        using NpgsqlDataReader reader = await command.ExecuteReaderAsync().ConfigureAwait(true);

        var operations = new List<BankAccountOperation>();

        while (await reader.ReadAsync().ConfigureAwait(true))
        {
            operations.Add(
                new BankAccountOperation(
                    BankAccountOperationsHistoryId: reader.GetInt64(0),
                    OperationType: await reader.GetFieldValueAsync<OperationType>(1).ConfigureAwait(true),
                    BankAccountId: reader.GetInt64(2),
                    Amount: reader.GetDecimal(3)));
        }

        return operations;
    }

    public async Task<IEnumerable<BankAccountOperation>> FindAllOperations()
    {
        const string sqlRequest = """
                                  SELECT bank_account_operations_history_id, operation_type, bank_account_id, money_amount
                                  FROM bank_account_operations_history
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);

        using NpgsqlDataReader reader = await command.ExecuteReaderAsync().ConfigureAwait(true);

        var operations = new List<BankAccountOperation>();

        while (await reader.ReadAsync().ConfigureAwait(true))
        {
            operations.Add(
                new BankAccountOperation(
                    BankAccountOperationsHistoryId: reader.GetInt64(0),
                    OperationType: await reader.GetFieldValueAsync<OperationType>(1).ConfigureAwait(true),
                    BankAccountId: reader.GetInt64(2),
                    Amount: reader.GetDecimal(3)));
        }

        return operations;
    }
}