﻿using Abstractions.Repositories;
using Itmo.Dev.Platform.Postgres.Connection;
using Itmo.Dev.Platform.Postgres.Extensions;
using Npgsql;

namespace Lab4.Infrastructure.DataAccess.Repositories;

public class SystemPasswordRepository : ISystemPasswordRepository
{
    private readonly IPostgresConnectionProvider _connectionProvider;

    public SystemPasswordRepository(IPostgresConnectionProvider connectionProvider)
    {
        _connectionProvider = connectionProvider;
    }

    public async Task<string?> FindPassword()
    {
        const string sqlRequest = """
                                    SELECT password
                                    FROM system_password;
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);

        using NpgsqlDataReader reader = await command.ExecuteReaderAsync().ConfigureAwait(true);

        return await reader.ReadAsync().ConfigureAwait(true) is false
            ? null
            : reader.GetString(0);
    }

    public async Task<int> ChangePinCode(string pinCodeHash)
    {
        int result = await ClearRepository()
                            .ConfigureAwait(true);

        if (result != 1)
            return result;

        const string sqlRequest = """
                                  INSERT INTO system_password
                                  VALUES (:pinCodeHash)
                                  """;

        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);
        command.AddParameter("pinCodeHash", pinCodeHash);

        return await command.ExecuteNonQueryAsync().ConfigureAwait(true);
    }

    private async Task<int> ClearRepository()
    {
        const string sqlRequest = """
                                  DELETE FROM system_password;
                                  """;
        NpgsqlConnection connection = await _connectionProvider
            .GetConnectionAsync(default)
            .ConfigureAwait(true);

        using var command = new NpgsqlCommand(sqlRequest, connection);

        return await command.ExecuteNonQueryAsync().ConfigureAwait(true);
    }
}