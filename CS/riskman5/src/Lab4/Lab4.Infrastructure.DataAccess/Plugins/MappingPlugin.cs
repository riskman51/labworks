﻿using Itmo.Dev.Platform.Postgres.Plugins;
using Lab4.Application.Models.BankAccountOperationsHistories;
using Npgsql;

namespace Lab4.Infrastructure.DataAccess.Plugins;

public class MappingPlugin : IDataSourcePlugin
{
    public void Configure(NpgsqlDataSourceBuilder builder)
    {
        builder.MapEnum<OperationType>();
    }
}