﻿using FluentMigrator;
using Itmo.Dev.Platform.Postgres.Migrations;

namespace Lab4.Infrastructure.DataAccess.Migrations;

[Migration(1, "Initial")]
public class Initial : SqlMigration
{
    protected override string GetUpSql(IServiceProvider serviceProvider) =>
    """
    CREATE TYPE operation_type AS ENUM (
        'create_account',
        'check_balance',
        'withdraw',
        'deposit'
    );
    
    CREATE TABLE bank_accounts (
        bank_account_id BIGINT PRIMARY KEY,
        pin_code_hash VARCHAR(255),
        balance_amount DECIMAL
    );
    
    CREATE TABLE bank_account_operations_history (
        bank_account_operations_history_id SERIAL PRIMARY KEY,
        operation_type operation_type,
        bank_account_id BIGINT REFERENCES bank_accounts(bank_account_id),
        money_amount DECIMAL
    );
    
    CREATE TABLE system_password (
        password VARCHAR(255) UNIQUE
    );
    
    INSERT INTO system_password VALUES ('112F6D7B0F9F14CD0AFAE08C39B97E5288DB36988AD9B31502BF0D954AD95F53');
    """;

    protected override string GetDownSql(IServiceProvider serviceProvider) =>
    """
    drop table bank_accounts;
    drop table bank_account_operations_history;
    drop table system_password;
    drop type operation_type;
    """;
}