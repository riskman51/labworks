﻿using Lab4.Presentation.Console.Scenarios.AdminScenarios;
using Lab4.Presentation.Console.Scenarios.BankAccountScenarios;
using Microsoft.Extensions.DependencyInjection;

namespace Lab4.Presentation.Console.Extansions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddPresentationConsole(this IServiceCollection collection)
    {
        collection.AddScoped<ScenarioRunner>();

        collection.AddScoped<IScenarioProvider, AdminCreateBankAccountScenarioProvider>();
        collection.AddScoped<IScenarioProvider, AdminLoginScenarioProvider>();
        collection.AddScoped<IScenarioProvider, AdminViewOperationsScenarioProvider>();
        collection.AddScoped<IScenarioProvider, AdminSystemPasswordChangeScenarioProvider>();
        collection.AddScoped<IScenarioProvider, AdminLogoutScenarioProvider>();

        collection.AddScoped<IScenarioProvider, BankAccountLoginScenarioProvider>();
        collection.AddScoped<IScenarioProvider, BankAccountDepositScenarioProvider>();
        collection.AddScoped<IScenarioProvider, BankAccountWithdrawScenarioProvider>();
        collection.AddScoped<IScenarioProvider, BankAccountCheckBalanceScenarioProvider>();
        collection.AddScoped<IScenarioProvider, BankAccountViewOperationsScenarioProvider>();
        collection.AddScoped<IScenarioProvider, BankAccountLogoutScenarioProvider>();

        return collection;
    }
}