﻿using System.Security.Cryptography;
using System.Text;
using Lab4.Application.Contracts;
using Lab4.Application.Contracts.Admins;
using Spectre.Console;

namespace Lab4.Presentation.Console.Scenarios.AdminScenarios;

public class AdminCreateBankAccountScenario : IScenario
{
    private readonly IAdminService _adminService;

    public AdminCreateBankAccountScenario(IAdminService adminService)
    {
        _adminService = adminService;
    }

    public string Name => "Create bank account";

    public void Run()
    {
        long id = AnsiConsole.Ask<long>("Input bank account id");
        string systemPasswordHash = Convert.ToHexString(
            SHA256.HashData(
                Encoding.UTF8.GetBytes(AnsiConsole.Ask<string>("Input system password"))));

        OperationsResultTypes result = _adminService
            .CreateAccount(id, systemPasswordHash)
            .Result;

        string message = result switch
        {
            OperationsResultTypes.Success => "Successful login",
            OperationsResultTypes.Fail failResult => failResult.Message,
            _ => "Something went wrong",
        };

        AnsiConsole.WriteLine(message);
        AnsiConsole.Ask<string>("Ok");
    }
}