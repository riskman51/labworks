﻿using Lab4.Application.Contracts.Admins;

namespace Lab4.Presentation.Console.Scenarios.AdminScenarios;

public class AdminLogoutScenario : IScenario
{
    private readonly IAdminService _adminService;

    public AdminLogoutScenario(IAdminService adminService)
    {
        _adminService = adminService;
    }

    public string Name => "Logout";

    public void Run()
    {
        _adminService.Logout();
    }
}