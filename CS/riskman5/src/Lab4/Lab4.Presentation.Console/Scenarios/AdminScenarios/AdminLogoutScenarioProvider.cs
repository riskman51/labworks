﻿using System.Diagnostics.CodeAnalysis;
using Lab4.Application.Contracts.Admins;

namespace Lab4.Presentation.Console.Scenarios.AdminScenarios;

public class AdminLogoutScenarioProvider : IScenarioProvider
{
    private readonly IAdminService _service;
    private readonly ICurrentAdminService _currentAdmin;

    public AdminLogoutScenarioProvider(IAdminService service, ICurrentAdminService currentAdmin)
    {
        _service = service;
        _currentAdmin = currentAdmin;
    }

    public bool TryGetScenario([NotNullWhen(true)] out IScenario? scenario)
    {
        if (_currentAdmin.Admin is null)
        {
            scenario = null;
            return false;
        }

        scenario = new AdminLogoutScenario(_service);
        return true;
    }
}