﻿using System.Security.Cryptography;
using System.Text;
using Lab4.Application.Contracts;
using Lab4.Application.Contracts.Admins;
using Spectre.Console;

namespace Lab4.Presentation.Console.Scenarios.AdminScenarios;

public class AdminSystemPasswordChangeScenario : IScenario
{
    private readonly IAdminService _adminService;

    public AdminSystemPasswordChangeScenario(IAdminService adminService)
    {
        _adminService = adminService;
    }

    public string Name => "Change system password";

    public void Run()
    {
        string systemPasswordHash = Convert.ToHexString(
            SHA256.HashData(
                Encoding.UTF8.GetBytes(AnsiConsole.Ask<string>("Input new system password"))));

        OperationsResultTypes result = _adminService
            .ChangeSystemPassword(systemPasswordHash)
            .Result;

        string message = result switch
        {
            OperationsResultTypes.Success => "Password was changed",
            OperationsResultTypes.Fail failResult => failResult.Message,
            _ => "Something went wrong",
        };

        AnsiConsole.WriteLine(message);
        AnsiConsole.Ask<string>("Ok");
    }
}