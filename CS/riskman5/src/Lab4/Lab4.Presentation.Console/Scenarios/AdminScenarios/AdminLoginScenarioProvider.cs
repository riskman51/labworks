﻿using System.Diagnostics.CodeAnalysis;
using Lab4.Application.Contracts.Admins;
using Lab4.Application.Contracts.BankAccounts;

namespace Lab4.Presentation.Console.Scenarios.AdminScenarios;

public class AdminLoginScenarioProvider : IScenarioProvider
{
    private readonly IAdminService _service;
    private readonly ICurrentBankAccountService _currentBankAccount;
    private readonly ICurrentAdminService _currentAdmin;

    public AdminLoginScenarioProvider(
        IAdminService service,
        ICurrentBankAccountService currentBankAccount,
        ICurrentAdminService currentAdmin)
    {
        _service = service;
        _currentBankAccount = currentBankAccount;
        _currentAdmin = currentAdmin;
    }

    public bool TryGetScenario(
        [NotNullWhen(true)] out IScenario? scenario)
    {
        if (_currentAdmin.Admin is not null
            || _currentBankAccount.BankAccount is not null)
        {
            scenario = null;
            return false;
        }

        scenario = new AdminLoginScenario(_service);
        return true;
    }
}