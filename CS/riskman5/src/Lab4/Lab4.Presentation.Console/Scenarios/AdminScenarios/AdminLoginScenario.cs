﻿using System.Security.Cryptography;
using System.Text;
using Lab4.Application.Contracts;
using Lab4.Application.Contracts.Admins;
using Spectre.Console;

namespace Lab4.Presentation.Console.Scenarios.AdminScenarios;

public class AdminLoginScenario : IScenario
{
    private readonly IAdminService _adminService;

    public AdminLoginScenario(IAdminService adminService)
    {
        _adminService = adminService;
    }

    public string Name => "Admin login";

    public void Run()
    {
        string systemPasswordHash = Convert.ToHexString(
            SHA256.HashData(
                Encoding.UTF8.GetBytes(AnsiConsole.Ask<string>("Input system password"))));

        OperationsResultTypes result = _adminService
            .Login(systemPasswordHash)
            .Result;

        string message = result switch
        {
            OperationsResultTypes.Success => "Successful login",
            OperationsResultTypes.Fail failResult => failResult.Message,
            _ => "Something went wrong",
        };

        AnsiConsole.WriteLine(message);
        AnsiConsole.Ask<string>("Ok");
    }
}