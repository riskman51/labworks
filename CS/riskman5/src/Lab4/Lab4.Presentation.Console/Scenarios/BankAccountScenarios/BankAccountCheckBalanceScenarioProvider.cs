﻿using System.Diagnostics.CodeAnalysis;
using Lab4.Application.Contracts.BankAccounts;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountCheckBalanceScenarioProvider : IScenarioProvider
{
    private readonly IBankAccountService _service;
    private readonly ICurrentBankAccountService _currentBankAccount;

    public BankAccountCheckBalanceScenarioProvider(
        IBankAccountService service,
        ICurrentBankAccountService currentBankAccount)
    {
        _service = service;
        _currentBankAccount = currentBankAccount;
    }

    public bool TryGetScenario([NotNullWhen(true)] out IScenario? scenario)
    {
        if (_currentBankAccount.BankAccount is null)
        {
            scenario = null;
            return false;
        }

        scenario = new BankAccountCheckBalanceScenario(_service);
        return true;
    }
}