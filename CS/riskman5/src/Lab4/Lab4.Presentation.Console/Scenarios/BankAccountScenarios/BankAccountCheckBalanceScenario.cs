﻿using System.Globalization;
using Lab4.Application.Contracts;
using Lab4.Application.Contracts.BankAccounts;
using Spectre.Console;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountCheckBalanceScenario : IScenario
{
    private readonly IBankAccountService _bankAccountService;

    public BankAccountCheckBalanceScenario(IBankAccountService bankAccountService)
    {
        _bankAccountService = bankAccountService;
    }

    public string Name => "Check balance";
    public void Run()
    {
        MoneyOperationsResultTypes result = _bankAccountService
            .CheckBalance()
            .Result;

        string message = result switch
        {
            MoneyOperationsResultTypes.Success successfulResult => successfulResult.Amount.ToString("C", CultureInfo.CurrentCulture),
            MoneyOperationsResultTypes.Fail failResult => failResult.Message,
            _ => "Something went wrong",
        };

        AnsiConsole.WriteLine(message);
        AnsiConsole.Ask<string>("Ok");
    }
}