﻿using System.Diagnostics.CodeAnalysis;
using Lab4.Application.Contracts.BankAccounts;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountDepositScenarioProvider : IScenarioProvider
{
    private readonly IBankAccountService _service;
    private readonly ICurrentBankAccountService _currentBankAccount;

    public BankAccountDepositScenarioProvider(
        IBankAccountService service,
        ICurrentBankAccountService currentBankAccount)
    {
        _service = service;
        _currentBankAccount = currentBankAccount;
    }

    public bool TryGetScenario([NotNullWhen(true)] out IScenario? scenario)
    {
        if (_currentBankAccount.BankAccount is null)
        {
            scenario = null;
            return false;
        }

        scenario = new BankAccountDepositScenario(_service);
        return true;
    }
}