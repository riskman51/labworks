﻿using Lab4.Application.Contracts;
using Lab4.Application.Contracts.BankAccounts;
using Spectre.Console;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountWithdrawScenario : IScenario
{
    private readonly IBankAccountService _bankAccountService;

    public BankAccountWithdrawScenario(IBankAccountService bankAccountService)
    {
        _bankAccountService = bankAccountService;
    }

    public string Name => "Withdraw";

    public void Run()
    {
        decimal amount = AnsiConsole.Ask<decimal>("Input the amount you want to withdraw from your bank account");

        OperationsResultTypes result = _bankAccountService
            .Withdraw(amount)
            .Result;

        string message = result switch
        {
            OperationsResultTypes.Success => "Successful operation",
            OperationsResultTypes.Fail failResult => failResult.Message,
            _ => "Something went wrong",
        };

        AnsiConsole.WriteLine(message);
        AnsiConsole.Ask<string>("Ok");
    }
}