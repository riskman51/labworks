﻿using Lab4.Application.Contracts;
using Lab4.Application.Contracts.BankAccounts;
using Spectre.Console;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountDepositScenario : IScenario
{
    private readonly IBankAccountService _bankAccountService;

    public BankAccountDepositScenario(IBankAccountService bankAccountService)
    {
        _bankAccountService = bankAccountService;
    }

    public string Name => "Deposit";

    public void Run()
    {
        decimal amount = AnsiConsole.Ask<decimal>("Input the amount you want to top up your bank account with");

        OperationsResultTypes result = _bankAccountService
            .Deposit(amount)
            .Result;

        string message = result switch
        {
            OperationsResultTypes.Success => "Successful operation",
            OperationsResultTypes.Fail failResult => failResult.Message,
            _ => "Something went wrong",
        };

        AnsiConsole.WriteLine(message);
        AnsiConsole.Ask<string>("Ok");
    }
}