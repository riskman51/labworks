﻿using System.Security.Cryptography;
using System.Text;
using Lab4.Application.Contracts;
using Lab4.Application.Contracts.BankAccounts;
using Spectre.Console;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountLoginScenario : IScenario
{
    private readonly IBankAccountService _bankAccountService;

    public BankAccountLoginScenario(IBankAccountService userService)
    {
        _bankAccountService = userService;
    }

    public string Name => "Bank account login";

    public void Run()
    {
        long id = AnsiConsole.Ask<long>("Input bank account id");
        string passwordHash = Convert.ToHexString(
            SHA256.HashData(
            Encoding.UTF8.GetBytes(AnsiConsole.Ask<string>("Input pin-code"))));

        OperationsResultTypes result = _bankAccountService
            .Login(id, passwordHash)
            .Result;

        string message = result switch
        {
            OperationsResultTypes.Success => "Successful login",
            OperationsResultTypes.Fail failResult => failResult.Message,
            _ => "Something went wrong",
        };

        AnsiConsole.WriteLine(message);
        AnsiConsole.Ask<string>("Ok");
    }
}