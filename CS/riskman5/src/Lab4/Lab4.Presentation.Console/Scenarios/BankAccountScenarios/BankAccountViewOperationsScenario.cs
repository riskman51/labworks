﻿using System.Globalization;
using Lab4.Application.Contracts;
using Lab4.Application.Contracts.BankAccounts;
using Lab4.Application.Models.BankAccountOperationsHistories;
using Spectre.Console;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountViewOperationsScenario : IScenario
{
    private readonly IBankAccountService _bankAccountService;

    public BankAccountViewOperationsScenario(IBankAccountService bankAccountService)
    {
        _bankAccountService = bankAccountService;
    }

    public string Name => "View operations";

    public void Run()
    {
        BankAccountOperationsResultTypes result = _bankAccountService
            .CheckOperationsHistory()
            .Result;

        switch (result)
        {
            case BankAccountOperationsResultTypes.Success successResult:
                var table = new Table();
                table.AddColumn(new TableColumn("id").Centered());
                table.AddColumn(new TableColumn("operation type").Centered());
                table.AddColumn(new TableColumn("bank account id").Centered());
                table.AddColumn(new TableColumn("money amount").Centered());

                foreach (BankAccountOperation operation in successResult.BankAccountOperations)
                {
                    table.AddRow(
                        operation.BankAccountOperationsHistoryId.ToString("C", CultureInfo.CurrentCulture),
                        operation.OperationType.ToString(),
                        operation.BankAccountId.ToString("C", CultureInfo.CurrentCulture),
                        operation.Amount.ToString("C", CultureInfo.CurrentCulture));
                }

                AnsiConsole.Write(table);
                break;

            case BankAccountOperationsResultTypes.Fail fail:
                AnsiConsole.WriteLine(fail.Message);
                break;

            default:
                AnsiConsole.WriteLine("Something went wrong");
                break;
        }

        AnsiConsole.Ask<string>("Ok");
    }
}