﻿using Lab4.Application.Contracts.BankAccounts;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountLogoutScenario : IScenario
{
    private readonly IBankAccountService _bankAccountService;

    public BankAccountLogoutScenario(IBankAccountService bankAccountService)
    {
        _bankAccountService = bankAccountService;
    }

    public string Name => "Logout";

    public void Run()
    {
        _bankAccountService.Logout();
    }
}