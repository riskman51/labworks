﻿using System.Diagnostics.CodeAnalysis;
using Lab4.Application.Contracts.Admins;
using Lab4.Application.Contracts.BankAccounts;

namespace Lab4.Presentation.Console.Scenarios.BankAccountScenarios;

public class BankAccountLoginScenarioProvider : IScenarioProvider
{
    private readonly IBankAccountService _service;
    private readonly ICurrentBankAccountService _currentBankAccount;
    private readonly ICurrentAdminService _currentAdmin;

    public BankAccountLoginScenarioProvider(
        IBankAccountService service,
        ICurrentBankAccountService currentBankAccount,
        ICurrentAdminService currentAdmin)
    {
        _service = service;
        _currentBankAccount = currentBankAccount;
        _currentAdmin = currentAdmin;
    }

    public bool TryGetScenario(
        [NotNullWhen(true)] out IScenario? scenario)
    {
        if (_currentBankAccount.BankAccount is not null
            || _currentAdmin.Admin is not null)
        {
            scenario = null;
            return false;
        }

        scenario = new BankAccountLoginScenario(_service);
        return true;
    }
}