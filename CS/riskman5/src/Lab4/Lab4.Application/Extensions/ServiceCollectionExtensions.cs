﻿using Lab4.Application.Admins;
using Lab4.Application.BankAccounts;
using Lab4.Application.Contracts.Admins;
using Lab4.Application.Contracts.BankAccounts;
using Microsoft.Extensions.DependencyInjection;

namespace Lab4.Application.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddApplication(this IServiceCollection collection)
    {
        collection.AddScoped<IBankAccountService, BankAccountService>();
        collection.AddScoped<IAdminService, AdminService>();

        collection.AddScoped<CurrentBankAccountManager>();
        collection.AddScoped<CurrentAdminManager>();

        collection.AddScoped<ICurrentBankAccountService>(
            k => k.GetRequiredService<CurrentBankAccountManager>());
        collection.AddScoped<ICurrentAdminService>(
            k => k.GetRequiredService<CurrentAdminManager>());

        return collection;
    }
}