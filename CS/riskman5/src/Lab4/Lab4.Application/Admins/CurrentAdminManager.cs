﻿using Lab4.Application.Contracts.Admins;
using Lab4.Application.Models.Admins;

namespace Lab4.Application.Admins;

public class CurrentAdminManager : ICurrentAdminService
{
    public Admin? Admin { get; set; }
}