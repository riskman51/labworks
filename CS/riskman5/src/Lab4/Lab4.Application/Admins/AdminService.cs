﻿using Abstractions.Repositories;
using Lab4.Application.Contracts;
using Lab4.Application.Contracts.Admins;
using Lab4.Application.Models.Admins;
using Lab4.Application.Models.BankAccountOperationsHistories;
using Lab4.Application.Models.BankAccounts;

namespace Lab4.Application.Admins;

public class AdminService : IAdminService
{
    private readonly CurrentAdminManager _currentAdminService;
    private readonly IBankAccountsRepository _bankAccountsRepository;
    private readonly IOperationsHistoryRepository _operationsHistoryRepository;
    private readonly ISystemPasswordRepository _systemPasswordRepository;

    public AdminService(CurrentAdminManager currentAdminService, IBankAccountsRepository bankAccountsRepository, IOperationsHistoryRepository operationsHistoryRepository, ISystemPasswordRepository systemPasswordRepository)
    {
        _currentAdminService = currentAdminService;
        _bankAccountsRepository = bankAccountsRepository;
        _operationsHistoryRepository = operationsHistoryRepository;
        _systemPasswordRepository = systemPasswordRepository;
    }

    public async Task<OperationsResultTypes> Login(string systemPasswordHash)
    {
         string? passwordFromDataBase = await _systemPasswordRepository
            .FindPassword()
            .ConfigureAwait(true);

         if (passwordFromDataBase is null)
             return new OperationsResultTypes.Fail("System password wasn't found");

         if (passwordFromDataBase != systemPasswordHash)
             return new OperationsResultTypes.Fail("Incorrect password");

         _currentAdminService.Admin = new Admin();

         return new OperationsResultTypes.Success();
    }

    public async Task<BankAccountOperationsResultTypes> CheckOperationsHistory()
    {
        if (_currentAdminService.Admin is null)
            return new BankAccountOperationsResultTypes.Fail("Unauthorized account");

        IEnumerable<BankAccountOperation> result = await _operationsHistoryRepository
            .FindAllOperations()
            .ConfigureAwait(true);

        return new BankAccountOperationsResultTypes.Success(
            result);
    }

    public async Task<OperationsResultTypes> CreateAccount(long id, string pinCodeHash)
    {
        if (_currentAdminService.Admin is null)
            return new OperationsResultTypes.Fail("Unauthorized account");

        BankAccount? bankAccount = await _bankAccountsRepository
            .FindBankAccountById(id)
            .ConfigureAwait(true);

        if (bankAccount is not null)
            return new OperationsResultTypes.Fail("There is already a user with this id");

        if (await _bankAccountsRepository
            .CreateBankAccount(id, pinCodeHash)
            .ConfigureAwait(true) != 1)
            return new OperationsResultTypes.Fail("Incomplete operation");

        return new OperationsResultTypes.Success();
    }

    public async Task<OperationsResultTypes> ChangeSystemPassword(string pinCodeHash)
    {
        if (_currentAdminService.Admin is null)
            return new OperationsResultTypes.Fail("Unauthorized account");

        if (await _systemPasswordRepository
                .ChangePinCode(pinCodeHash)
                .ConfigureAwait(true)
            != 1)
            return new OperationsResultTypes.Fail("Incomplete operation");

        return new OperationsResultTypes.Success();
    }

    public void Logout()
    {
        _currentAdminService.Admin = null;
    }
}