﻿using Lab4.Application.Contracts.BankAccounts;
using Lab4.Application.Models.BankAccounts;

namespace Lab4.Application.BankAccounts;

public class CurrentBankAccountManager : ICurrentBankAccountService
{
    public BankAccount? BankAccount { get; set; }
}