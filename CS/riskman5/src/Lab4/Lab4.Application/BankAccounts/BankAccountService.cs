﻿using Abstractions.Repositories;
using Lab4.Application.Contracts;
using Lab4.Application.Contracts.BankAccounts;
using Lab4.Application.Models.BankAccountOperationsHistories;
using Lab4.Application.Models.BankAccounts;

namespace Lab4.Application.BankAccounts;

public class BankAccountService : IBankAccountService
{
    private readonly IBankAccountsRepository _bankAccountsRepository;
    private readonly IOperationsHistoryRepository _operationsHistoryRepository;
    private readonly CurrentBankAccountManager _currentBankAccountManager;

    public BankAccountService(
        IBankAccountsRepository bankAccountsRepository,
        IOperationsHistoryRepository operationsHistoryRepository,
        CurrentBankAccountManager currentBankAccountManager)
    {
        _bankAccountsRepository = bankAccountsRepository;
        _operationsHistoryRepository = operationsHistoryRepository;
        _currentBankAccountManager = currentBankAccountManager;
    }

    public async Task<OperationsResultTypes> Login(long bankAccountId, string pinCodeHash)
    {
        BankAccount? bankAccount = await _bankAccountsRepository
            .FindBankAccountById(bankAccountId)
            .ConfigureAwait(true);

        if (bankAccount is null)
            return new OperationsResultTypes.Fail("Account wasn't found");

        if (bankAccount.PinCodeHash != pinCodeHash)
            return new OperationsResultTypes.Fail("Incorrect password");

        _currentBankAccountManager.BankAccount = bankAccount;

        return new OperationsResultTypes.Success();
    }

    public async Task<OperationsResultTypes> Deposit(decimal amount)
    {
        if (_currentBankAccountManager.BankAccount is null)
            return new OperationsResultTypes.Fail("Unauthorized account");

        MoneyOperationsResultTypes result = await CheckBalance()
                                                  .ConfigureAwait(true);

        switch (result)
        {
            case MoneyOperationsResultTypes.Fail failedResult:
                return new OperationsResultTypes.Fail(failedResult.Message);
            case MoneyOperationsResultTypes.Success success:
            {
                decimal amountFromDatabase = success.Amount;

                if (amount <= 0)
                    return new OperationsResultTypes.Fail("Wrong amount");

                if (await _bankAccountsRepository.UpdateMoneyAmountInAccountById(
                        _currentBankAccountManager.BankAccount.BankAccountId, amountFromDatabase + amount)
                    .ConfigureAwait(true) != 1)
                    return new OperationsResultTypes.Fail("DataBase: incomplete operation");

                await _operationsHistoryRepository
                    .UpdateOperationsHistoryRepository(
                        OperationType.Deposit,
                        _currentBankAccountManager.BankAccount.BankAccountId,
                        amount)
                    .ConfigureAwait(true);

                return new OperationsResultTypes.Success();
            }

            default:
                return new OperationsResultTypes.Fail("Out of range: " + nameof(result));
        }
    }

    public async Task<OperationsResultTypes> Withdraw(decimal amount)
    {
        if (_currentBankAccountManager.BankAccount is null)
            return new OperationsResultTypes.Fail("Unauthorized account");

        MoneyOperationsResultTypes result = await CheckBalance()
            .ConfigureAwait(true);

        switch (result)
        {
            case MoneyOperationsResultTypes.Fail failedResult:
                return new OperationsResultTypes.Fail(failedResult.Message);
            case MoneyOperationsResultTypes.Success success:
            {
                decimal amountFromDatabase = success.Amount;

                if (amount > amountFromDatabase
                    || amount <= 0)
                    return new OperationsResultTypes.Fail("Wrong amount");

                if (await _bankAccountsRepository.UpdateMoneyAmountInAccountById(
                            _currentBankAccountManager.BankAccount.BankAccountId, amountFromDatabase - amount)
                        .ConfigureAwait(true) != 1)
                    return new OperationsResultTypes.Fail("DataBase: incomplete operation");

                await _operationsHistoryRepository
                    .UpdateOperationsHistoryRepository(
                        OperationType.Withdraw,
                        _currentBankAccountManager.BankAccount.BankAccountId,
                        amount)
                    .ConfigureAwait(true);

                return new OperationsResultTypes.Success();
            }

            default:
                return new OperationsResultTypes.Fail("Out of range: " + nameof(result));
        }
    }

    public async Task<MoneyOperationsResultTypes> CheckBalance()
    {
        if (_currentBankAccountManager.BankAccount is null)
            return new MoneyOperationsResultTypes.Fail("Unauthorized account");

        decimal? amountFromDatabase = await _bankAccountsRepository
            .FindMoneyAmountById(_currentBankAccountManager.BankAccount.BankAccountId)
            .ConfigureAwait(true);

        return amountFromDatabase is null
            ? new MoneyOperationsResultTypes.Fail("DataBase value not found")
            : new MoneyOperationsResultTypes.Success((decimal)amountFromDatabase);
    }

    public async Task<BankAccountOperationsResultTypes> CheckOperationsHistory()
    {
        if (_currentBankAccountManager.BankAccount is null)
            return new BankAccountOperationsResultTypes.Fail("Unauthorized account");

        IEnumerable<BankAccountOperation> result = await _operationsHistoryRepository
            .FindOperationsByBankAccountId(_currentBankAccountManager.BankAccount.BankAccountId)
            .ConfigureAwait(true);

        return new BankAccountOperationsResultTypes.Success(
            result);
    }

    public void Logout()
    {
        _currentBankAccountManager.BankAccount = null;
    }
}