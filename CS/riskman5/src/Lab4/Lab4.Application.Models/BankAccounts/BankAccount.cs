﻿namespace Lab4.Application.Models.BankAccounts;

public record BankAccount(long BankAccountId, string PinCodeHash);