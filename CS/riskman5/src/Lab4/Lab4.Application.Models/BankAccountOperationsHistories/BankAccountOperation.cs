﻿namespace Lab4.Application.Models.BankAccountOperationsHistories;

public record BankAccountOperation(
    long BankAccountOperationsHistoryId,
    OperationType OperationType,
    long BankAccountId,
    decimal Amount);