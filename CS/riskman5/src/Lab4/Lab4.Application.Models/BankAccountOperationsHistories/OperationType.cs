﻿namespace Lab4.Application.Models.BankAccountOperationsHistories;

public enum OperationType
{
    Withdraw,
    Deposit,
}