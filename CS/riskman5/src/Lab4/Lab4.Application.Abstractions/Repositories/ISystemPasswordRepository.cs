﻿namespace Abstractions.Repositories;

public interface ISystemPasswordRepository
{
    Task<string?> FindPassword();

    Task<int> ChangePinCode(string pinCodeHash);
}