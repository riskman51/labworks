﻿using Lab4.Application.Models.BankAccounts;

namespace Abstractions.Repositories;

public interface IBankAccountsRepository
{
    Task<BankAccount?> FindBankAccountById(long id);

    Task<decimal?> FindMoneyAmountById(long id);

    Task<int> UpdateMoneyAmountInAccountById(long id, decimal amount);

    Task<int> CreateBankAccount(long id, string pinCodeHash);
}