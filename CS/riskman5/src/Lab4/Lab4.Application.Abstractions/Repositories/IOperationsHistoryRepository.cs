﻿using Lab4.Application.Models.BankAccountOperationsHistories;

namespace Abstractions.Repositories;

public interface IOperationsHistoryRepository
{
    Task<int> UpdateOperationsHistoryRepository(
        OperationType operationType,
        long bankAccountId,
        decimal amountChanged);

    Task<IEnumerable<BankAccountOperation>> FindOperationsByBankAccountId(long id);

    Task<IEnumerable<BankAccountOperation>> FindAllOperations();
}