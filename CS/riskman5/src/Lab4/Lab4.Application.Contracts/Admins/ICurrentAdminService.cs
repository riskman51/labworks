﻿using Lab4.Application.Models.Admins;

namespace Lab4.Application.Contracts.Admins;

public interface ICurrentAdminService
{
    Admin? Admin { get; }
}