﻿namespace Lab4.Application.Contracts.Admins;

public interface IAdminService
{
    Task<OperationsResultTypes> Login(string systemPasswordHash);

    Task<BankAccountOperationsResultTypes> CheckOperationsHistory();

    Task<OperationsResultTypes> CreateAccount(long id, string pinCodeHash);

    Task<OperationsResultTypes> ChangeSystemPassword(string pinCodeHash);

    void Logout();
}