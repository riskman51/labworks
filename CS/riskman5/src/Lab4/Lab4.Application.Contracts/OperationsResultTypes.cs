﻿namespace Lab4.Application.Contracts;

public abstract record OperationsResultTypes()
{
    public sealed record Success : OperationsResultTypes;

    public sealed record Fail(string Message) : OperationsResultTypes;
}