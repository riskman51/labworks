﻿using Lab4.Application.Models.BankAccountOperationsHistories;

namespace Lab4.Application.Contracts;

public abstract record BankAccountOperationsResultTypes()
{
    public sealed record Success
        (IEnumerable<BankAccountOperation> BankAccountOperations) : BankAccountOperationsResultTypes;

    public sealed record Fail(string Message) : BankAccountOperationsResultTypes;
}