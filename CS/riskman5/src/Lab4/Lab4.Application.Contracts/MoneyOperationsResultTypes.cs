﻿namespace Lab4.Application.Contracts;

public abstract record MoneyOperationsResultTypes()
{
    public sealed record Success(decimal Amount) : MoneyOperationsResultTypes;

    public sealed record Fail(string Message) : MoneyOperationsResultTypes;
}