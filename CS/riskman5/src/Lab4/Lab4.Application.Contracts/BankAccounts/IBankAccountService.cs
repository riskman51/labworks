﻿namespace Lab4.Application.Contracts.BankAccounts;

public interface IBankAccountService
{
    Task<OperationsResultTypes> Login(long bankAccountId, string pinCodeHash);

    Task<OperationsResultTypes> Deposit(decimal amount);

    Task<OperationsResultTypes> Withdraw(decimal amount);

    Task<MoneyOperationsResultTypes> CheckBalance();

    Task<BankAccountOperationsResultTypes> CheckOperationsHistory();

    void Logout();
}