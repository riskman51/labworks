﻿using Lab4.Application.Models.BankAccounts;

namespace Lab4.Application.Contracts.BankAccounts;

public interface ICurrentBankAccountService
{
    BankAccount? BankAccount { get; }
}