﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab2.Corpuses;

public interface ICorpusBuilder
{
    ICorpusBuilder WithName(string name);

    ICorpusBuilder WithVideoCardMaxLength(int videoCardMaxLength);

    ICorpusBuilder WithVideoCardMaxWidth(int videoCardMaxWidth);

    ICorpusBuilder WithSupportedFormFactorsOfMotherboards(HashSet<int> supportedFormFactorsOfMotherboards);

    ICorpusBuilder WithPowerConsumption(int powerConsumption);

    Corpus Build();
}