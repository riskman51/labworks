﻿using System;
using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.VideoCards;

namespace Itmo.ObjectOrientedProgramming.Lab2.Corpuses;

public class Corpus : IIdentifiable, IComponent, ICompatibility<Motherboard>, ICompatibility<VideoCard>
{
    private Corpus(int id, string name, int videoCardMaxLength, int videoCardMaxWidth, HashSet<int> supportedMotherboardFormFactorIds, int powerConsumption)
    {
        Id = id;
        Name = name;
        VideoCardMaxLength = videoCardMaxLength;
        VideoCardMaxWidth = videoCardMaxWidth;
        SupportedMotherboardFormFactorIds = supportedMotherboardFormFactorIds;
        PowerConsumption = powerConsumption;
    }

    public static IIdBuilder<ICorpusBuilder> Builder => new CorpusBuilder();

    public int Id { get; }

    public string Name { get; }

    public int VideoCardMaxLength { get; }

    public int VideoCardMaxWidth { get;  }

    public HashSet<int> SupportedMotherboardFormFactorIds { get; }

    public int PowerConsumption { get; }

    public CompatibilityResultType Compare(Motherboard component)
    {
        return SupportedMotherboardFormFactorIds.Contains(component.MotherboardFormFactor.Id)
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.Incompatible(this, component, "Unsupported motherboard form-factor");
    }

    public CompatibilityResultType Compare(VideoCard component)
    {
        return (component.Length <= VideoCardMaxLength && component.Width <= VideoCardMaxWidth)
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.Incompatible(this, component, "Length or width of videocard is larger than corpus can take");
    }

    private class CorpusBuilder : IIdBuilder<ICorpusBuilder>, ICorpusBuilder
    {
        private int? _id;
        private string? _name;
        private int? _videoCardMaxLength;
        private int? _videoCardMaxWidth;
        private HashSet<int>? _supportedMotherboardFormFactorIds;
        private int? _powerConsumption;

        public ICorpusBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public ICorpusBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public ICorpusBuilder WithVideoCardMaxLength(int videoCardMaxLength)
        {
            _videoCardMaxLength = videoCardMaxLength;

            return this;
        }

        public ICorpusBuilder WithVideoCardMaxWidth(int videoCardMaxWidth)
        {
            _videoCardMaxWidth = videoCardMaxWidth;

            return this;
        }

        public ICorpusBuilder WithSupportedFormFactorsOfMotherboards(HashSet<int> supportedMotherboardFormFactorIds)
        {
            _supportedMotherboardFormFactorIds = supportedMotherboardFormFactorIds;

            return this;
        }

        public ICorpusBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public Corpus Build()
        {
            return new Corpus(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _videoCardMaxLength ?? throw new ArgumentNullException(nameof(_videoCardMaxLength)),
                _videoCardMaxWidth ?? throw new ArgumentNullException(nameof(_videoCardMaxWidth)),
                _supportedMotherboardFormFactorIds ?? throw new ArgumentNullException(nameof(_supportedMotherboardFormFactorIds)),
                _powerConsumption ?? throw new ArgumentException(nameof(_powerConsumption)));
        }
    }
}