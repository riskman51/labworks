﻿using Itmo.ObjectOrientedProgramming.Lab2.Ssds.ConnectionTypes;

namespace Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;

public class Sata : IConnectionType
{
    public Sata(decimal version)
    {
        Version = version;
    }

    public decimal Version { get;  }
}