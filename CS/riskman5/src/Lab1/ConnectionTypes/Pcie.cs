﻿using Itmo.ObjectOrientedProgramming.Lab2.Ssds.ConnectionTypes;

namespace Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;

public class Pcie : IConnectionType
{
    public Pcie(decimal pcieVersion)
    {
        Version = pcieVersion;
    }

    public decimal Version { get;  }
}