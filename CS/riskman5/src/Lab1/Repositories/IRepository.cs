﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories;

public interface IRepository<T>
where T : IIdentifiable
{
    void Update(T item);

    T GetItem(int id);

    void DeleteItem(int id);
}