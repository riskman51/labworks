﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories;

public class Repository<T> : IRepository<T>
where T : IIdentifiable
{
    private readonly Dictionary<int, T> _itemsDict;

    public Repository()
    {
        _itemsDict = new Dictionary<int, T>();
    }

    public void Update(T item)
    {
        _itemsDict.Add(item.Id, item);
    }

    public T GetItem(int id)
    {
        return _itemsDict[id];
    }

    public void DeleteItem(int id)
    {
        _itemsDict.Remove(id);
    }
}