﻿using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.VideoCards;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class VideocardRepositorySmallFactory : IRepositoryFactory<IRepository<VideoCard>, VideoCard>
{
    public IRepository<VideoCard> Create()
    {
        var videoCardRepository = new Repository<VideoCard>();

        videoCardRepository.Update(
            VideoCard.Builder
                .WithId(70001)
                .WithName("MSI GeForce RTX 3070 Ti GAMING X TRIO")
                .WithPowerConsumption(310)
                .WithLength(100)
                .WithWidth(80)
                .WithVram(8)
                .WithPciVersion(new Pcie(4))
                .Build());

        videoCardRepository.Update(
            VideoCard.Builder
                .WithId(70002)
                .WithName("Palit GeForce GTX 1660 SUPER GamingPro")
                .WithPowerConsumption(125)
                .WithLength(120)
                .WithWidth(105)
                .WithVram(4)
                .WithPciVersion(new Pcie(3))
                .Build());

        return videoCardRepository;
    }
}