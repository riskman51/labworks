﻿using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.HardDisks;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class HardDiskRepositorySmallFactory : IRepositoryFactory<IRepository<HardDisk>, HardDisk>
{
    public IRepository<HardDisk> Create()
    {
        var corpusRepository = new Repository<HardDisk>();

        corpusRepository.Update(
            HardDisk.Builder
                .WithId(90001)
                .WithName("WD Blue [WD10EZEX]")
                .WithPowerConsumption(10)
                .WithCapacity(1024)
                .WithSata(
                    new Sata(3))
                .WithSpindleRotationSpeed(7200)
                .Build());

        corpusRepository.Update(
            HardDisk.Builder
                .WithId(90002)
                .WithName("Toshiba DT01")
                .WithPowerConsumption(10)
                .WithCapacity(1024)
                .WithSata(
                    new Sata(3))
                .WithSpindleRotationSpeed(7200)
                .Build());

        return corpusRepository;
    }
}