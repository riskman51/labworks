﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.MemoryProfiles;
using Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors.Factories;
using Itmo.ObjectOrientedProgramming.Lab2.Rams;
using Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs.Factories;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class RamRepositorySmallFactory : IRepositoryFactory<IRepository<Ram>, Ram>
{
    public IRepository<Ram> Create()
    {
        var ramRepositorySmallFactory = new Repository<Ram>();

        ramRepositorySmallFactory.Update(
            Ram.Builder
                .WithId(43201)
                .WithName("HyperX Fury")
                .WithRamFormFactor(new DimmRamFormFactorFactory().Create())
                .WithMemorySize(8192)
                .WithDdr(new Ddr4Factory().Create())
                .WithSupportedFrequenciesJedecAndVoltagePairs(
                    new HashSet<(int JedecFrequency, double Voltage)>()
                        { (3000, 1.35) })
                .WithAvailableXmpProfiles(
                    new HashSet<XmpProfile>())
                .WithPowerConsumption(2)
                .Build());

        ramRepositorySmallFactory.Update(
            Ram.Builder
                .WithId(43202)
                .WithName("G.SKILL Trident")
                .WithRamFormFactor(new DimmRamFormFactorFactory().Create())
                .WithMemorySize(16384)
                .WithDdr(new Ddr5Factory().Create())
                .WithSupportedFrequenciesJedecAndVoltagePairs(
                    new HashSet<(int JedecFrequency, double Voltage)>()
                        { (6000, 1.35) })
                .WithAvailableXmpProfiles(
                    new HashSet<XmpProfile>())
                .WithPowerConsumption(2)
                .Build());

        return ramRepositorySmallFactory;
    }
}