﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Processors;
using Itmo.ObjectOrientedProgramming.Lab2.Processors.ProcessorSockets;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class ProcessorRepositorySmallFactory : IRepositoryFactory<IRepository<Processor>, Processor>
{
    public IRepository<Processor> Create()
    {
        var processorRepositorySmallFactory = new Repository<Processor>();

        processorRepositorySmallFactory.Update(
            Processor.Builder
                .WithId(10001)
                .WithName("AMD Ryzen 7 3700X")
                .WithProcessorSocket(
                    ProcessorSocket.Builder
                    .WithId(100)
                    .WithName("AM4")
                    .Build())
                .WithPowerConsumption(65)
                .WithTdp(65)
                .WithCoresQuantity(8)
                .WithCoreFrequency(3000)
                .WithSupportedMemoryFrequencies(new HashSet<int>() { 3000, 3200, 3600 })
                .Build());

        processorRepositorySmallFactory.Update(
            Processor.Builder
                .WithId(10002)
                .WithName("Intel Core i9-13900K")
                .WithProcessorSocket(
                    ProcessorSocket.Builder
                        .WithId(101)
                        .WithName("LGA1700")
                        .Build())
                .WithPowerConsumption(253)
                .WithTdp(253)
                .WithCoresQuantity(24)
                .WithCoreFrequency(4000)
                .WithSupportedMemoryFrequencies(new HashSet<int>() { 5600, 6000, 6400 })
                .Build());

        return processorRepositorySmallFactory;
    }
}