﻿using Itmo.ObjectOrientedProgramming.Lab2.PowerSupplies;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class PowerSupplyRepositorySmallFactory : IRepositoryFactory<IRepository<PowerSupply>, PowerSupply>
{
    public IRepository<PowerSupply> Create()
    {
        var powerSupplyRepository = new Repository<PowerSupply>();

        powerSupplyRepository.Update(
            PowerSupply.Builder
                .WithId(200001)
                .WithName("be quiet! SYSTEM POWER 10")
                .WithPowerConsumption(10)
                .WithPeakLoad(210)
                .Build());

        powerSupplyRepository.Update(
            PowerSupply.Builder
                .WithId(200002)
                .WithName("Thermaltake Toughpower GF")
                .WithPowerConsumption(10)
                .WithPeakLoad(850)
                .Build());

        return powerSupplyRepository;
    }
}