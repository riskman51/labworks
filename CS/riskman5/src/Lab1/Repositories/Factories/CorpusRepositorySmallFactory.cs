﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Corpuses;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class CorpusRepositorySmallFactory : IRepositoryFactory<IRepository<Corpus>, Corpus>
{
    public IRepository<Corpus> Create()
    {
        var corpusRepository = new Repository<Corpus>();

        corpusRepository.Update(
            Corpus.Builder
                .WithId(100001)
                .WithName("NZXT H510")
                .WithPowerConsumption(10)
                .WithSupportedFormFactorsOfMotherboards(
                    new HashSet<int>() { 2001, 2002, 2003 })
                .WithVideoCardMaxLength(381)
                .WithVideoCardMaxWidth(200)
                .Build());

        corpusRepository.Update(
            Corpus.Builder
                .WithId(100002)
                .WithName("Thermaltake View 71 Tempered Glass Edition")
                .WithPowerConsumption(15)
                .WithSupportedFormFactorsOfMotherboards(
                    new HashSet<int>() { 2001, 2002, 2003 })
                .WithVideoCardMaxLength(420)
                .WithVideoCardMaxWidth(200)
                .Build());

        return corpusRepository;
    }
}