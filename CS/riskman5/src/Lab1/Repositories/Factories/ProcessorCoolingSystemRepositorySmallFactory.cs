﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.ProcessorCoolingSystems;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class ProcessorCoolingSystemRepositorySmallFactory : IRepositoryFactory<IRepository<ProcessorCoolingSystem>, ProcessorCoolingSystem>
{
    public IRepository<ProcessorCoolingSystem> Create()
    {
        var processorCoolingSystemRepository = new Repository<ProcessorCoolingSystem>();

        processorCoolingSystemRepository.Update(
            ProcessorCoolingSystem.Builder
                .WithId(12321)
                .WithName("ID-COOLING SE-224-XTS")
                .WithLength(75)
                .WithWidth(120)
                .WithHeight(151)
                .WithSupportedSocketsIds(new HashSet<int>() { 100, 101 })
                .WithPowerConsumption(35)
                .WithTdp(220)
                .Build());

        processorCoolingSystemRepository.Update(
            ProcessorCoolingSystem.Builder
                .WithId(12322)
                .WithName("Be quiet! Dark Rock Lite")
                .WithLength(145)
                .WithWidth(136)
                .WithHeight(163)
                .WithSupportedSocketsIds(new HashSet<int>() { 100, 101 })
                .WithPowerConsumption(60)
                .WithTdp(50)
                .Build());

        return processorCoolingSystemRepository;
    }
}