﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models.Factories;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Bioses;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Chipsets;
using Itmo.ObjectOrientedProgramming.Lab2.Processors.ProcessorSockets;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class MotherboardRepositorySmallFactory : IRepositoryFactory<IRepository<Motherboard>, Motherboard>
{
    public IRepository<Motherboard> Create()
    {
        var motherBoardRepository = new Repository<Motherboard>();

        motherBoardRepository.Update(
            Motherboard.Builder
                .WithId(1)
                .WithName("msi b550-a pro")
                .WithProcessorSocket(ProcessorSocket.Builder
                    .WithId(100)
                    .WithName("AM4")
                    .Build())
                .WithAvailablePcies(new Pcie(4), 10)
                .WithAvailableSatas(new Sata(3), 10)
                .WithChipset(Chipset.Builder
                    .WithId(120)
                    .WithName("b550")
                    .WithSupportedRamFrequencies(new HashSet<int>() { 3200 })
                    .WithSupportedXmpProfilesIds(new HashSet<int>() { 1 })
                    .Build())
                .WithAvailableDdrId(2004)
                .WithRamSlotsQuantity(4)
                .WithMotherboardFormFactor(new AtxMotherboardFormFactorFactory().Create())
                .WithBios(Bios.Builder
                    .WithId(5000)
                    .WithType("UEFI")
                    .WithBiosVersion(8)
                    .WithSupportedProcessorsIds(new HashSet<int>() { 100, 101 })
                    .Build())
                .WithPowerConsumption(1)
                .WithAvailableRamFormFactorId(1000)
                .Build());

        motherBoardRepository.Update(
            Motherboard.Builder
                .WithId(2)
                .WithName("ASUS PRIME B660-M")
                .WithProcessorSocket(ProcessorSocket.Builder
                    .WithId(101)
                    .WithName("LGA1700")
                    .Build())
                .WithAvailablePcies(new Pcie(4), 15)
                .WithAvailableSatas(new Sata(3), 9)
                .WithChipset(Chipset.Builder
                    .WithId(121)
                    .WithName("b650")
                    .WithSupportedRamFrequencies(new HashSet<int>() { 5400 })
                    .WithSupportedXmpProfilesIds(new HashSet<int>() { 2 })
                    .Build())
                .WithAvailableDdrId(2005)
                .WithRamSlotsQuantity(4)
                .WithMotherboardFormFactor(new AtxMotherboardFormFactorFactory().Create())
                .WithBios(Bios.Builder
                    .WithId(5001)
                    .WithType("UEFI")
                    .WithBiosVersion(9)
                    .WithSupportedProcessorsIds(new HashSet<int>() { 102, 103 })
                    .Build())
                .WithPowerConsumption(1)
                .WithAvailableRamFormFactorId(1000)
                .Build());
        return motherBoardRepository;
    }
}