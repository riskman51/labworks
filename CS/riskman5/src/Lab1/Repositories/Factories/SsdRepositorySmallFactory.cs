﻿using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.Ssds;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class SsdRepositorySmallFactory : IRepositoryFactory<IRepository<Ssd>, Ssd>
{
    public IRepository<Ssd> Create()
    {
        var ssdRepository = new Repository<Ssd>();

        ssdRepository.Update(
            Ssd.Builder
                .WithId(95001)
                .WithName("Samsung 870 EVO")
                .WithCapacity(1000)
                .PowerConsumption(20)
                .WithConnection(new Sata(3))
                .Build());

        ssdRepository.Update(
            Ssd.Builder
                .WithId(95002)
                .WithName("Kingston A400")
                .WithCapacity(480)
                .PowerConsumption(15)
                .WithConnection(new Sata(3))
                .Build());

        return ssdRepository;
    }
}