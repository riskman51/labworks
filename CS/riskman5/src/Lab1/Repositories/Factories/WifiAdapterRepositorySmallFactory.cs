﻿using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters;
using Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters.BluetoothModules;

namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories.Factories;

public class WifiAdapterRepositorySmallFactory : IRepositoryFactory<IRepository<WifiAdapter>, WifiAdapter>
{
    public IRepository<WifiAdapter> Create()
    {
        var wifiAdapterRepository = new Repository<WifiAdapter>();

        wifiAdapterRepository.Update(
            WifiAdapter.Builder
                .WithId(10101)
                .WithName("TP-LINK Archer T2U Plus")
                .WithPcie(new Pcie(4))
                .WithBluetoothModule(
                    BluetoothModule.Builder
                        .WithId(121231)
                        .WithBluetoothVersion(5)
                        .Build())
                .Build());

        wifiAdapterRepository.Update(
            WifiAdapter.Builder
                .WithId(10210)
                .WithName("Mercusys MW300UH")
                .WithPcie(new Pcie(3))
                .WithBluetoothModule(
                    BluetoothModule.Builder
                        .WithId(121232)
                        .WithBluetoothVersion(5)
                        .Build())
                .Build());

        return wifiAdapterRepository;
    }
}