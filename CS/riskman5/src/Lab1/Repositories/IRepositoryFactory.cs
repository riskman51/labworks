﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Repositories;

public interface IRepositoryFactory<T, TU>
where TU : IIdentifiable
{
    IRepository<TU> Create();
}