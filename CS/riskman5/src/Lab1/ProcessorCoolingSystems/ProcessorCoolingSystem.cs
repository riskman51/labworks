﻿using System;
using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.Processors;

namespace Itmo.ObjectOrientedProgramming.Lab2.ProcessorCoolingSystems;

public class ProcessorCoolingSystem : IIdentifiable, IComponent, ICompatibility<Processor>, ICompatibility<Motherboard>
{
    private ProcessorCoolingSystem(int id, string name, int length, int width, int height, HashSet<int> supportedSocketsIds, int powerConsumption, int tdp)
    {
        Id = id;
        Name = name;
        Length = length;
        Width = width;
        Height = height;
        SupportedSocketsIds = supportedSocketsIds;
        PowerConsumption = powerConsumption;
        Tdp = tdp;
    }

    public static IIdBuilder<IProcessorCoolingSystemBuilder> Builder => new ProcessorCoolingSystemBuilder();

    public int Id { get; }

    public string Name { get; }

    public int Length { get; }

    public int Width { get;  }

    public int Height { get;  }

    public HashSet<int> SupportedSocketsIds { get; }

    public int PowerConsumption { get; }

    public int Tdp { get;  }

    public CompatibilityResultType Compare(Processor component)
    {
        return component.Tdp <= Tdp
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.CompatibleWithWarning(this, component, "The maximum dissipation of the cooling system TDP is less than the processor TDP");
    }

    public CompatibilityResultType Compare(Motherboard component)
    {
        return SupportedSocketsIds.Contains(component.ProcessorSocket.Id)
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.Incompatible(this, component, "Processor cooling system does not support socket");
    }

    private class ProcessorCoolingSystemBuilder : IIdBuilder<IProcessorCoolingSystemBuilder>,
        IProcessorCoolingSystemBuilder
    {
        private int? _id;
        private string? _name;
        private int? _length;
        private int? _width;
        private int? _height;
        private HashSet<int>? _supportedSocketsIds;
        private int? _powerConsumption;
        private int? _tdp;

        public IProcessorCoolingSystemBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IProcessorCoolingSystemBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IProcessorCoolingSystemBuilder WithLength(int length)
        {
            _length = length;

            return this;
        }

        public IProcessorCoolingSystemBuilder WithWidth(int width)
        {
            _width = width;

            return this;
        }

        public IProcessorCoolingSystemBuilder WithHeight(int height)
        {
            _height = height;

            return this;
        }

        public IProcessorCoolingSystemBuilder WithSupportedSocketsIds(HashSet<int> supportedSocketsIds)
        {
            _supportedSocketsIds = supportedSocketsIds;

            return this;
        }

        public IProcessorCoolingSystemBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public IProcessorCoolingSystemBuilder WithTdp(int tdp)
        {
            _tdp = tdp;

            return this;
        }

        public ProcessorCoolingSystem Build()
        {
            return new ProcessorCoolingSystem(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _length ?? throw new ArgumentNullException(nameof(_length)),
                _width ?? throw new ArgumentNullException(nameof(_width)),
                _height ?? throw new ArgumentNullException(nameof(_height)),
                _supportedSocketsIds ?? throw new ArgumentNullException(nameof(_supportedSocketsIds)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_powerConsumption)),
                _tdp ?? throw new ArgumentNullException(nameof(_tdp)));
        }
    }
}