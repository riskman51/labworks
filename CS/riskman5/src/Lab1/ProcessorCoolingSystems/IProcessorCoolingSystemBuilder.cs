﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab2.ProcessorCoolingSystems;

public interface IProcessorCoolingSystemBuilder
{
    IProcessorCoolingSystemBuilder WithName(string name);

    IProcessorCoolingSystemBuilder WithLength(int length);

    IProcessorCoolingSystemBuilder WithWidth(int width);

    IProcessorCoolingSystemBuilder WithHeight(int height);

    IProcessorCoolingSystemBuilder WithSupportedSocketsIds(HashSet<int> supportedSockets);

    IProcessorCoolingSystemBuilder WithPowerConsumption(int powerConsumption);

    IProcessorCoolingSystemBuilder WithTdp(int tdp);

    ProcessorCoolingSystem Build();
}