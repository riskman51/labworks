﻿using Itmo.ObjectOrientedProgramming.Lab2.Ssds.ConnectionTypes;

namespace Itmo.ObjectOrientedProgramming.Lab2.Ssds;

public interface ISsdBuilder
{
    ISsdBuilder WithName(string name);

    ISsdBuilder WithConnection(IConnectionType connection);

    ISsdBuilder WithCapacity(int capacity);

    ISsdBuilder MaxOperatingSpeed(int maxOperatingSpeed);

    ISsdBuilder PowerConsumption(int powerConsumption);

    Ssd Build();
}