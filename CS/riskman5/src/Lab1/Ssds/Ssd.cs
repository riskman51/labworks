﻿using System;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.Ssds.ConnectionTypes;

namespace Itmo.ObjectOrientedProgramming.Lab2.Ssds;

public class Ssd : IComponent, IIdentifiable, ICompatibility<Motherboard>
{
    private Ssd(int id, string name, IConnectionType connection, int capacity, int maxOperatingSpeed, int powerConsumption)
    {
        Id = id;
        Name = name;
        Connection = connection;
        Capacity = capacity;
        MaxOperatingSpeed = maxOperatingSpeed;
        PowerConsumption = powerConsumption;
    }

    public static IIdBuilder<ISsdBuilder> Builder => new SsdBuilder();

    public int Id { get; }

    public string Name { get; }

    public IConnectionType Connection { get; }

    public int Capacity { get;  }

    public int MaxOperatingSpeed { get; }

    public int PowerConsumption { get; }

    public CompatibilityResultType Compare(Motherboard component)
    {
        if (Connection is Pcie pcieConnection)
        {
            return pcieConnection.Version == component.Pcie.Version
                ? new CompatibilityResultType.Compatible()
                : new CompatibilityResultType.CompatibleWithWarning(this, component, "PCI-E version is different");
        }

        if (Connection is Sata sataConnection)
        {
            return sataConnection.Version == component.Sata.Version
                ? new CompatibilityResultType.Compatible()
                : new CompatibilityResultType.CompatibleWithWarning(this, component, "SATA version is different");
        }

        throw new ArgumentException(nameof(Connection));
    }

    private class SsdBuilder : IIdBuilder<ISsdBuilder>, ISsdBuilder
    {
        private int? _id;
        private string? _name;
        private IConnectionType? _connectionType;
        private int? _capacity;
        private int? _maxOperatingSpeed;
        private int? _powerConsumption;

        public ISsdBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public ISsdBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public ISsdBuilder WithConnection(IConnectionType connection)
        {
            _connectionType = connection;

            return this;
        }

        public ISsdBuilder WithCapacity(int capacity)
        {
            _capacity = capacity;

            return this;
        }

        public ISsdBuilder MaxOperatingSpeed(int maxOperatingSpeed)
        {
            _maxOperatingSpeed = maxOperatingSpeed;

            return this;
        }

        public ISsdBuilder PowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public Ssd Build()
        {
            return new Ssd(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _connectionType ?? throw new ArgumentNullException(nameof(_connectionType)),
                _capacity ?? throw new ArgumentNullException(nameof(_capacity)),
                _maxOperatingSpeed ?? throw new ArgumentNullException(nameof(_maxOperatingSpeed)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_powerConsumption)));
        }
    }
}