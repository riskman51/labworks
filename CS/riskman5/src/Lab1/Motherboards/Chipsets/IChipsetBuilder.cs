﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Chipsets;

public interface IChipsetBuilder
{
    IChipsetBuilder WithName(string name);

    IChipsetBuilder WithSupportedRamFrequencies(HashSet<int> supportedRamFrequencies);

    IChipsetBuilder WithSupportedXmpProfilesIds(HashSet<int> supportedXmpProfilesIds);

    Motherboards.Chipsets.Chipset Build();
}