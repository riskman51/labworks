﻿using System;
using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Chipsets;

public class Chipset : IIdentifiable
{
    private Chipset(int id, string name, HashSet<int> supportedRamFrequencies, HashSet<int> supportedXmpProfilesIds)
    {
        Id = id;
        Name = name;
        SupportedRamFrequencies = supportedRamFrequencies;
        SupportedXmpProfilesIds = supportedXmpProfilesIds;
    }

    public static IIdBuilder<IChipsetBuilder> Builder => new ChipsetBuilder();

    public int Id { get; }

    public string Name { get; }

    public HashSet<int> SupportedRamFrequencies { get;  }

    public HashSet<int> SupportedXmpProfilesIds { get; }

    private class ChipsetBuilder : IIdBuilder<IChipsetBuilder>, IChipsetBuilder
    {
        private int? _id;
        private string? _name;
        private HashSet<int>? _supportedRamFrequencies;
        private HashSet<int>? _supportedXmpProfilesIds;

        public IChipsetBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IChipsetBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IChipsetBuilder WithSupportedRamFrequencies(HashSet<int> supportedRamFrequencies)
        {
            _supportedRamFrequencies = supportedRamFrequencies;

            return this;
        }

        public IChipsetBuilder WithSupportedXmpProfilesIds(HashSet<int> supportedXmpProfilesIds)
        {
            _supportedXmpProfilesIds = supportedXmpProfilesIds;

            return this;
        }

        public Chipset Build()
        {
            return new Chipset(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _supportedRamFrequencies ?? throw new ArgumentNullException(nameof(_supportedRamFrequencies)),
                _supportedXmpProfilesIds ?? throw new ArgumentNullException(nameof(_supportedXmpProfilesIds)));
        }
    }
}