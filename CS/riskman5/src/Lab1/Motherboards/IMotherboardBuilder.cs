﻿using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Bioses;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Chipsets;
using Itmo.ObjectOrientedProgramming.Lab2.Processors.ProcessorSockets;

namespace Itmo.ObjectOrientedProgramming.Lab2.Motherboards;

public interface IMotherboardBuilder
{
    IMotherboardBuilder WithName(string name);

    IMotherboardBuilder WithProcessorSocket(ProcessorSocket processorSocket);

    IMotherboardBuilder WithAvailablePcies(Pcie pcie, int count);

    IMotherboardBuilder WithAvailableSatas(Sata sata, int count);

    IMotherboardBuilder WithChipset(Chipset chipset);

    IMotherboardBuilder WithAvailableDdrId(int availableDdrId);

    IMotherboardBuilder WithRamSlotsQuantity(int ramSlotsQuantity);

    IMotherboardBuilder WithAvailableRamFormFactorId(int availableRamFormFactorId);

    IMotherboardBuilder WithMotherboardFormFactor(MotherboardFormFactor motherboardFormFactor);

    IMotherboardBuilder WithBios(Bios bios);

    IMotherboardBuilder WithPowerConsumption(int powerConsumption);

    Motherboard Build();
}