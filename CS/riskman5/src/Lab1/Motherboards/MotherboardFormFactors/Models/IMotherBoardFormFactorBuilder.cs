﻿namespace Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models;

public interface IMotherBoardFormFactorBuilder
{
    IMotherBoardFormFactorBuilder WithName(string name);

    IMotherBoardFormFactorBuilder WithLength(int length);

    IMotherBoardFormFactorBuilder WithWidth(int width);

    MotherboardFormFactor Build();
}