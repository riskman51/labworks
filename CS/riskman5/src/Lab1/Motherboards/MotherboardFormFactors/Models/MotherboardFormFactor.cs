﻿using System;

namespace Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models;

public class MotherboardFormFactor
{
    private MotherboardFormFactor(int id, string name, int length, int width)
    {
        Id = id;
        Name = name;
        Length = length;
        Width = width;
    }

    public static IIdBuilder<IMotherBoardFormFactorBuilder> Builder => new MotherboardFormFactorBuilder();

    public int Id { get; }

    public string Name { get;  }

    public int Length { get; }

    public int Width { get;  }

    private class MotherboardFormFactorBuilder : IIdBuilder<IMotherBoardFormFactorBuilder>, IMotherBoardFormFactorBuilder
    {
        private int? _id;
        private string? _name;
        private int? _length;
        private int? _width;

        public IMotherBoardFormFactorBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IMotherBoardFormFactorBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IMotherBoardFormFactorBuilder WithLength(int length)
        {
            _length = length;

            return this;
        }

        public IMotherBoardFormFactorBuilder WithWidth(int width)
        {
            _width = width;

            return this;
        }

        public MotherboardFormFactor Build()
        {
            return new MotherboardFormFactor(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _length ?? throw new ArgumentNullException(nameof(_length)),
                _width ?? throw new ArgumentNullException(nameof(_width)));
        }
    }
}