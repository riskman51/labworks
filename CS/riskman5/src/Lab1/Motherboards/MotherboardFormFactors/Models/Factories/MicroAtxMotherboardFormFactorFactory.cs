﻿namespace Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models.Factories;

public class MicroAtxMotherboardFormFactorFactory : IMotherboardFormFactorFactory
{
    public MotherboardFormFactor Create()
    {
        return MotherboardFormFactor.Builder
            .WithId(2002)
            .WithName("MicroATX")
            .WithLength(244)
            .WithWidth(244)
            .Build();
    }
}