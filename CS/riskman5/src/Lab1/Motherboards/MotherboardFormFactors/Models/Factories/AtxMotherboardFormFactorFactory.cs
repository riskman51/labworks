﻿namespace Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models.Factories;

public class AtxMotherboardFormFactorFactory : IMotherboardFormFactorFactory
{
    public MotherboardFormFactor Create()
    {
        return MotherboardFormFactor.Builder
            .WithId(2001)
            .WithName("MiniITX")
            .WithLength(170)
            .WithWidth(170)
            .Build();
    }
}