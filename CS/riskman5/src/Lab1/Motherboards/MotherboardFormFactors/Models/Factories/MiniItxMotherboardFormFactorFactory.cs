﻿namespace Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models.Factories;

public class MiniItxMotherboardFormFactorFactory : IMotherboardFormFactorFactory
{
    public MotherboardFormFactor Create()
    {
        return MotherboardFormFactor.Builder
            .WithId(2003)
            .WithName("MiniITX")
            .WithLength(170)
            .WithWidth(170)
            .Build();
    }
}