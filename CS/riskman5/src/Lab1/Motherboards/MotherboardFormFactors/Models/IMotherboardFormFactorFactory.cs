﻿namespace Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models;

public interface IMotherboardFormFactorFactory
{
    MotherboardFormFactor Create();
}