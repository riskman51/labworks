﻿using System;
using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.MotherboardFormFactors.Models;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Bioses;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Chipsets;
using Itmo.ObjectOrientedProgramming.Lab2.Processors.ProcessorSockets;

namespace Itmo.ObjectOrientedProgramming.Lab2.Motherboards;

public class Motherboard : IIdentifiable, IComponent
{
    private Motherboard(int id, string name, ProcessorSocket processorSocket, Pcie pcie, int pcieLinesCount, Sata sata, int sataPortsCount, Chipset chipset, int availableDdrId, int ramSlotsQuantity, int availableRamFormFactorId, MotherboardFormFactor motherboardFormFactor, Bios bios, int powerConsumption)
    {
        Id = id;
        Name = name;
        ProcessorSocket = processorSocket;
        Pcie = pcie;
        PcieLinesCount = pcieLinesCount;
        Sata = sata;
        SataPortsCount = sataPortsCount;
        Chipset = chipset;
        AvailableDdrId = availableDdrId;
        RamSlotsQuantity = ramSlotsQuantity;
        AvailableRamFormFactorId = availableRamFormFactorId;
        MotherboardFormFactor = motherboardFormFactor;
        Bios = bios;
        PowerConsumption = powerConsumption;
    }

    public static IIdBuilder<IMotherboardBuilder> Builder => new MotherboardBuilder();

    public int Id { get; }

    public string Name { get; }

    public ProcessorSocket ProcessorSocket { get; }

    public Pcie Pcie { get; }

    public int PcieLinesCount { get; }

    public Sata Sata { get; }

    public int SataPortsCount { get; }

    public Chipset Chipset { get; }

    public int AvailableDdrId { get; }

    public int AvailableRamFormFactorId { get; }

    public int RamSlotsQuantity { get; }

    public MotherboardFormFactor MotherboardFormFactor { get; }

    public Bios Bios { get; }

    public int PowerConsumption { get; }

    private class MotherboardBuilder : IIdBuilder<IMotherboardBuilder>, IMotherboardBuilder
    {
        private int? _id;
        private string? _name;
        private ProcessorSocket? _processorSocket;
        private Pcie? _pcie;
        private int? _pcieLinesCount;
        private Sata? _sata;
        private int? _sataPortsCount;
        private Chipset? _chipset;
        private int? _availableDdrId;
        private int? _ramSlotsQuantity;
        private int? _availableRamFormFactorId;
        private MotherboardFormFactor? _motherboardFormFactor;
        private Bios? _bios;
        private int? _powerConsumption;

        public IMotherboardBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IMotherboardBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IMotherboardBuilder WithProcessorSocket(ProcessorSocket processorSocket)
        {
            _processorSocket = processorSocket;

            return this;
        }

        public IMotherboardBuilder WithAvailablePcies(Pcie pcie, int count)
        {
            _pcie = pcie;
            _pcieLinesCount = count;

            return this;
        }

        public IMotherboardBuilder WithAvailableSatas(Sata sata, int count)
        {
            _sata = sata;
            _sataPortsCount = count;

            return this;
        }

        public IMotherboardBuilder WithChipset(Chipset chipset)
        {
            _chipset = chipset;

            return this;
        }

        public IMotherboardBuilder WithAvailableDdrId(int availableDdrId)
        {
            _availableDdrId = availableDdrId;

            return this;
        }

        public IMotherboardBuilder WithRamSlotsQuantity(int ramSlotsQuantity)
        {
            _ramSlotsQuantity = ramSlotsQuantity;

            return this;
        }

        public IMotherboardBuilder WithAvailableRamFormFactorId(int availableRamFormFactorId)
        {
            _availableRamFormFactorId = availableRamFormFactorId;

            return this;
        }

        public IMotherboardBuilder WithMotherboardFormFactor(MotherboardFormFactor motherboardFormFactor)
        {
            _motherboardFormFactor = motherboardFormFactor;

            return this;
        }

        public IMotherboardBuilder WithBios(Bios bios)
        {
            _bios = bios;

            return this;
        }

        public IMotherboardBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public Motherboard Build()
        {
            return new Motherboard(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _processorSocket ?? throw new ArgumentNullException(nameof(_processorSocket)),
                _pcie ?? throw new ArgumentNullException(nameof(_pcie)),
                _pcieLinesCount ?? throw new ArgumentNullException(nameof(_pcieLinesCount)),
                _sata ?? throw new ArgumentNullException(nameof(_sata)),
                _sataPortsCount ?? throw new ArgumentNullException(nameof(_sataPortsCount)),
                _chipset ?? throw new ArgumentNullException(nameof(_chipset)),
                _availableDdrId ?? throw new ArgumentNullException(nameof(_availableDdrId)),
                _ramSlotsQuantity ?? throw new ArgumentNullException(nameof(_ramSlotsQuantity)),
                _availableRamFormFactorId ?? throw new ArgumentNullException(nameof(_availableRamFormFactorId)),
                _motherboardFormFactor ?? throw new ArgumentNullException(nameof(_motherboardFormFactor)),
                _bios ?? throw new ArgumentNullException(nameof(_bios)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_powerConsumption)));
        }
    }
}