﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Bioses;

public interface IBiosBuilder
{
    IBiosBuilder WithType(string type);

    IBiosBuilder WithBiosVersion(double biosVersion);

    IBiosBuilder WithSupportedProcessorsIds(HashSet<int> supportedProcessorsIds);

    Bios Build();
}