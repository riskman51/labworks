﻿using System;
using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.Processors;

namespace Itmo.ObjectOrientedProgramming.Lab2.Motherboards.Bioses;

public class Bios : IIdentifiable, ICompatibility<Processor>
{
    private Bios(int id, string type, double biosVersion, HashSet<int> supportedProcessorsIds)
    {
        Id = id;
        Type = type;
        BiosVersion = biosVersion;
        SupportedProcessorsIds = supportedProcessorsIds;
    }

    public static IIdBuilder<IBiosBuilder> Builder => new BiosBuilder();

    public int Id { get; }

    public string Type { get; }

    public double BiosVersion { get; }

    public HashSet<int> SupportedProcessorsIds { get; }

    public CompatibilityResultType Compare(Processor component)
    {
        return SupportedProcessorsIds.Contains(component.Id)
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.Incompatible(component,  null, "Bios: Unsupported processor");
    }

    private class BiosBuilder : IIdBuilder<IBiosBuilder>, IBiosBuilder
    {
        private int? _id;
        private string? _type;
        private double? _biosVersion;
        private HashSet<int>? _supportedProcessorsIds;

        public IBiosBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IBiosBuilder WithType(string type)
        {
            _type = type;

            return this;
        }

        public IBiosBuilder WithBiosVersion(double biosVersion)
        {
            _biosVersion = biosVersion;

            return this;
        }

        public IBiosBuilder WithSupportedProcessorsIds(HashSet<int> supportedProcessorsIds)
        {
            _supportedProcessorsIds = supportedProcessorsIds;

            return this;
        }

        public Bios Build()
        {
            return new Bios(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _type ?? throw new ArgumentNullException(nameof(_type)),
                _biosVersion ?? throw new ArgumentNullException(nameof(_biosVersion)),
                _supportedProcessorsIds ?? throw new ArgumentNullException(nameof(_supportedProcessorsIds)));
        }
    }
}