﻿using System;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;

namespace Itmo.ObjectOrientedProgramming.Lab2.HardDisks;

public class HardDisk : IIdentifiable, IComponent, ICompatibility<Motherboard>
{
    private HardDisk(int id, string name, Sata sata, int capacity, int spindleRotationSpeed, int powerConsumption)
    {
        Id = id;
        Name = name;
        Sata = sata;
        Capacity = capacity;
        SpindleRotationSpeed = spindleRotationSpeed;
        PowerConsumption = powerConsumption;
        Sata = sata;
    }

    public static HardDiskBuilder Builder => new HardDiskBuilder();

    public int Id { get; }

    public string Name { get; }

    public Sata Sata { get; }

    public int Capacity { get;  }

    public int SpindleRotationSpeed { get;  }

    public int PowerConsumption { get; }

    public CompatibilityResultType Compare(Motherboard component)
    {
        return Sata.Version == component.Sata.Version
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.CompatibleWithWarning(this, component, "SATA version is different");
    }

    public class HardDiskBuilder : IIdBuilder<IHardDiskBuilder>, IHardDiskBuilder
    {
        private int? _id;
        private string? _name;
        private Sata? _sata;
        private int? _capacity;
        private int? _spindleRotationSpeed;
        private int? _powerConsumption;

        public IHardDiskBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IHardDiskBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IHardDiskBuilder WithSata(Sata sata)
        {
            _sata = sata;

            return this;
        }

        public IHardDiskBuilder WithCapacity(int capacity)
        {
            _capacity = capacity;

            return this;
        }

        public IHardDiskBuilder WithSpindleRotationSpeed(int spindleRotationSpeed)
        {
            _spindleRotationSpeed = spindleRotationSpeed;

            return this;
        }

        public IHardDiskBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public HardDisk Build()
        {
            return new HardDisk(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _sata ?? throw new ArgumentNullException(nameof(_sata)),
                _capacity ?? throw new ArgumentNullException(nameof(_capacity)),
                _spindleRotationSpeed ?? throw new ArgumentNullException(nameof(_spindleRotationSpeed)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_powerConsumption)));
        }
    }
}