﻿using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;

namespace Itmo.ObjectOrientedProgramming.Lab2.HardDisks;

public interface IHardDiskBuilder
{
    IHardDiskBuilder WithName(string name);

    IHardDiskBuilder WithSata(Sata sata);

    IHardDiskBuilder WithCapacity(int capacity);

    IHardDiskBuilder WithSpindleRotationSpeed(int spindleRotationSpeed);

    IHardDiskBuilder WithPowerConsumption(int powerConsumption);

    HardDisk Build();
}