﻿namespace Itmo.ObjectOrientedProgramming.Lab2;

public interface IIdentifiable
{
    int Id { get; }
}