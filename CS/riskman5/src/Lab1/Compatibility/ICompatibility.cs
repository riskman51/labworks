﻿using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;

namespace Itmo.ObjectOrientedProgramming.Lab2;

public interface ICompatibility<T>
{
    CompatibilityResultType Compare(T component);
}