﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Compatibility;

public abstract record CompatibilityResultType()
{
    public record Compatible() : CompatibilityResultType;

    public sealed record CompatibleWithWarning(IComponent FirstComponent, IComponent? SecondComponent, string Message) : Compatible;

    public sealed record Incompatible(IComponent? FirstComponent, IComponent? SecondComponent, string Message) : CompatibilityResultType;

    public sealed record AbsenceOfNecessaryComponent(string Component) : Compatible;

    public sealed record AbsenceOfVariableComponent(string FirstComponent, string SecondComponent) : Compatible;
}