﻿using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters.BluetoothModules;

namespace Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters;

public interface IWifiAdapterBuilder
{
    IWifiAdapterBuilder WithName(string name);

    IWifiAdapterBuilder WithWifiVersion(double wifiVersion);

    IWifiAdapterBuilder WithPcie(Pcie pcie);

    IWifiAdapterBuilder WithPowerConsumption(int powerConsumption);

    IWifiAdapterBuilder WithBluetoothModule(BluetoothModule bluetoothModule);

    WifiAdapter Build();
}
