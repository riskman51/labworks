﻿using System;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters.BluetoothModules;

namespace Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters;

public class WifiAdapter : IComponent, IIdentifiable, ICompatibility<Motherboard>
{
    private WifiAdapter(int id, string name, double wifiVersion, Pcie pcie, int powerConsumption, BluetoothModule? bluetoothModule)
    {
        Id = id;
        Name = name;
        WifiVersion = wifiVersion;
        Pcie = pcie;
        PowerConsumption = powerConsumption;
        BluetoothModule = bluetoothModule;
    }

    public static IIdBuilder<IWifiAdapterBuilder> Builder => new WifiAdapterBuilder();

    public int Id { get;  }

    public string Name { get; }

    public double WifiVersion { get;  }

    public Pcie Pcie { get;  }

    public int PowerConsumption { get;  }

    public BluetoothModule? BluetoothModule { get; }

    public CompatibilityResultType Compare(Motherboard component)
    {
        return Pcie.Version == component.Pcie.Version
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.CompatibleWithWarning(this, component, "PCI-E version is different");
    }

    private class WifiAdapterBuilder : IIdBuilder<IWifiAdapterBuilder>, IWifiAdapterBuilder
    {
        private int? _id;
        private string? _name;
        private double? _wifiVersion;
        private Pcie? _pcie;
        private int? _powerConsumption;
        private BluetoothModule? _bluetoothModule;

        public IWifiAdapterBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IWifiAdapterBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IWifiAdapterBuilder WithWifiVersion(double wifiVersion)
        {
            _wifiVersion = wifiVersion;

            return this;
        }

        public IWifiAdapterBuilder WithPcie(Pcie pcie)
        {
            _pcie = pcie;

            return this;
        }

        public IWifiAdapterBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public IWifiAdapterBuilder WithBluetoothModule(BluetoothModule bluetoothModule)
        {
            _bluetoothModule = bluetoothModule;

            return this;
        }

        public WifiAdapter Build()
        {
            return new WifiAdapter(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _wifiVersion ?? throw new ArgumentNullException(nameof(_wifiVersion)),
                _pcie ?? throw new ArgumentNullException(nameof(_pcie)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_powerConsumption)),
                _bluetoothModule);
        }
    }
}