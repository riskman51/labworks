﻿namespace Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters.BluetoothModules;

public interface IBluetoothModuleBuilder
{
    IBluetoothModuleBuilder WithBluetoothVersion(int bluetoothVersion);

    BluetoothModule Build();
}