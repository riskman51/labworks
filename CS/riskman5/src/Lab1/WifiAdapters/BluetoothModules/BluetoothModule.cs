﻿using System;

namespace Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters.BluetoothModules;

public class BluetoothModule : IIdentifiable
{
    private BluetoothModule(int id, double bluetoothVersion)
    {
        Id = id;
        BluetoothVersion = bluetoothVersion;
    }

    public static IIdBuilder<IBluetoothModuleBuilder> Builder => new BluetoothModuleBuilder();

    public int Id { get; }

    public double BluetoothVersion { get; }

    private class BluetoothModuleBuilder : IIdBuilder<IBluetoothModuleBuilder>, IBluetoothModuleBuilder
    {
        private int? _id;
        private double? _bluetoothVersion;

        public IBluetoothModuleBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IBluetoothModuleBuilder WithBluetoothVersion(int bluetoothVersion)
        {
            _bluetoothVersion = bluetoothVersion;

            return this;
        }

        public BluetoothModule Build()
        {
            return new BluetoothModule(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _bluetoothVersion ?? throw new ArgumentNullException(nameof(_bluetoothVersion)));
        }
    }
}