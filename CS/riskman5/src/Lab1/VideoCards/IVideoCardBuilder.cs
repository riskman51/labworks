﻿using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;

namespace Itmo.ObjectOrientedProgramming.Lab2.VideoCards;

public interface IVideoCardBuilder
{
    public IVideoCardBuilder WithName(string name);

    public IVideoCardBuilder WithLength(int length);

    public IVideoCardBuilder WithWidth(int width);

    public IVideoCardBuilder WithVram(double videoRamQuantity);

    public IVideoCardBuilder WithPciVersion(Pcie pcie);

    public IVideoCardBuilder WithPowerConsumption(int powerConsumption);

    public VideoCard Build();
}