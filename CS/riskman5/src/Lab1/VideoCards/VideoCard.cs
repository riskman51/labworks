﻿using System;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.ConnectionTypes;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;

namespace Itmo.ObjectOrientedProgramming.Lab2.VideoCards;

public class VideoCard : IComponent, IIdentifiable, ICompatibility<Motherboard>
{
    private VideoCard(int id, string name, int length, int width, double videoRamQuantity, Pcie pcie, int powerConsumption)
    {
        Id = id;
        Name = name;
        Length = length;
        Width = width;
        VideoRamQuantity = videoRamQuantity;
        Pcie = pcie;
        PowerConsumption = powerConsumption;
    }

    public static IIdBuilder<IVideoCardBuilder> Builder => new VideoCardBuilder();

    public int Id { get;  }

    public string Name { get; }

    public int Length { get;  }

    public int Width { get;  }

    public double VideoRamQuantity { get; }

    public Pcie Pcie { get; }

    public int PowerConsumption { get;  }

    public CompatibilityResultType Compare(Motherboard component)
    {
            return Pcie.Version == component.Pcie.Version
                ? new CompatibilityResultType.Compatible()
                : new CompatibilityResultType.CompatibleWithWarning(this, component, "PCI-E version is different");
    }

    private class VideoCardBuilder : IIdBuilder<IVideoCardBuilder>, IVideoCardBuilder
    {
        private int? _id;
        private string? _name;
        private int? _length;
        private int? _width;
        private double? _videoRamQuantity;
        private Pcie? _pcie;
        private int? _powerConsumption;

        public IVideoCardBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IVideoCardBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IVideoCardBuilder WithLength(int length)
        {
            _length = length;

            return this;
        }

        public IVideoCardBuilder WithWidth(int width)
        {
            _width = width;

            return this;
        }

        public IVideoCardBuilder WithVram(double videoRamQuantity)
        {
            _videoRamQuantity = videoRamQuantity;

            return this;
        }

        public IVideoCardBuilder WithPciVersion(Pcie pcie)
        {
            _pcie = pcie;

            return this;
        }

        public IVideoCardBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public VideoCard Build()
        {
            return new VideoCard(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _length ?? throw new ArgumentNullException(nameof(_length)),
                _width ?? throw new ArgumentNullException(nameof(_width)),
                _videoRamQuantity ?? throw new ArgumentNullException(nameof(_videoRamQuantity)),
                _pcie ?? throw new ArgumentNullException(nameof(_pcie)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_powerConsumption)));
        }
    }
}