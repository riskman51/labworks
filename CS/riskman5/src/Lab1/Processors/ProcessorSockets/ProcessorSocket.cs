﻿using System;
using Itmo.ObjectOrientedProgramming.Lab2.ProcessorSockets;

namespace Itmo.ObjectOrientedProgramming.Lab2.Processors.ProcessorSockets;

public class ProcessorSocket
{
    private ProcessorSocket(int id, string name)
    {
        Id = id;
        Name = name;
    }

    public static IIdBuilder<IProcessorSocketBuilder> Builder => new ProcessorSocketBuilder();

    public int Id { get;  }

    public string Name { get;  }

    private class ProcessorSocketBuilder : IIdBuilder<IProcessorSocketBuilder>, IProcessorSocketBuilder
    {
        private int? _id;
        private string? _name;

        public IProcessorSocketBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IProcessorSocketBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public ProcessorSocket Build()
        {
            return new ProcessorSocket(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)));
        }
    }
}