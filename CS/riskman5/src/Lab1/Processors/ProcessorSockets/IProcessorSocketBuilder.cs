﻿using Itmo.ObjectOrientedProgramming.Lab2.Processors.ProcessorSockets;

namespace Itmo.ObjectOrientedProgramming.Lab2.ProcessorSockets;

public interface IProcessorSocketBuilder
{
    IProcessorSocketBuilder WithName(string name);

    ProcessorSocket Build();
}