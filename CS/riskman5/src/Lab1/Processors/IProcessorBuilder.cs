﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Processors.ProcessorSockets;

namespace Itmo.ObjectOrientedProgramming.Lab2.Processors;

public interface IProcessorBuilder
{
    IProcessorBuilder WithName(string name);

    IProcessorBuilder WithCoreFrequency(int coreFrequency);

    IProcessorBuilder WithCoresQuantity(int coresQuantity);

    IProcessorBuilder WithProcessorSocket(ProcessorSocket processorSocket);

    IProcessorBuilder WithBuiltInGraphicsCard(bool builtInGraphicsCard);

    IProcessorBuilder WithSupportedMemoryFrequencies(HashSet<int> supportedMemoryFrequencies);

    IProcessorBuilder WithTdp(int tdp);

    IProcessorBuilder WithPowerConsumption(int powerConsumption);

    Processor Build();
}