﻿using System;
using System.Collections.Generic;
using System.Linq;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.Processors.ProcessorSockets;
using Itmo.ObjectOrientedProgramming.Lab2.Rams;

namespace Itmo.ObjectOrientedProgramming.Lab2.Processors;

public class Processor : IIdentifiable, IComponent, ICompatibility<Motherboard>, ICompatibility<Ram>
{
    private Processor(int id, string name, int coreFrequency, int coresQuantity, ProcessorSocket processorSocket, bool builtInGraphicsCard, HashSet<int> supportedMemoryFrequencies, int tdp, int powerConsumption)
    {
        Id = id;
        Name = name;
        CoreFrequency = coreFrequency;
        CoresQuantity = coresQuantity;
        ProcessorSocket = processorSocket;
        BuiltInGraphicsCard = builtInGraphicsCard;
        SupportedMemoryFrequencies = supportedMemoryFrequencies;
        Tdp = tdp;
        PowerConsumption = powerConsumption;
    }

    public static IIdBuilder<IProcessorBuilder> Builder => new ProcessorBuilder();

    public int Id { get; }

    public string Name { get; }

    public int CoreFrequency { get; }

    public int CoresQuantity { get; }

    public ProcessorSocket ProcessorSocket { get; }

    public bool BuiltInGraphicsCard { get; }

    public HashSet<int> SupportedMemoryFrequencies { get; }

    public int Tdp { get; }

    public int PowerConsumption { get; }

    public CompatibilityResultType Compare(Ram component)
    {
        return component.SupportedFrequenciesJedecAndVoltagePairs
            .Any(pair => SupportedMemoryFrequencies.Contains(pair.JedecFrequency))
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.Incompatible(this, component, "The processor does not support any frequency of RAM");
    }

    public CompatibilityResultType Compare(Motherboard component)
    {
        return ProcessorSocket.Id == component.ProcessorSocket.Id
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.Incompatible(this, component, "The processor and motherboard have different sockets");
    }

    private class ProcessorBuilder : IIdBuilder<IProcessorBuilder>, IProcessorBuilder
    {
        private int? _id;
        private string? _name;
        private int? _coreFrequency;
        private int? _coresQuantity;
        private ProcessorSocket? _processorSocket;
        private bool _builtInGraphicsCard;
        private HashSet<int>? _supportedMemoryFrequencies;
        private int? _tdp;
        private int? _powerConsumption;

        public IProcessorBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IProcessorBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IProcessorBuilder WithCoreFrequency(int coreFrequency)
        {
            _coreFrequency = coreFrequency;

            return this;
        }

        public IProcessorBuilder WithCoresQuantity(int coresQuantity)
        {
            _coresQuantity = coresQuantity;

            return this;
        }

        public IProcessorBuilder WithProcessorSocket(ProcessorSocket processorSocket)
        {
            _processorSocket = processorSocket;

            return this;
        }

        public IProcessorBuilder WithBuiltInGraphicsCard(bool builtInGraphicsCard)
        {
            _builtInGraphicsCard = builtInGraphicsCard;

            return this;
        }

        public IProcessorBuilder WithSupportedMemoryFrequencies(HashSet<int> supportedMemoryFrequencies)
        {
            _supportedMemoryFrequencies = supportedMemoryFrequencies;

            return this;
        }

        public IProcessorBuilder WithTdp(int tdp)
        {
            _tdp = tdp;

            return this;
        }

        public IProcessorBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public Processor Build()
        {
            return new Processor(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _coreFrequency ?? throw new ArgumentNullException(nameof(_coreFrequency)),
                _coresQuantity ?? throw new ArgumentNullException(nameof(_coresQuantity)),
                _processorSocket ?? throw new ArgumentNullException(nameof(_processorSocket)),
                _builtInGraphicsCard,
                _supportedMemoryFrequencies ?? throw new ArgumentNullException(nameof(_supportedMemoryFrequencies)),
                _tdp ?? throw new ArgumentNullException(nameof(_tdp)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_powerConsumption)));
        }
    }
}