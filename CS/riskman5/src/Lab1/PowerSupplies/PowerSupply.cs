﻿using System;
using System.Collections.Generic;
using System.Linq;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;

namespace Itmo.ObjectOrientedProgramming.Lab2.PowerSupplies;

public class PowerSupply : IIdentifiable, IComponent, ICompatibility<IList<IComponent?>>
{
    private PowerSupply(int id, string name, int peakLoad, int powerConsumption)
    {
        Id = id;
        Name = name;
        PeakLoad = peakLoad;
        PowerConsumption = powerConsumption;
    }

    public static IIdBuilder<IPowerSupplyBuilder> Builder => new PowerSupplyBuilder();

    public int Id { get; }

    public string Name { get; }

    public int PeakLoad { get;  }

    public int PowerConsumption { get; }

    public CompatibilityResultType Compare(IList<IComponent?> component)
    {
        int totalPowerConsumption = component.Sum(constituent =>
        {
            if (constituent != null)
                return constituent.PowerConsumption;
            return 0;
        });

        return totalPowerConsumption <= PowerConsumption
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.CompatibleWithWarning(this, null, "Total power consumption is less than power supply's peak load");
    }

    private class PowerSupplyBuilder : IIdBuilder<IPowerSupplyBuilder>, IPowerSupplyBuilder
    {
        private int? _id;
        private string? _name;
        private int? _peakLoad;
        private int? _powerConsumption;

        public IPowerSupplyBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IPowerSupplyBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IPowerSupplyBuilder WithPeakLoad(int peakLoad)
        {
            _peakLoad = peakLoad;

            return this;
        }

        public IPowerSupplyBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public PowerSupply Build()
        {
            return new PowerSupply(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _peakLoad ?? throw new ArgumentNullException(nameof(_peakLoad)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_peakLoad)));
        }
    }
}