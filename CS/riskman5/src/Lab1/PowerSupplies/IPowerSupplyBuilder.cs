﻿namespace Itmo.ObjectOrientedProgramming.Lab2.PowerSupplies;

public interface IPowerSupplyBuilder
{
    IPowerSupplyBuilder WithName(string name);

    IPowerSupplyBuilder WithPeakLoad(int peakLoad);

    IPowerSupplyBuilder WithPowerConsumption(int powerConsumption);

    PowerSupply Build();
}