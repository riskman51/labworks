﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Corpuses;
using Itmo.ObjectOrientedProgramming.Lab2.HardDisks;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.PowerSupplies;
using Itmo.ObjectOrientedProgramming.Lab2.ProcessorCoolingSystems;
using Itmo.ObjectOrientedProgramming.Lab2.Processors;
using Itmo.ObjectOrientedProgramming.Lab2.Rams;
using Itmo.ObjectOrientedProgramming.Lab2.Ssds;
using Itmo.ObjectOrientedProgramming.Lab2.VideoCards;
using Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters;

namespace Itmo.ObjectOrientedProgramming.Lab2.Computers;

public class Computer
{
    public Computer(Motherboard motherboard, Processor processor, ProcessorCoolingSystem processorCoolingSystems, IList<Ram> rams, VideoCard videoCard, IList<Ssd> ssds, IList<HardDisk> hardDisks, Corpus corpus, PowerSupply powerSupply, WifiAdapter? wifiAdapter)
    {
        Motherboard = motherboard;
        Processor = processor;
        ProcessorCoolingSystems = processorCoolingSystems;
        Rams = rams;
        VideoCard = videoCard;
        Ssds = ssds;
        HardDisks = hardDisks;
        Corpus = corpus;
        PowerSupply = powerSupply;
        WifiAdapter = wifiAdapter;
    }

    public Motherboard Motherboard { get; }

    public Processor Processor { get;  }

    public ProcessorCoolingSystem ProcessorCoolingSystems { get; }

    public IList<Ram> Rams { get; }

    public VideoCard? VideoCard { get;  }

    public IList<Ssd> Ssds { get; }

    public IList<HardDisk> HardDisks { get; }

    public Corpus Corpus { get; }

    public PowerSupply PowerSupply { get; }

    public WifiAdapter? WifiAdapter { get; }

    public IComputerBuilder Direct()
    {
        IComputerBuilder builder = new ComputerBuilder()
            .WithMotherBoard(Motherboard)
            .WithProcessor(Processor)
            .WithRams(Rams)
            .WithSsds(Ssds)
            .WithHardDisks(HardDisks)
            .WithCorpus(Corpus)
            .WithPowerSupply(PowerSupply);

        if (VideoCard is not null)
            builder.WithVideoCard(VideoCard);

        if (WifiAdapter is not null)
            builder.WithWifiAdapter(WifiAdapter);

        return builder;
    }
}