﻿using System;
using System.Collections.Generic;
using System.Linq;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.Corpuses;
using Itmo.ObjectOrientedProgramming.Lab2.HardDisks;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.PowerSupplies;
using Itmo.ObjectOrientedProgramming.Lab2.ProcessorCoolingSystems;
using Itmo.ObjectOrientedProgramming.Lab2.Processors;
using Itmo.ObjectOrientedProgramming.Lab2.Rams;
using Itmo.ObjectOrientedProgramming.Lab2.Ssds;
using Itmo.ObjectOrientedProgramming.Lab2.VideoCards;
using Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters;

namespace Itmo.ObjectOrientedProgramming.Lab2.Computers;

public class ComputerBuilder : IComputerBuilder
{
    private readonly HashSet<CompatibilityResultType> _checkingResults;
    private Motherboard? _motherboard;
    private Processor? _processor;
    private ProcessorCoolingSystem? _processorCoolingSystem;
    private IList<Ram> _rams;
    private VideoCard? _videoCard;
    private IList<Ssd> _ssds;
    private IList<HardDisk> _hardDisks;
    private Corpus? _corpus;
    private PowerSupply? _powerSupply;
    private WifiAdapter? _wifiAdapter;

    public ComputerBuilder()
    {
        _rams = new List<Ram>();
        _ssds = new List<Ssd>();
        _hardDisks = new List<HardDisk>();
        _checkingResults = new HashSet<CompatibilityResultType>();
    }

    public IComputerBuilder WithMotherBoard(Motherboard motherboard)
    {
        _motherboard = motherboard;

        return this;
    }

    public IComputerBuilder WithProcessor(Processor processor)
    {
        _processor = processor;

        return this;
    }

    public IComputerBuilder WithProcessorCoolingSystem(ProcessorCoolingSystem processorCoolingSystem)
    {
        _processorCoolingSystem = processorCoolingSystem;

        return this;
    }

    public IComputerBuilder WithRams(IList<Ram> rams)
    {
        _rams = rams;

        return this;
    }

    public IComputerBuilder WithVideoCard(VideoCard videoCard)
    {
        _videoCard = videoCard;

        return this;
    }

    public IComputerBuilder WithSsds(IList<Ssd> ssds)
    {
        _ssds = ssds;

        return this;
    }

    public IComputerBuilder WithHardDisks(IList<HardDisk> hardDisks)
    {
        _hardDisks = hardDisks;

        return this;
    }

    public IComputerBuilder WithCorpus(Corpus corpus)
    {
        _corpus = corpus;

        return this;
    }

    public IComputerBuilder WithPowerSupply(PowerSupply powerSupply)
    {
        _powerSupply = powerSupply;

        return this;
    }

    public IComputerBuilder WithWifiAdapter(WifiAdapter wifiAdapter)
    {
        _wifiAdapter = wifiAdapter;

        return this;
    }

    public ComputerAssemblyResultTypes Build()
    {
        AbsenceComponentsChecks();
        VariableComponentsChecks();
        CompatabilityOfComponentsCheck();

        return !_checkingResults.Any(result => result is CompatibilityResultType.Incompatible)
            ? new ComputerAssemblyResultTypes.SuccessfulAssembly(
                new Computer(
                    _motherboard ?? throw new ArgumentNullException(nameof(_motherboard)),
                    _processor ?? throw new ArgumentNullException(nameof(_processor)),
                    _processorCoolingSystem ?? throw new ArgumentNullException(nameof(_processorCoolingSystem)),
                    _rams ?? throw new ArgumentNullException(nameof(_rams)),
                    _videoCard ?? throw new ArgumentNullException(nameof(_videoCard)),
                    _ssds ?? throw new ArgumentNullException(nameof(_ssds)),
                    _hardDisks ?? throw new ArgumentNullException(nameof(_hardDisks)),
                    _corpus ?? throw new ArgumentNullException(nameof(_corpus)),
                    _powerSupply ?? throw new ArgumentNullException(nameof(_powerSupply)),
                    _wifiAdapter),
                new HashSet<CompatibilityResultType.CompatibleWithWarning>(_checkingResults
                    .Where(result => result is CompatibilityResultType.CompatibleWithWarning)
                    .Cast<CompatibilityResultType.CompatibleWithWarning>()))
            : new ComputerAssemblyResultTypes.FailedAssembly(
                new HashSet<CompatibilityResultType.Incompatible>(_checkingResults
                    .Where(result => result is CompatibilityResultType.Incompatible)
                    .Cast<CompatibilityResultType.Incompatible>()),
                new HashSet<CompatibilityResultType.CompatibleWithWarning>(_checkingResults
                    .Where(result => result is CompatibilityResultType.CompatibleWithWarning)
                    .Cast<CompatibilityResultType.CompatibleWithWarning>()));
    }

    private void AbsenceComponentsChecks()
    {
        if (_motherboard is null)
            _checkingResults.Add(new CompatibilityResultType.AbsenceOfNecessaryComponent(nameof(Motherboard)));
        if (_processor is null)
            _checkingResults.Add(new CompatibilityResultType.AbsenceOfNecessaryComponent(nameof(Processor)));
        if (_processorCoolingSystem is null)
            _checkingResults.Add(new CompatibilityResultType.AbsenceOfNecessaryComponent(nameof(ProcessorCoolingSystem)));
        if (!_rams.Any())
            _checkingResults.Add(new CompatibilityResultType.AbsenceOfNecessaryComponent(nameof(Ram)));
        if (_powerSupply is null)
            _checkingResults.Add(new CompatibilityResultType.AbsenceOfNecessaryComponent(nameof(PowerSupply)));
        if (_corpus is null)
            _checkingResults.Add(new CompatibilityResultType.AbsenceOfNecessaryComponent(nameof(_corpus)));
    }

    private void VariableComponentsChecks()
    {
        if (_processor is { BuiltInGraphicsCard: true } && _videoCard is not null)
            _checkingResults.Add(new CompatibilityResultType.AbsenceOfVariableComponent(nameof(Processor), nameof(VideoCard)));
        if (!_hardDisks.Any() && !_ssds.Any())
            _checkingResults.Add(new CompatibilityResultType.AbsenceOfVariableComponent(nameof(HardDisk), nameof(Ssd)));
    }

    private void CompatabilityOfComponentsCheck()
    {
        if (_motherboard is null || _processor is null || _processorCoolingSystem is null
            || _powerSupply is null || _corpus is null)
            return;

        if (_hardDisks.Count <= _motherboard.SataPortsCount)
        {
            foreach (HardDisk hardDisk in _hardDisks)
                _checkingResults.Add(hardDisk.Compare(_motherboard));
        }
        else
        {
            _checkingResults.Add(new CompatibilityResultType.Incompatible(_motherboard, null, "too many HardDisks"));
        }

        if (_ssds.Count <= _motherboard.PcieLinesCount)
        {
            foreach (Ssd ssd in _ssds)
                _checkingResults.Add(ssd.Compare(_motherboard));
        }
        else
        {
            _checkingResults.Add(new CompatibilityResultType.Incompatible(_motherboard, null, "too many Ssds"));
        }

        if (_wifiAdapter is not null)
            _checkingResults.Add(_wifiAdapter.Compare(_motherboard));

        if (_videoCard is not null)
        {
            _checkingResults.Add(_videoCard.Compare(_motherboard));
            _checkingResults.Add(_corpus.Compare(_videoCard));
        }

        _checkingResults.Add(_corpus.Compare(_motherboard));

        _checkingResults.Add(_processorCoolingSystem.Compare(_motherboard));
        _checkingResults.Add(_processorCoolingSystem.Compare(_processor));

        IList<IComponent?> allComponents = new List<IComponent?>()
        {
            _motherboard,
            _processor,
            _processorCoolingSystem,
            _videoCard,
            _corpus,
            _powerSupply,
            _wifiAdapter,
        };

        foreach (Ram ram in _rams)
            allComponents.Add(ram);

        foreach (HardDisk hardDisk in _hardDisks)
            allComponents.Add(hardDisk);

        foreach (Ssd ssd in _ssds)
            allComponents.Add(ssd);

        _checkingResults.Add(_powerSupply.Compare(allComponents));
        _checkingResults.Add(_processor.Compare(_motherboard));
    }
}