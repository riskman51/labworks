﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;

namespace Itmo.ObjectOrientedProgramming.Lab2.Computers;

public abstract record ComputerAssemblyResultTypes()
{
    public sealed record SuccessfulAssembly(Computer Computer,
        HashSet<CompatibilityResultType.CompatibleWithWarning> Warnings) : ComputerAssemblyResultTypes;

    public sealed record FailedAssembly(HashSet<CompatibilityResultType.Incompatible> Fails,
        HashSet<CompatibilityResultType.CompatibleWithWarning> Warnings) : ComputerAssemblyResultTypes;
}