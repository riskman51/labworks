﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.Corpuses;
using Itmo.ObjectOrientedProgramming.Lab2.HardDisks;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.PowerSupplies;
using Itmo.ObjectOrientedProgramming.Lab2.ProcessorCoolingSystems;
using Itmo.ObjectOrientedProgramming.Lab2.Processors;
using Itmo.ObjectOrientedProgramming.Lab2.Rams;
using Itmo.ObjectOrientedProgramming.Lab2.Ssds;
using Itmo.ObjectOrientedProgramming.Lab2.VideoCards;
using Itmo.ObjectOrientedProgramming.Lab2.WifiAdapters;

namespace Itmo.ObjectOrientedProgramming.Lab2.Computers;

public interface IComputerBuilder
{
   IComputerBuilder WithMotherBoard(Motherboard motherboard);

   IComputerBuilder WithProcessor(Processor processor);

   IComputerBuilder WithProcessorCoolingSystem(ProcessorCoolingSystem processorCoolingSystem);

   IComputerBuilder WithRams(IList<Ram> rams);

   IComputerBuilder WithVideoCard(VideoCard videoCard);

   IComputerBuilder WithSsds(IList<Ssd> ssds);

   IComputerBuilder WithHardDisks(IList<HardDisk> hardDisks);

   IComputerBuilder WithCorpus(Corpus corpus);

   IComputerBuilder WithPowerSupply(PowerSupply powerSupply);

   IComputerBuilder WithWifiAdapter(WifiAdapter wifiAdapter);

   ComputerAssemblyResultTypes Build();
}