﻿namespace Itmo.ObjectOrientedProgramming.Lab2;

public interface IIdBuilder<out T>
{
    T WithId(int id);
}