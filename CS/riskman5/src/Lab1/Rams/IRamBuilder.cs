﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab2.MemoryProfiles;
using Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors;
using Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs;

namespace Itmo.ObjectOrientedProgramming.Lab2.Rams;

public interface IRamBuilder
{
    IRamBuilder WithName(string name);

    IRamBuilder WithMemorySize(int size);

    IRamBuilder WithSupportedFrequenciesJedecAndVoltagePairs(
        HashSet<(int JedecFrequency, double Voltage)> supportedFrequenciesJedecAndVoltagePairs);

    IRamBuilder WithAvailableXmpProfiles(HashSet<XmpProfile> availableXmpProfiles);

    IRamBuilder WithRamFormFactor(RamFormFactor ramFormFactor);

    IRamBuilder WithDdr(Ddr ddr);

    IRamBuilder WithPowerConsumption(int powerConsumption);

    Ram Build();
}