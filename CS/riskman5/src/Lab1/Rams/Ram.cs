﻿using System;
using System.Collections.Generic;
using System.Linq;
using Itmo.ObjectOrientedProgramming.Lab2.Compatibility;
using Itmo.ObjectOrientedProgramming.Lab2.MemoryProfiles;
using Itmo.ObjectOrientedProgramming.Lab2.Motherboards;
using Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors;
using Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs;

namespace Itmo.ObjectOrientedProgramming.Lab2.Rams;

public class Ram : IIdentifiable, IComponent, ICompatibility<Motherboard>
{
    private Ram(int id, string name, int memorySize, HashSet<(int JedecFrequency, double Voltage)> supportedFrequenciesJedecAndVoltagePairs, HashSet<XmpProfile> availableXmpProfiles, RamFormFactor ramFormFactor, Ddr ddr, int powerConsumption)
    {
        Id = id;
        Name = name;
        MemorySize = memorySize;
        SupportedFrequenciesJedecAndVoltagePairs = supportedFrequenciesJedecAndVoltagePairs;
        AvailableXmpProfiles = availableXmpProfiles;
        RamFormFactor = ramFormFactor;
        Ddr = ddr;
        PowerConsumption = powerConsumption;
    }

    public static IIdBuilder<IRamBuilder> Builder => new RamBuilder();

    public int Id { get; }

    public string Name { get; }

    public int MemorySize { get; }

    public HashSet<(int JedecFrequency, double Voltage)> SupportedFrequenciesJedecAndVoltagePairs { get;  }

    public HashSet<XmpProfile> AvailableXmpProfiles { get; }

    public RamFormFactor RamFormFactor { get; }

    public Ddr Ddr { get; }

    public int PowerConsumption { get; }

    public CompatibilityResultType Compare(Motherboard component)
    {
        return (component.AvailableRamFormFactorId == RamFormFactor.Id &&
                component.AvailableDdrId == Ddr.Id &&
                AvailableXmpProfiles.Any(availableXmpProfile =>
                    component.Chipset.SupportedXmpProfilesIds.Contains(availableXmpProfile.Id)))
            ? new CompatibilityResultType.Compatible()
            : new CompatibilityResultType.Incompatible(this, component, "RAM is incompatible with the motherboard");
    }

    private class RamBuilder : IIdBuilder<IRamBuilder>, IRamBuilder
    {
        private int? _id;
        private string? _name;
        private int? _size;
        private HashSet<(int JedecFrequency, double Voltage)>? _supportedFrequenciesJedecAndVoltagePairs;
        private HashSet<XmpProfile>? _availableXmpProfiles;
        private RamFormFactor? _ramFormFactor;
        private Ddr? _ddr;
        private int? _powerConsumption;

        public IRamBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IRamBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public IRamBuilder WithMemorySize(int size)
        {
            _size = size;

            return this;
        }

        public IRamBuilder WithSupportedFrequenciesJedecAndVoltagePairs(
            HashSet<(int JedecFrequency, double Voltage)> supportedFrequenciesJedecAndVoltagePairs)
        {
            _supportedFrequenciesJedecAndVoltagePairs = supportedFrequenciesJedecAndVoltagePairs;

            return this;
        }

        public IRamBuilder WithAvailableXmpProfiles(HashSet<XmpProfile> availableXmpProfiles)
        {
            _availableXmpProfiles = availableXmpProfiles;

            return this;
        }

        public IRamBuilder WithRamFormFactor(RamFormFactor ramFormFactor)
        {
            _ramFormFactor = ramFormFactor;

            return this;
        }

        public IRamBuilder WithDdr(Ddr ddr)
        {
            _ddr = ddr;

            return this;
        }

        public IRamBuilder WithPowerConsumption(int powerConsumption)
        {
            _powerConsumption = powerConsumption;

            return this;
        }

        public Ram Build()
        {
            return new Ram(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)),
                _size ?? throw new ArgumentNullException(nameof(_size)),
                _supportedFrequenciesJedecAndVoltagePairs ?? throw new ArgumentNullException(nameof(_supportedFrequenciesJedecAndVoltagePairs)),
                _availableXmpProfiles ?? throw new ArgumentNullException(nameof(_availableXmpProfiles)),
                _ramFormFactor ?? throw new ArgumentNullException(nameof(_ramFormFactor)),
                _ddr ?? throw new ArgumentNullException(nameof(_ddr)),
                _powerConsumption ?? throw new ArgumentNullException(nameof(_powerConsumption)));
        }
    }
}