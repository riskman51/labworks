﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs;

public interface IDdrFactory
{
    Ddr Create();
}