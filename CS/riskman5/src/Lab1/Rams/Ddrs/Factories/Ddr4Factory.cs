﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs.Factories;

public class Ddr4Factory : IDdrFactory
{
    public Ddr Create()
    {
        return Ddr.Builder
            .WithId(2004)
            .WithName("DDR4")
            .Build();
    }
}