﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs.Factories;

public class Ddr3Factory : IDdrFactory
{
    public Ddr Create()
    {
        return Ddr.Builder
            .WithId(2003)
            .WithName("DDR3")
            .Build();
    }
}