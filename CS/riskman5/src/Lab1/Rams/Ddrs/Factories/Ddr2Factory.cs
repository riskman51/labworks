﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs.Factories;

public class Ddr2Factory : IDdrFactory
{
    public Ddr Create()
    {
        return Ddr.Builder
            .WithId(3002)
            .WithName("DDR2")
            .Build();
    }
}