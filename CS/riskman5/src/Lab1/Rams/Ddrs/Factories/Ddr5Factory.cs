﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs.Factories;

public class Ddr5Factory : IDdrFactory
{
    public Ddr Create()
    {
        return Ddr.Builder
            .WithId(2005)
            .WithName("DDR5")
            .Build();
    }
}