﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs.Factories;

public class Ddr1Factory : IDdrFactory
{
    public Ddr Create()
    {
        return Ddr.Builder
            .WithId(3001)
            .WithName("DDR1")
            .Build();
    }
}