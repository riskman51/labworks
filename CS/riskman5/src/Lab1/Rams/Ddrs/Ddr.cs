﻿using System;

namespace Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs;

public class Ddr : IIdentifiable
{
    private Ddr(int id, string name)
    {
        Id = id;
        Name = name;
    }

    public static IIdBuilder<IDdrBuilder> Builder => new DdrBuilder();

    public int Id { get; }

    public string Name { get;  }

    private class DdrBuilder : IIdBuilder<IDdrBuilder>, IDdrBuilder
    {
        private int? _id;
        private string? _name;

        public IDdrBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IDdrBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public Ddr Build()
        {
            return new Ddr(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)));
        }
    }
}