﻿namespace Itmo.ObjectOrientedProgramming.Lab2.Rams.Ddrs;

public interface IDdrBuilder
{
    IDdrBuilder WithName(string name);

    Ddr Build();
}