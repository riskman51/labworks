﻿using System;

namespace Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors;

public class RamFormFactor : IIdentifiable
{
    private RamFormFactor(int id, string name)
    {
        Id = id;
        Name = name;
    }

    public static IIdBuilder<IRamFormFactorBuilder> Builder => new RamFormFactorBuilder();

    public int Id { get; }

    public string Name { get; }

    private class RamFormFactorBuilder : IIdBuilder<IRamFormFactorBuilder>, IRamFormFactorBuilder
    {
        private int? _id;
        private string? _name;

        public IRamFormFactorBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IRamFormFactorBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public RamFormFactor Build()
        {
            return new RamFormFactor(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _name ?? throw new ArgumentNullException(nameof(_name)));
        }
    }
}