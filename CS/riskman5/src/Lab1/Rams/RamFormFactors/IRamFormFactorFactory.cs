﻿namespace Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors;

public interface IRamFormFactorFactory
{
    RamFormFactor Create();
}