﻿namespace Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors;

public interface IRamFormFactorBuilder
{
    IRamFormFactorBuilder WithName(string name);

    RamFormFactor Build();
}