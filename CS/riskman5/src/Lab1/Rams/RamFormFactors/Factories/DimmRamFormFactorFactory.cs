﻿namespace Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors.Factories;

public class DimmRamFormFactorFactory : IRamFormFactorFactory
{
    public RamFormFactor Create()
    {
        return RamFormFactor.Builder
            .WithId(1000)
            .WithName("DIMM")
            .Build();
    }
}