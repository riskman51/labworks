﻿namespace Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors.Factories;

public class SodimmRamFormFactorFactory : IRamFormFactorFactory
{
    public RamFormFactor Create()
    {
        return RamFormFactor.Builder
            .WithId(1001)
            .WithName("SODIMM")
            .Build();
    }
}