﻿namespace Itmo.ObjectOrientedProgramming.Lab2.RamFormFactors.Factories;

public class SimmRamFormFactorFactory : IRamFormFactorFactory
{
    public RamFormFactor Create()
    {
        return RamFormFactor.Builder
            .WithId(1002)
            .WithName("SIMM")
            .Build();
    }
}