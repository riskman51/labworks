﻿using System;
using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab2.MemoryProfiles;

public class XmpProfile : IIdentifiable
{
    private XmpProfile(int id, IEnumerable<int> timings, double voltage, int frequency)
    {
        Id = id;
        Timings = timings;
        Voltage = voltage;
        Frequency = frequency;
    }

    public static IIdBuilder<IXmpProfileBuilder> Builder => new XmpProfileBuilder();

    public int Id { get; }

    public IEnumerable<int> Timings { get; }

    public double Voltage { get; }

    public int Frequency { get; }

    private class XmpProfileBuilder : IIdBuilder<IXmpProfileBuilder>, IXmpProfileBuilder
    {
        private int? _id;
        private IEnumerable<int>? _timings;
        private double? _voltage;
        private int? _frequency;

        public IXmpProfileBuilder WithId(int id)
        {
            _id = id;

            return this;
        }

        public IXmpProfileBuilder WithTimings(IEnumerable<int> timings)
        {
            _timings = timings;

            return this;
        }

        public IXmpProfileBuilder WithVoltage(double voltage)
        {
            _voltage = voltage;

            return this;
        }

        public IXmpProfileBuilder WithFrequency(int frequency)
        {
            _frequency = frequency;

            return this;
        }

        public XmpProfile Build()
        {
            return new XmpProfile(
                _id ?? throw new ArgumentNullException(nameof(_id)),
                _timings ?? throw new ArgumentNullException(nameof(_timings)),
                _voltage ?? throw new ArgumentNullException(nameof(_voltage)),
                _frequency ?? throw new ArgumentNullException(nameof(_frequency)));
        }
    }
}