﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab2.MemoryProfiles;

public interface IXmpProfileBuilder
{
    IXmpProfileBuilder WithTimings(IEnumerable<int> timings);

    IXmpProfileBuilder WithVoltage(double voltage);

    IXmpProfileBuilder WithFrequency(int frequency);

    XmpProfile Build();
}