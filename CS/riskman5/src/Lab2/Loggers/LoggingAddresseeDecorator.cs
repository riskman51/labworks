﻿using Itmo.ObjectOrientedProgramming.Lab3.Addressees;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;

namespace Itmo.ObjectOrientedProgramming.Lab3.Loggers;

public class LoggingAddresseeDecorator : IAddressee
{
    private readonly IAddressee _addressee;
    private readonly ILogger _logger;

    internal LoggingAddresseeDecorator(IAddressee addressee, ILogger logger)
    {
        _addressee = addressee;
        _logger = logger;
    }

    public void ReceiveMessage(IMessage message)
    {
        _logger.Log(message.Render());
        _addressee.ReceiveMessage(message);
    }
}