﻿namespace Itmo.ObjectOrientedProgramming.Lab3.Loggers;

public class FileRecordingLogger : ILogger
{
    private readonly string _path;

    public FileRecordingLogger(string path)
    {
        _path = path;
    }

    public void Log(string text)
    {
        System.IO.File.WriteAllText(_path, text);
    }
}