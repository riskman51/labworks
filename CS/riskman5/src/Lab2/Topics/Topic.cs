﻿using Itmo.ObjectOrientedProgramming.Lab3.Addressees;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;

namespace Itmo.ObjectOrientedProgramming.Lab3.Topics;

public class Topic
{
    private readonly IAddressee _addressee;

    public Topic(IAddressee addressee, string title)
    {
        _addressee = addressee;
        Title = title;
    }

    public string Title { get; }

    public void AddMessage(IMessage message)
    {
        _addressee.ReceiveMessage(message);
    }
}