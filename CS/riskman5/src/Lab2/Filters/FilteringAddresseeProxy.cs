﻿using Itmo.ObjectOrientedProgramming.Lab3.Addressees;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;

namespace Itmo.ObjectOrientedProgramming.Lab3.Filters;

public class FilteringAddresseeProxy : IAddressee
{
    private readonly IAddressee _addressee;
    private readonly IFilter _filter;

    internal FilteringAddresseeProxy(IAddressee addressee, IFilter filter)
    {
        _addressee = addressee;
        _filter = filter;
    }

    public void ReceiveMessage(IMessage message)
    {
        if (_filter.ImportanceLevels.Contains(message.ImportanceLevel.Level))
            _addressee.ReceiveMessage(message);
    }
}