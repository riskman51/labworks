﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;

namespace Itmo.ObjectOrientedProgramming.Lab3.Filters;

public class Filter : IFilter
{
    public Filter(HashSet<ImportanceLevel> importanceLevels)
    {
        ImportanceLevels = new HashSet<int>();
        foreach (ImportanceLevel importanceLevel in importanceLevels)
            ImportanceLevels.Add(importanceLevel.Level);
    }

    public HashSet<int> ImportanceLevels { get; }
}