﻿using System.Collections.Generic;
namespace Itmo.ObjectOrientedProgramming.Lab3.Filters;

public interface IFilter
{
    HashSet<int> ImportanceLevels { get; }
}