﻿using System;

namespace Itmo.ObjectOrientedProgramming.Lab3.Messengers;

public class Messenger
{
    private readonly string _name;

    public Messenger(string name)
    {
        _name = name;
    }

    public void Print(string message)
    {
        Console.WriteLine($"Messenger {_name} : {message}");
    }
}