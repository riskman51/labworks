﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;

namespace Itmo.ObjectOrientedProgramming.Lab3.Users;

public class User
{
    public User()
    {
        Messages = new List<ReadableMessageDecorator>();
    }

    public IList<ReadableMessageDecorator> Messages { get; }
}