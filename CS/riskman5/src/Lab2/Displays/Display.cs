﻿using Itmo.ObjectOrientedProgramming.Lab3.DisplayDrivers;

namespace Itmo.ObjectOrientedProgramming.Lab3.Displays;

public class Display : IDisplay
{
    public Display(IDisplayDriver displayDriver)
    {
        DisplayDriver = displayDriver;
    }

    public IDisplayDriver DisplayDriver { get; set; }

    public void Print(string text)
    {
        DisplayDriver.ClearOutput();
        DisplayDriver.WriteText(text);
    }
}