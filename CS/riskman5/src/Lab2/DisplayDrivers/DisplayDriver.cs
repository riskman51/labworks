﻿using System;
using System.Drawing;

namespace Itmo.ObjectOrientedProgramming.Lab3.DisplayDrivers;

public class DisplayDriver : IDisplayDriver
{
    private Color _color;

    public DisplayDriver(Color color)
    {
        _color = color;
    }

    public void SetColor(Color color)
    {
        _color = color;
    }

    public void ClearOutput()
    {
        Console.Clear();
    }

    public void WriteText(string text)
    {
        Console.WriteLine(Crayon.Output.Rgb(_color.R, _color.G, _color.B).Text(text));
    }
}