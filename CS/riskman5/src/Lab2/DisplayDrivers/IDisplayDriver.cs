﻿using System.Drawing;

namespace Itmo.ObjectOrientedProgramming.Lab3.DisplayDrivers;

public interface IDisplayDriver
{
    void SetColor(Color color);

    void ClearOutput();

    void WriteText(string text);
}