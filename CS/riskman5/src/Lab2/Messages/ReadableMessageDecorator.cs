﻿using Itmo.ObjectOrientedProgramming.Lab3.Messages.ReadingStatuses;
using Itmo.ObjectOrientedProgramming.Lab3.Messages.ReadResults;

namespace Itmo.ObjectOrientedProgramming.Lab3.Messages;

public class ReadableMessageDecorator : IMessage
{
    private readonly IMessage _message;

    public ReadableMessageDecorator(IMessage message)
    {
        _message = message;
        ReadingStatus = new ReadingStatus.NotRead();
    }

    public ReadingStatus ReadingStatus { get; private set; }

    public ImportanceLevel ImportanceLevel => _message.ImportanceLevel;

    public ReadResult ReadMessage()
    {
        if (ReadingStatus is ReadingStatus.Read)
            return new ReadResult.Fail();

        ReadingStatus = new ReadingStatus.Read();
        return new ReadResult.Success();
    }

    public string Render() => _message.Render();
}