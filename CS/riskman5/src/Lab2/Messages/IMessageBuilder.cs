﻿namespace Itmo.ObjectOrientedProgramming.Lab3.Messages;

public interface IMessageBuilder
{
    IMessageBuilder WithTitle(string title);

    IMessageBuilder WithBody(string body);

    IMessageBuilder WithImportanceLevel(ImportanceLevel importanceLevel);

    Message Build();
}