﻿namespace Itmo.ObjectOrientedProgramming.Lab3.Messages.ReadingStatuses;

public abstract record ReadingStatus()
{
    public sealed record Read() : ReadingStatus;

    public sealed record NotRead() : ReadingStatus;
}