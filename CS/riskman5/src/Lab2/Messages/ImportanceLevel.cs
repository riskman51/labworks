﻿using System;

namespace Itmo.ObjectOrientedProgramming.Lab3.Messages;

public class ImportanceLevel
{
    private int _level;

    public ImportanceLevel(int level)
    {
        if (level < 0)
            throw new ArgumentException("Importance level can't be under the zero");
        _level = level;
    }

    public int Level
    {
        get => _level;
        set
        {
            if (value < 0)
                throw new ArgumentException("Importance level can't be under the zero");
            _level = value;
        }
    }
}