﻿using System;

namespace Itmo.ObjectOrientedProgramming.Lab3.Messages;

public class MessageBuilder : IMessageBuilder
{
    private string? _title;
    private string? _body;
    private ImportanceLevel? _importanceLevel;

    public IMessageBuilder WithTitle(string title)
    {
        _title = title;

        return this;
    }

    public IMessageBuilder WithBody(string body)
    {
        _body = body;

        return this;
    }

    public IMessageBuilder WithImportanceLevel(ImportanceLevel importanceLevel)
    {
        _importanceLevel = importanceLevel;

        return this;
    }

    public Message Build()
    {
        return new Message(
            _title ?? throw new ArgumentNullException(nameof(_title)),
            _body ?? throw new ArgumentNullException(nameof(_body)),
            _importanceLevel ?? throw new ArgumentNullException(nameof(_importanceLevel)));
    }
}