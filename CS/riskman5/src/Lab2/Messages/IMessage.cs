﻿namespace Itmo.ObjectOrientedProgramming.Lab3.Messages;

public interface IMessage : IRenderable
{
    public ImportanceLevel ImportanceLevel { get; }
}