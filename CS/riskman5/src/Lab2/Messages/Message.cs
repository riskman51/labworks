﻿using System.Text;

namespace Itmo.ObjectOrientedProgramming.Lab3.Messages;

public class Message : IMessage
{
    internal Message(string title, string body, ImportanceLevel importanceLevel)
    {
        Title = title;
        Body = body;
        ImportanceLevel = importanceLevel;
    }

    public static IMessageBuilder Builder => new MessageBuilder();

    public string Title { get; set; }

    public string Body { get; set; }

    public ImportanceLevel ImportanceLevel { get; set; }

    public string Render()
    {
        var stringBuilder = new StringBuilder();

        stringBuilder.Append(Title);
        stringBuilder.Append('\n');
        stringBuilder.Append(Body);

        return stringBuilder.ToString();
    }
}