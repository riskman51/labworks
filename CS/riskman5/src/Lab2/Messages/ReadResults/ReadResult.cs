﻿namespace Itmo.ObjectOrientedProgramming.Lab3.Messages.ReadResults;

public abstract record ReadResult
{
    public sealed record Success : ReadResult;

    public sealed record Fail : ReadResult;
}