﻿using Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;
using Itmo.ObjectOrientedProgramming.Lab3.Users;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees;

public class UserAdapter : IAddressee
{
    private readonly User _user;

    internal UserAdapter(User user)
    {
        _user = user;
    }

    public static IUserAdapterBuilder Builder => new UserAdapterBuilder();

    public void ReceiveMessage(IMessage message)
    {
        _user.Messages.Add(new ReadableMessageDecorator(message));
    }
}