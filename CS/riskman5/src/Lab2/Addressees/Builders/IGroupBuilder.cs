﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

public interface IGroupBuilder
{
    IModifiersBuilder WithAddresses(IEnumerable<IAddressee> addressees);
}