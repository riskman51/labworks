﻿using System;
using Itmo.ObjectOrientedProgramming.Lab3.Displays;
using Itmo.ObjectOrientedProgramming.Lab3.Filters;
using Itmo.ObjectOrientedProgramming.Lab3.Loggers;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

internal class DisplayAdapterBuilder : IDisplayAdapterBuilder, IModifiersBuilder
{
    private IDisplay? _display;
    private Filter? _filter;
    private ILogger? _logger;

    public IModifiersBuilder WithDisplay(IDisplay display)
    {
        _display = display;

        return this;
    }

    public IModifiersBuilder WithFilter(Filter filter)
    {
        _filter = filter;

        return this;
    }

    public IModifiersBuilder WithLogger(ILogger logger)
    {
        _logger = logger;

        return this;
    }

    public IAddressee Build()
    {
        if (_display is null)
            throw new ArgumentNullException(nameof(_display));

        IAddressee displayAdapter = new DisplayAdapter(_display);

        if (_logger is not null)
            displayAdapter = new LoggingAddresseeDecorator(displayAdapter, _logger);

        if (_filter is not null)
            displayAdapter = new FilteringAddresseeProxy(displayAdapter, _filter);

        return displayAdapter;
    }
}