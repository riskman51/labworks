﻿using System;
using Itmo.ObjectOrientedProgramming.Lab3.Filters;
using Itmo.ObjectOrientedProgramming.Lab3.Loggers;
using Itmo.ObjectOrientedProgramming.Lab3.Users;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

internal class UserAdapterBuilder : IUserAdapterBuilder, IModifiersBuilder
{
    private User? _user;
    private Filter? _filter;
    private ILogger? _logger;

    public IModifiersBuilder WithUser(User user)
    {
        _user = user;

        return this;
    }

    public IModifiersBuilder WithFilter(Filter filter)
    {
        _filter = filter;

        return this;
    }

    public IModifiersBuilder WithLogger(ILogger logger)
    {
        _logger = logger;

        return this;
    }

    public IAddressee Build()
    {
        if (_user is null)
            throw new ArgumentNullException(nameof(_user));

        IAddressee userAdapter = new UserAdapter(_user);

        if (_logger is not null)
            userAdapter = new LoggingAddresseeDecorator(userAdapter, _logger);

        if (_filter is not null)
            userAdapter = new FilteringAddresseeProxy(userAdapter, _filter);

        return userAdapter;
    }
}