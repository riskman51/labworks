﻿using Itmo.ObjectOrientedProgramming.Lab3.Displays;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

public interface IDisplayAdapterBuilder
{
    IModifiersBuilder WithDisplay(IDisplay display);
}