﻿using Itmo.ObjectOrientedProgramming.Lab3.Users;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

public interface IUserAdapterBuilder
{
    IModifiersBuilder WithUser(User user);
}