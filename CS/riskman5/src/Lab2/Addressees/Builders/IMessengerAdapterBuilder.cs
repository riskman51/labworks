﻿using Itmo.ObjectOrientedProgramming.Lab3.Messengers;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

public interface IMessengerAdapterBuilder
{
    IModifiersBuilder WithMessenger(Messenger messenger);
}