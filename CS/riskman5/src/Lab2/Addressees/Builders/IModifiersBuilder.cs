﻿using Itmo.ObjectOrientedProgramming.Lab3.Filters;
using Itmo.ObjectOrientedProgramming.Lab3.Loggers;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

public interface IModifiersBuilder
{
    IModifiersBuilder WithFilter(Filter filter);

    IModifiersBuilder WithLogger(ILogger logger);

    IAddressee Build();
}