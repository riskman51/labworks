﻿using System;
using Itmo.ObjectOrientedProgramming.Lab3.Filters;
using Itmo.ObjectOrientedProgramming.Lab3.Loggers;
using Itmo.ObjectOrientedProgramming.Lab3.Messengers;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

internal class MessengerAdapterBuilder : IMessengerAdapterBuilder, IModifiersBuilder
{
    private Messenger? _messenger;
    private Filter? _filter;
    private ILogger? _logger;

    public IModifiersBuilder WithMessenger(Messenger messenger)
    {
        _messenger = messenger;

        return this;
    }

    public IModifiersBuilder WithFilter(Filter filter)
    {
        _filter = filter;

        return this;
    }

    public IModifiersBuilder WithLogger(ILogger logger)
    {
        _logger = logger;

        return this;
    }

    public IAddressee Build()
    {
        if (_messenger is null)
            throw new ArgumentNullException(nameof(_messenger));

        IAddressee messengerAdapter = new MessengerAdapter(_messenger);

        if (_logger is not null)
            messengerAdapter = new LoggingAddresseeDecorator(messengerAdapter, _logger);

        if (_filter is not null)
            messengerAdapter = new FilteringAddresseeProxy(messengerAdapter, _filter);

        return messengerAdapter;
    }
}