﻿using System;
using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab3.Filters;
using Itmo.ObjectOrientedProgramming.Lab3.Loggers;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;

internal class GroupBuilder : IGroupBuilder, IModifiersBuilder
{
    private IEnumerable<IAddressee>? _addressees;
    private Filter? _filter;
    private ILogger? _logger;

    public IModifiersBuilder WithAddresses(IEnumerable<IAddressee> addressees)
    {
        _addressees = addressees;

        return this;
    }

    public IModifiersBuilder WithFilter(Filter filter)
    {
        _filter = filter;

        return this;
    }

    public IModifiersBuilder WithLogger(ILogger logger)
    {
        _logger = logger;

        return this;
    }

    public IAddressee Build()
    {
        if (_addressees is null)
            throw new ArgumentNullException(nameof(_addressees));

        IAddressee group = new Group(_addressees);

        if (_logger is not null)
            group = new LoggingAddresseeDecorator(group, _logger);

        if (_filter is not null)
            group = new FilteringAddresseeProxy(group, _filter);

        return group;
    }
}