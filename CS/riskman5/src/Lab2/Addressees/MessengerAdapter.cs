﻿using Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;
using Itmo.ObjectOrientedProgramming.Lab3.Messengers;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees;

public class MessengerAdapter : IAddressee
{
    private readonly Messenger _messenger;

    internal MessengerAdapter(Messenger messenger)
    {
        _messenger = messenger;
    }

    public static IMessengerAdapterBuilder Builder => new MessengerAdapterBuilder();

    public void ReceiveMessage(IMessage message)
    {
        _messenger.Print(message.Render());
    }
}