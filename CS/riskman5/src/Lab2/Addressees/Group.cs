﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees;

public class Group : IAddressee
{
    private readonly IEnumerable<IAddressee> _addressees;

    internal Group(IEnumerable<IAddressee> addressees)
    {
        _addressees = addressees;
    }

    public static IGroupBuilder Builder => new GroupBuilder();

    public void ReceiveMessage(IMessage message)
    {
        foreach (IAddressee addressee in _addressees)
            addressee.ReceiveMessage(message);
    }
}