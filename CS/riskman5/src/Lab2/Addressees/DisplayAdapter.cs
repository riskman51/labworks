﻿using Itmo.ObjectOrientedProgramming.Lab3.Addressees.Builders;
using Itmo.ObjectOrientedProgramming.Lab3.Displays;
using Itmo.ObjectOrientedProgramming.Lab3.Messages;

namespace Itmo.ObjectOrientedProgramming.Lab3.Addressees;

public class DisplayAdapter : IAddressee
{
    private readonly IDisplay _display;

    internal DisplayAdapter(IDisplay display)
    {
        _display = display;
    }

    public static IDisplayAdapterBuilder Builder => new DisplayAdapterBuilder();

    public void ReceiveMessage(IMessage message)
    {
        _display.Print(message.Render());
    }
}