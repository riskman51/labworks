﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Itmo.ObjectOrientedProgramming.Lab4.Paths;

public class SystemAdaptivePath : IPath
{
    public SystemAdaptivePath(IPath absolutePath, IPath relativePath)
    {
        PathParts = absolutePath.PathParts.Concat(relativePath.PathParts);
    }

    public SystemAdaptivePath(string path)
    {
        PathParts = path.Split(Path.DirectorySeparatorChar);
    }

    public IEnumerable<string> PathParts { get; }
}