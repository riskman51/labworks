﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab4.Paths;

public interface IPath
{
    IEnumerable<string> PathParts { get; }
}