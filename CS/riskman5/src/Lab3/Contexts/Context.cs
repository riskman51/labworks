﻿using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;
using Itmo.ObjectOrientedProgramming.Lab4.Writers;

namespace Itmo.ObjectOrientedProgramming.Lab4.Contexts;

public class Context
{
    public IPath? CurrentPath { get; set; }

    public IFileSystem? CurrentFileSystem { get; set; }

    public IWriter? CurrentWriter { get; set; }
}