﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands;
using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.DisconnectHandlers;

public class DisconnectHandler : BaseMajorChainLink
{
    public override CommandBuildResultType Handle(Request.Iterator iterator)
    {
        if (iterator.Current() != "disconnect")
        {
            return Next is not null
                ? Next.Handle(iterator)
                : new CommandBuildResultType.Failure("unsupported command");
        }

        return new CommandBuildResultType.Success(new DisconnectCommand());
    }
}