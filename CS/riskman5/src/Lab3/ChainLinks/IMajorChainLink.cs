﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks;

public interface IMajorChainLink
{
    CommandBuildResultType Handle(Request.Iterator iterator);

    IMajorChainLink AddNext(IMajorChainLink majorChainLink);
}