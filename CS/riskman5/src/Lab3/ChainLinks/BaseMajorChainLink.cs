﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks;

public abstract class BaseMajorChainLink : IMajorChainLink
{
    protected IMajorChainLink? Next { get; set; }

    public abstract CommandBuildResultType Handle(Request.Iterator iterator);

    public IMajorChainLink AddNext(IMajorChainLink majorChainLink)
    {
        if (Next is null)
            Next = majorChainLink;
        else
            Next.AddNext(majorChainLink);

        return this;
    }
}