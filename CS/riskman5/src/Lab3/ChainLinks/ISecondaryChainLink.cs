﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks;

public interface ISecondaryChainLink<T>
{
    CommandBuildResultType.Failure? Handle(Request.Iterator iterator, T builder);

    ISecondaryChainLink<T> AddNext(ISecondaryChainLink<T> secondaryChainLink);
}