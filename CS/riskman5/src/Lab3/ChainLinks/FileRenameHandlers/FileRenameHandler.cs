﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileRenameHandlers;

public class FileRenameHandler : BaseMajorChainLink
{
    private ISecondaryChainLink<FileRenameCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType Handle(Request.Iterator iterator)
    {
        string? fullCommand = iterator.CurrentMany(2, ' ');
        if (fullCommand != "file rename")
        {
            return Next is not null
                ? Next.Handle(iterator)
                : new CommandBuildResultType.Failure("Unknown command");
        }

        var fileRenameCommandBuilder = new FileRenameCommandBuilder();

        if (!iterator.MoveNext() || _secondaryChainLinkHead is null)
            return fileRenameCommandBuilder.Build();

        CommandBuildResultType? result = _secondaryChainLinkHead.Handle(iterator, fileRenameCommandBuilder);

        return result is CommandBuildResultType.Failure
            ? result
            : fileRenameCommandBuilder.Build();
    }

    public FileRenameHandler AddNextSecondaryChainLink(ISecondaryChainLink<FileRenameCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}