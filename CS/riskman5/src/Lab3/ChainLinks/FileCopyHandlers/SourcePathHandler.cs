﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileCopyHandlers;

public class SourcePathHandler : BaseSecondaryChainLink<FileCopyCommandBuilder>
{
    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, FileCopyCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return new CommandBuildResultType.Failure("No necessary parameter");
        builder.WithSourcePath(new SystemAdaptivePath(iterator.Current()));
        return Next?.Handle(iterator, builder);
    }
}