﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks;

public abstract class BaseSecondaryChainLink<T> : ISecondaryChainLink<T>
{
    protected ISecondaryChainLink<T>? Next { get; set; }

    public abstract CommandBuildResultType.Failure? Handle(Request.Iterator iterator, T builder);

    public ISecondaryChainLink<T> AddNext(ISecondaryChainLink<T> secondaryChainLink)
    {
        if (Next is null)
            Next = secondaryChainLink;
        else
            Next.AddNext(secondaryChainLink);

        return this;
    }
}