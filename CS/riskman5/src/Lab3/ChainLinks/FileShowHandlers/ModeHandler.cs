﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileShowHandlers;

public class ModeHandler : BaseSecondaryChainLink<FileShowCommandBuilder>
{
    private ISecondaryChainLink<FileShowCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, FileShowCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return null;
        return iterator.Current() != "-m"
            ? Next?.Handle(iterator, builder)
            : _secondaryChainLinkHead?.Handle(iterator, builder);
    }

    public ModeHandler AddNextSecondaryChainLink(ISecondaryChainLink<FileShowCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}