﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Modes;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileShowHandlers;

public class ConsoleHandler : BaseSecondaryChainLink<FileShowCommandBuilder>
{
    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, FileShowCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return null;

        if (iterator.Current() != "console")
        {
            return Next is not null
                ? Next.Handle(iterator, builder)
                : new CommandBuildResultType.Failure("Unknown mode");
        }

        builder.WithMode(new WriterMode.Console());
        return null;
    }
}