﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileDeleteHandlers;

public class FileDeleteHandler : BaseMajorChainLink
{
    private ISecondaryChainLink<FileDeleteCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType Handle(Request.Iterator iterator)
    {
        string? fullCommand = iterator.CurrentMany(2, ' ');
        if (fullCommand != "file delete")
        {
            return Next is not null
                ? Next.Handle(iterator)
                : new CommandBuildResultType.Failure("Unknown command");
        }

        var fileDeleteCommandBuilder = new FileDeleteCommandBuilder();

        if (!iterator.MoveNext() || _secondaryChainLinkHead is null)
            return fileDeleteCommandBuilder.Build();

        CommandBuildResultType? result = _secondaryChainLinkHead.Handle(iterator, fileDeleteCommandBuilder);

        return result is CommandBuildResultType.Failure
            ? result
            : fileDeleteCommandBuilder.Build();
    }

    public FileDeleteHandler AddNextSecondaryChainLink(ISecondaryChainLink<FileDeleteCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}