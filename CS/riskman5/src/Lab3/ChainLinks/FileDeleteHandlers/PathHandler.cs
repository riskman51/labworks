﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileDeleteHandlers;

public class PathHandler : BaseSecondaryChainLink<FileDeleteCommandBuilder>
{
    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, FileDeleteCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return new CommandBuildResultType.Failure("No necessary parameter");
        builder.WithPath(new SystemAdaptivePath(iterator.Current()));
        return Next?.Handle(iterator, builder);
    }
}