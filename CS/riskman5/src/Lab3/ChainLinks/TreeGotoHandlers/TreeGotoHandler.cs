﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.TreeGotoHandlers;

public class TreeGotoHandler : BaseMajorChainLink
{
    private ISecondaryChainLink<TreeGotoCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType Handle(Request.Iterator iterator)
    {
        string? fullCommand = iterator.CurrentMany(2, ' ');
        if (fullCommand != "tree goto")
        {
            return Next is not null
                ? Next.Handle(iterator)
                : new CommandBuildResultType.Failure("Unknown command");
        }

        var treeGotoCommandBuilder = new TreeGotoCommandBuilder();

        if (!iterator.MoveNext() || _secondaryChainLinkHead is null)
            return treeGotoCommandBuilder.Build();

        CommandBuildResultType? result = _secondaryChainLinkHead.Handle(iterator, treeGotoCommandBuilder);

        return result is CommandBuildResultType.Failure
            ? result
            : treeGotoCommandBuilder.Build();
    }

    public TreeGotoHandler AddNextSecondaryChainLink(ISecondaryChainLink<TreeGotoCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}