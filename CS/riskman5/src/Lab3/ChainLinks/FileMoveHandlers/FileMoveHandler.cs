﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileMoveHandlers;

public class FileMoveHandler : BaseMajorChainLink
{
    private ISecondaryChainLink<FileMoveCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType Handle(Request.Iterator iterator)
    {
        string? fullCommand = iterator.CurrentMany(2, ' ');
        if (fullCommand != "file move")
        {
            return Next is not null
                ? Next.Handle(iterator)
                : new CommandBuildResultType.Failure("Unknown command");
        }

        var fileMoveCommandBuilder = new FileMoveCommandBuilder();

        if (!iterator.MoveNext() || _secondaryChainLinkHead is null)
            return fileMoveCommandBuilder.Build();

        CommandBuildResultType? result = _secondaryChainLinkHead.Handle(iterator, fileMoveCommandBuilder);

        return result is CommandBuildResultType.Failure
            ? result
            : fileMoveCommandBuilder.Build();
    }

    public FileMoveHandler AddNextSecondaryChainLink(ISecondaryChainLink<FileMoveCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}