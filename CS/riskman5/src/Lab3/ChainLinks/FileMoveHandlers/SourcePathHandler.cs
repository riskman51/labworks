﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileMoveHandlers;

public class SourcePathHandler : BaseSecondaryChainLink<FileMoveCommandBuilder>
{
    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, FileMoveCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return new CommandBuildResultType.Failure("No necessary parameter");
        builder.WithSourcePath(new SystemAdaptivePath(iterator.Current()));
        return Next?.Handle(iterator, builder);
    }
}