﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.TreeListHandlers;

public class TreeListHandler : BaseMajorChainLink
{
    private ISecondaryChainLink<TreeListCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType Handle(Request.Iterator iterator)
    {
        string? fullCommand = iterator.CurrentMany(2, ' ');
        if (fullCommand != "tree list")
        {
            return Next is not null
                ? Next.Handle(iterator)
                : new CommandBuildResultType.Failure("Unknown command");
        }

        var treeListCommandBuilder = new TreeListCommandBuilder();

        if (!iterator.MoveNext() || _secondaryChainLinkHead is null)
            return treeListCommandBuilder.Build();

        CommandBuildResultType? result = _secondaryChainLinkHead.Handle(iterator, treeListCommandBuilder);

        return result is CommandBuildResultType.Failure
            ? result
            : treeListCommandBuilder.Build();
    }

    public TreeListHandler AddNextSecondaryChainLink(ISecondaryChainLink<TreeListCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}