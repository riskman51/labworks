﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.TreeListHandlers;

public class DepthAmountHandler : BaseSecondaryChainLink<TreeListCommandBuilder>
{
    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, TreeListCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return null;

        if (!int.TryParse(iterator.Current(), out int depth))
            return new CommandBuildResultType.Failure("depth must be integer");

        builder.WithDepth(depth);
        return null;
    }
}