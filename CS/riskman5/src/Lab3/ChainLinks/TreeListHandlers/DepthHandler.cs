﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.TreeListHandlers;

public class DepthHandler : BaseSecondaryChainLink<TreeListCommandBuilder>
{
    private ISecondaryChainLink<TreeListCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, TreeListCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return null;

        return iterator.Current() != "-d"
            ? Next?.Handle(iterator, builder)
            : _secondaryChainLinkHead?.Handle(iterator, builder);
    }

    public DepthHandler AddNextSecondaryChainLink(ISecondaryChainLink<TreeListCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}