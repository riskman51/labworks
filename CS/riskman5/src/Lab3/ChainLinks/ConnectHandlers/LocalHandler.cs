﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Modes;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.ConnectHandlers;

public class LocalHandler : BaseSecondaryChainLink<ConnectCommandBuilder>
{
    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, ConnectCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return new CommandBuildResultType.Failure("No necessary parameter");

        if (iterator.Current() != "local")
        {
            return Next is not null
                ? Next.Handle(iterator, builder)
                : new CommandBuildResultType.Failure("Unknown mode");
        }

        builder.WithMode(new FileSystemMode.Local());
        return null;
    }
}