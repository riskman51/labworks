﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.ConnectHandlers;

public class ModeHandler : BaseSecondaryChainLink<ConnectCommandBuilder>
{
    private ISecondaryChainLink<ConnectCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType.Failure? Handle(Request.Iterator iterator, ConnectCommandBuilder builder)
    {
        if (!iterator.MoveNext())
            return new CommandBuildResultType.Failure("No necessary parameter");

        return iterator.Current() != "-m"
            ? Next?.Handle(iterator, builder)
            : _secondaryChainLinkHead?.Handle(iterator, builder);
    }

    public ModeHandler AddNextSecondaryChainLink(ISecondaryChainLink<ConnectCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}