﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;

namespace Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.ConnectHandlers;

public class ConnectHandler : BaseMajorChainLink
{
    private ISecondaryChainLink<ConnectCommandBuilder>? _secondaryChainLinkHead;

    public override CommandBuildResultType Handle(Request.Iterator iterator)
    {
        if (iterator.Current() != "connect")
        {
            return Next is not null
                ? Next.Handle(iterator)
                : new CommandBuildResultType.Failure("Unknown command");
        }

        var connectCommandBuilder = new ConnectCommandBuilder();

        CommandBuildResultType? result = _secondaryChainLinkHead?.Handle(iterator, connectCommandBuilder);

        return result is CommandBuildResultType.Failure
            ? result
            : connectCommandBuilder.Build();
    }

    public ConnectHandler AddNextSecondaryChainLink(ISecondaryChainLink<ConnectCommandBuilder> next)
    {
        if (_secondaryChainLinkHead is not null)
            _secondaryChainLinkHead.AddNext(next);
        else
            _secondaryChainLinkHead = next;

        return this;
    }
}