﻿namespace Itmo.ObjectOrientedProgramming.Lab4.Modes;

public abstract record WriterMode()
{
    public sealed record Console() : WriterMode;
}