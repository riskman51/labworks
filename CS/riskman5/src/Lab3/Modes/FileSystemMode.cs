﻿namespace Itmo.ObjectOrientedProgramming.Lab4.Modes;

public abstract record FileSystemMode()
{
    public sealed record Local() : FileSystemMode;
}