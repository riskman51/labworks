﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab4.Requests;

public class Request
{
    private readonly IReadOnlyList<string> _arguments;

    public Request(string request)
    {
       _arguments = request.Split();
    }

    public Iterator GetIterator => new Iterator(_arguments);

    public class Iterator
    {
        private IReadOnlyList<string> _list;
        private int _position;

        public Iterator(IReadOnlyList<string> list)
        {
            _list = list;
            _position = 0;
        }

        public bool MoveNext(int count = 1)
        {
            if (_position + count >= _list.Count)
                return false;
            _position += count;
            return true;
        }

        public string Current()
        {
            return _list[_position];
        }

        public string? CurrentMany(int count, char separateValue)
        {
            if (_position + count > _list.Count)
                return null;

            string currentMany = string.Empty;
            for (int i = 0; i < _position + count; i++)
                currentMany += _list[_position + i] + separateValue;

            return currentMany[..^1];
        }
    }
}