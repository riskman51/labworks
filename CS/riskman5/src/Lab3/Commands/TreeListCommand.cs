﻿using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems.FileSystemComponents;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands;

public class TreeListCommand : ICommand
{
    private readonly int _depth;

    public TreeListCommand(int depth)
    {
        _depth = depth;
    }

    public FileSystemOperationResultType Execute(Context context)
    {
        if (context.CurrentFileSystem is null || context.CurrentPath is null)
            return new FileSystemOperationResultType.Failure("File system is not connected");

        IFileSystemComponent? fileSystemComponent = context.CurrentFileSystem.TreeList(_depth, context.CurrentPath);
        if (fileSystemComponent is null)
            return new FileSystemOperationResultType.Failure("File system is not connected");

        var visitor = new Visitor(_depth);
        fileSystemComponent.Accept(visitor);
        return new FileSystemOperationResultType.Success();
    }
}