﻿using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands;

public class TreeGotoCommand : ICommand
{
    private readonly IPath _path;

    public TreeGotoCommand(IPath path)
    {
        _path = path;
    }

    public FileSystemOperationResultType Execute(Context context)
    {
        if (context.CurrentFileSystem is null || context.CurrentPath is null)
            return new FileSystemOperationResultType.Failure("File system is not connected");

        IPath? path = context.CurrentFileSystem.FindDirectoryAbsolutePath(context.CurrentPath, _path);
        if (path is null)
            return new FileSystemOperationResultType.Failure("Path wasn't found");
        context.CurrentPath = path;
        return new FileSystemOperationResultType.Success();
    }
}