﻿using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands;

public class FileRenameCommand : ICommand
{
    private readonly IPath _path;
    private readonly string _name;

    public FileRenameCommand(IPath path, string name)
    {
        _path = path;
        _name = name;
    }

    public FileSystemOperationResultType Execute(Context context)
    {
        if (context.CurrentFileSystem is null
            || context.CurrentPath is null)
            return new FileSystemOperationResultType.Failure("File system is not connected");

        IPath? absolutePath = context.CurrentFileSystem.FindFileAbsolutePath(context.CurrentPath, _path);

        if (absolutePath is null)
            return new FileSystemOperationResultType.Failure("Path was not found");
        context.CurrentFileSystem.FileRename(absolutePath, _name);
        return new FileSystemOperationResultType.Success();
    }
}