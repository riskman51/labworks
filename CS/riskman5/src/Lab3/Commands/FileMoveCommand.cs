﻿using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands;

public class FileMoveCommand : ICommand
{
    private readonly IPath _sourcePath;
    private readonly IPath _destinationPath;

    public FileMoveCommand(IPath sourcePath, IPath destinationPath)
    {
        _sourcePath = sourcePath;
        _destinationPath = destinationPath;
    }

    public FileSystemOperationResultType Execute(Context context)
    {
        if (context.CurrentFileSystem is null
            || context.CurrentPath is null)
            return new FileSystemOperationResultType.Failure("File system is not connected");

        IPath? sourceAbsolutePath = context.CurrentFileSystem.FindFileAbsolutePath(context.CurrentPath, _sourcePath);
        IPath? destinationAbsolutePath = context.CurrentFileSystem.FindFileAbsolutePath(context.CurrentPath, _destinationPath);

        if (sourceAbsolutePath is null || destinationAbsolutePath is null)
            return new FileSystemOperationResultType.Failure("Path was not found");
        context.CurrentFileSystem.FileMove(sourceAbsolutePath, destinationAbsolutePath);
        return new FileSystemOperationResultType.Success();
    }
}