﻿using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.Modes;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;
using Itmo.ObjectOrientedProgramming.Lab4.Writers;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands;

public class FileShowCommand : ICommand
{
    private readonly IPath _path;
    private readonly WriterMode _mode;

    public FileShowCommand(IPath path, WriterMode mode)
    {
        _path = path;
        _mode = mode;
    }

    public FileSystemOperationResultType Execute(Context context)
    {
        if (context.CurrentFileSystem is null || context.CurrentPath is null)
            return new FileSystemOperationResultType.Failure("File system is not connected");

        if (_mode is not WriterMode.Console)
            return new FileSystemOperationResultType.Failure("not supported mode");

        IPath? absolutePath = context.CurrentFileSystem.FindFileAbsolutePath(context.CurrentPath, _path);
        if (absolutePath is null)
            return new FileSystemOperationResultType.Failure("Path was not found");

        var writer = new ConsoleWriter();
        writer.Write(context.CurrentFileSystem.FileShow(absolutePath));

        return new FileSystemOperationResultType.Success();
    }
}