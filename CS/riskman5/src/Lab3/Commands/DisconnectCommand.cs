﻿using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands;

public class DisconnectCommand : ICommand
{
    public FileSystemOperationResultType Execute(Context context)
    {
        if (context.CurrentFileSystem is null)
            return new FileSystemOperationResultType.Failure("File system is not connected");

        context.CurrentFileSystem = null;
        context.CurrentPath = null;
        return new FileSystemOperationResultType.Success();
    }
}