﻿using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;

public class TreeGotoCommandBuilder
{
    private IPath? _path;

    public TreeGotoCommandBuilder WithPath(IPath path)
    {
        _path = path;

        return this;
    }

    public void ClearArguments()
    {
        _path = null;
    }

    public CommandBuildResultType Build()
    {
        if (_path is null)
            return new CommandBuildResultType.Failure("Not enough arguments");
        return new CommandBuildResultType.Success(
            new TreeGotoCommand(_path));
    }
}