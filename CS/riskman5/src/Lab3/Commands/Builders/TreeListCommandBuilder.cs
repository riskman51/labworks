﻿namespace Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;

public class TreeListCommandBuilder
{
    private int _depth;

    public TreeListCommandBuilder()
    {
        _depth = 1;
    }

    public TreeListCommandBuilder WithDepth(int depth)
    {
        _depth = depth;

        return this;
    }

    public CommandBuildResultType Build()
    {
        return new CommandBuildResultType.Success(
            new TreeListCommand(_depth));
    }
}