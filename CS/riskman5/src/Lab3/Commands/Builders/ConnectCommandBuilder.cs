﻿using Itmo.ObjectOrientedProgramming.Lab4.Modes;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;

public class ConnectCommandBuilder
{
    private IPath? _path;
    private FileSystemMode? _mode;

    public ConnectCommandBuilder WithPath(IPath path)
    {
        _path = path;

        return this;
    }

    public ConnectCommandBuilder WithMode(FileSystemMode fileSystemMode)
    {
        _mode = fileSystemMode;

        return this;
    }

    public CommandBuildResultType Build()
    {
        if (_path is null || _mode is null)
            return new CommandBuildResultType.Failure("not enough arguments");
        return new CommandBuildResultType.Success(
            new ConnectCommand(_path, _mode));
    }
}