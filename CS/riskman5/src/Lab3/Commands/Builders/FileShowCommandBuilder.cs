﻿using Itmo.ObjectOrientedProgramming.Lab4.Modes;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;

public class FileShowCommandBuilder
{
    private IPath? _path;
    private WriterMode _mode;

    public FileShowCommandBuilder()
    {
        _mode = new WriterMode.Console();
    }

    public FileShowCommandBuilder WithPath(IPath path)
    {
        _path = path;

        return this;
    }

    public FileShowCommandBuilder WithMode(WriterMode mode)
    {
        _mode = mode;

        return this;
    }

    public void ClearArguments()
    {
        _path = null;
        _mode = new WriterMode.Console();
    }

    public CommandBuildResultType Build()
    {
        if (_path is null)
            return new CommandBuildResultType.Failure("Not enough arguments");
        return new CommandBuildResultType.Success(
            new FileShowCommand(_path, _mode));
    }
}