﻿using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;

public class FileMoveCommandBuilder
{
    private IPath? _sourcePath;
    private IPath? _destinationPath;

    public FileMoveCommandBuilder WithSourcePath(IPath sourcePath)
    {
        _sourcePath = sourcePath;

        return this;
    }

    public FileMoveCommandBuilder WithDestinationPath(IPath destinationPath)
    {
        _destinationPath = destinationPath;

        return this;
    }

    public void ClearArguments()
    {
        _sourcePath = null;
        _destinationPath = null;
    }

    public CommandBuildResultType Build()
    {
        if (_sourcePath is null || _destinationPath is null)
            return new CommandBuildResultType.Failure("Not enough arguments");
        return new CommandBuildResultType.Success(
            new FileMoveCommand(_sourcePath, _destinationPath));
    }
}