﻿namespace Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;

public abstract record CommandBuildResultType()
{
    public sealed record Success(ICommand Command) : CommandBuildResultType;

    public sealed record Failure(string Message) : CommandBuildResultType;
}