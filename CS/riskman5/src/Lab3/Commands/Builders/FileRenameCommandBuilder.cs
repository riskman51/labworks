﻿using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;

public class FileRenameCommandBuilder
{
    private IPath? _path;
    private string? _name;

    public FileRenameCommandBuilder WithPath(IPath path)
    {
        _path = path;

        return this;
    }

    public FileRenameCommandBuilder WithName(string name)
    {
        _name = name;

        return this;
    }

    public void ClearArguments()
    {
        _path = null;
        _name = null;
    }

    public CommandBuildResultType Build()
    {
        if (_path is null || _name is null)
            return new CommandBuildResultType.Failure("Not enough arguments");
        return new CommandBuildResultType.Success(
            new FileRenameCommand(_path, _name));
    }
}