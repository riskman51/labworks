﻿using System.IO;
using System.Linq;
using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.Modes;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands;

public class ConnectCommand : ICommand
{
    private readonly IPath _path;
    private readonly FileSystemMode _fileSystemMode;

    public ConnectCommand(IPath path, FileSystemMode fileSystemMode)
    {
        _path = path;
        _fileSystemMode = fileSystemMode;
    }

    public FileSystemOperationResultType Execute(Context context)
    {
        if (_fileSystemMode is not FileSystemMode.Local)
            return new FileSystemOperationResultType.Failure("Not supported file system mode");

        if (!Directory.Exists(Path.Combine(_path.PathParts.ToArray())))
            return new FileSystemOperationResultType.Failure("Directory not found");

        context.CurrentFileSystem = new LocalFileSystem(_path);
        context.CurrentPath = _path;
        return new FileSystemOperationResultType.Success();
    }
}