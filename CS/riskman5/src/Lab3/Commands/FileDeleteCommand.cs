﻿using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.Commands;

public class FileDeleteCommand : ICommand
{
    private readonly IPath _path;

    public FileDeleteCommand(IPath path)
    {
        _path = path;
    }

    public FileSystemOperationResultType Execute(Context context)
    {
        if (context.CurrentFileSystem is null
            || context.CurrentPath is null)
            return new FileSystemOperationResultType.Failure("File system is not connected");

        IPath? absolutePath = context.CurrentFileSystem.FindFileAbsolutePath(context.CurrentPath, _path);

        if (absolutePath is null)
            return new FileSystemOperationResultType.Failure("Path was not found");
        context.CurrentFileSystem.FileDelete(absolutePath);
        return new FileSystemOperationResultType.Success();
    }
}