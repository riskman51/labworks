﻿using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;

namespace Itmo.ObjectOrientedProgramming.Lab4.Parsers;

public interface IParser
{
    CommandBuildResultType Parse(string request);
}