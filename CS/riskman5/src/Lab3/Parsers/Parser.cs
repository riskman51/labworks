﻿using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.ConnectHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.DisconnectHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileCopyHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileDeleteHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileMoveHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileRenameHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileShowHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.TreeGotoHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.TreeListHandlers;
using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Requests;
using DestinationPathHandler = Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileMoveHandlers.DestinationPathHandler;
using ModeHandler = Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.ConnectHandlers.ModeHandler;
using PathHandler = Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.ConnectHandlers.PathHandler;
using SourcePathHandler = Itmo.ObjectOrientedProgramming.Lab4.ChainLinks.FileMoveHandlers.SourcePathHandler;

namespace Itmo.ObjectOrientedProgramming.Lab4.Parsers;

public class Parser : IParser
{
    private readonly IMajorChainLink _majorChainLinkHead;

    public Parser()
    {
         _majorChainLinkHead = new ConnectHandler()
            .AddNextSecondaryChainLink(
                new PathHandler())
            .AddNextSecondaryChainLink(
                new ModeHandler()
                    .AddNextSecondaryChainLink(
                        new LocalHandler()))
            .AddNext(
                new DisconnectHandler())
            .AddNext(
                new TreeGotoHandler()
                    .AddNextSecondaryChainLink(
                        new ChainLinks.TreeGotoHandlers.PathHandler()))
            .AddNext(
                new TreeListHandler()
                    .AddNextSecondaryChainLink(
                        new DepthHandler()
                            .AddNextSecondaryChainLink(
                                new DepthAmountHandler()))
            .AddNext(
                new FileShowHandler()
                    .AddNextSecondaryChainLink(
                        new ChainLinks.FileShowHandlers.PathHandler())
                    .AddNextSecondaryChainLink(
                        new ChainLinks.FileShowHandlers.ModeHandler()
                            .AddNextSecondaryChainLink(new ChainLinks.FileShowHandlers.ConsoleHandler())))
            .AddNext(
                new FileMoveHandler()
                    .AddNextSecondaryChainLink(
                        new SourcePathHandler())
                    .AddNextSecondaryChainLink(
                        new DestinationPathHandler()))
            .AddNext(
                new FileCopyHandler()
                    .AddNextSecondaryChainLink(
                        new ChainLinks.FileCopyHandlers.SourcePathHandler())
                    .AddNextSecondaryChainLink(
                        new ChainLinks.FileCopyHandlers.DestinationPathHandler()))
            .AddNext(
                new FileDeleteHandler()
                    .AddNextSecondaryChainLink(
                        new ChainLinks.FileDeleteHandlers.PathHandler()))
            .AddNext(
                new FileRenameHandler()
                    .AddNextSecondaryChainLink(
                        new ChainLinks.FileRenameHandlers.PathHandler())
                    .AddNextSecondaryChainLink(
                        new NameHandler())));
    }

    public CommandBuildResultType Parse(string request)
    {
        var newRequest = new Request(request);
        return _majorChainLinkHead.Handle(newRequest.GetIterator);
    }
}