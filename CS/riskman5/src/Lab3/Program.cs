﻿using System;
using Itmo.ObjectOrientedProgramming.Lab4.Commands.Builders;
using Itmo.ObjectOrientedProgramming.Lab4.Contexts;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems;
using Itmo.ObjectOrientedProgramming.Lab4.Parsers;

namespace Itmo.ObjectOrientedProgramming.Lab4;

public static class Program
{
    public static void Main()
    {
        var context = new Context();
        var parser = new Parser();

        while (true)
        {
            string? request = Console.ReadLine();
            if (request is null)
                continue;
            CommandBuildResultType result = parser.Parse(request);

            if (result is CommandBuildResultType.Success successfulRequest)
            {
                if (successfulRequest.Command.Execute(context) is FileSystemOperationResultType.Failure failureOperation)
                    Console.WriteLine(failureOperation.Message);
            }
            else if (result is CommandBuildResultType.Failure failureRequest)
            {
                Console.WriteLine(failureRequest.Message);
            }
        }
    }
}