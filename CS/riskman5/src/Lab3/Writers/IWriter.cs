﻿using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab4.Writers;

public interface IWriter
{
    void Write(IEnumerable<string> text);

    void Write(string str);
}