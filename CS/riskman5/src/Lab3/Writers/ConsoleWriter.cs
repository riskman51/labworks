﻿using System;
using System.Collections.Generic;

namespace Itmo.ObjectOrientedProgramming.Lab4.Writers;

public class ConsoleWriter : IWriter
{
    public void Write(IEnumerable<string> text)
    {
        foreach (string str in text)
            Console.WriteLine(str);
    }

    public void Write(string str)
    {
        Console.WriteLine(str);
    }
}