﻿namespace Itmo.ObjectOrientedProgramming.Lab4.FileSystems;

public abstract record ConnectionResultType()
{
    public sealed record Connected() : ConnectionResultType;

    public sealed record Disconnected() : ConnectionResultType;
}