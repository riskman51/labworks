﻿namespace Itmo.ObjectOrientedProgramming.Lab4.FileSystems;

public abstract record FileSystemOperationResultType
{
    public sealed record Success() : FileSystemOperationResultType;

    public sealed record Failure(string Message) : FileSystemOperationResultType;
}