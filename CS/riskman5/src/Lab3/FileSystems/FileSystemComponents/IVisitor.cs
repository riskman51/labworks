﻿namespace Itmo.ObjectOrientedProgramming.Lab4.FileSystems.FileSystemComponents;

public interface IVisitor
{ }

public interface IVisitor<T> : IVisitor
    where T : IFileSystemComponent
{
    void Visit(T component);
}