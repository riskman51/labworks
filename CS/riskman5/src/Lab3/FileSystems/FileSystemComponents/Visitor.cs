﻿using System;
using System.Linq;

namespace Itmo.ObjectOrientedProgramming.Lab4.FileSystems.FileSystemComponents;

public class Visitor : IVisitor<DirectoryComponent>, IVisitor<FileComponent>
{
    private int _depth;

    public Visitor(int depth)
    {
        _depth = depth;
    }

    public void Visit(DirectoryComponent component)
    {
        Console.WriteLine(string.Concat(Enumerable.Repeat('\t', _depth)) + component.Name);

        _depth += 1;

        foreach (IFileSystemComponent fileSystemComponent in component.FileSystemComponents)
        {
            fileSystemComponent.Accept(this);
        }

        _depth -= 1;
    }

    public void Visit(FileComponent component)
    {
        Console.WriteLine(string.Concat(Enumerable.Repeat('\t', _depth)) + component.Name);
    }
}