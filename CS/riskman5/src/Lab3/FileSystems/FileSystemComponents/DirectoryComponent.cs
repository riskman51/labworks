﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.FileSystems.FileSystemComponents;

public class DirectoryComponent : IFileSystemComponent
{
    private readonly IPath _absolutePath;

    public DirectoryComponent(IPath absolutePath, string name)
    {
        _absolutePath = absolutePath;
        Name = name;
        FileSystemComponents = new List<IFileSystemComponent>();
    }

    public string Name { get; }

    public IList<IFileSystemComponent> FileSystemComponents { get; private set; }

    public void AddSubComponents(int depth, IFileSystem fileSystem)
    {
        if (depth == 0)
            return;
        foreach (IPath absolutePath in fileSystem.GetDirectories(_absolutePath))
        {
            string? directoryName = fileSystem.FindComponentName(absolutePath);
            if (directoryName is null)
                continue;
            var directory = new DirectoryComponent(absolutePath, directoryName);
            directory.AddSubComponents(depth - 1, fileSystem);
            FileSystemComponents.Add(directory);
        }

        foreach (IPath absolutePath in fileSystem.GetFiles(_absolutePath))
        {
            string? fileName = fileSystem.FindComponentName(absolutePath);
            if (fileName is null)
                continue;
            var file = new FileComponent(fileName);
            FileSystemComponents.Add(file);
        }
    }

    public void Accept(IVisitor visitor)
    {
        if (visitor is IVisitor<DirectoryComponent> directoryVisitor)
            directoryVisitor.Visit(this);
    }
}