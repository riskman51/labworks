﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems.FileSystemComponents;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;
using Path = System.IO.Path;

namespace Itmo.ObjectOrientedProgramming.Lab4.FileSystems;

public class LocalFileSystem : IFileSystem
{
    private readonly IPath _systemAbsolutePath;

    public LocalFileSystem(IPath path)
    {
        _systemAbsolutePath = path;
    }

    public IFileSystemComponent? TreeList(int depth, IPath currenPath)
    {
        string? directoryName = FindComponentName(currenPath);
        if (directoryName is null)
            return null;
        var currentDirectory = new DirectoryComponent(currenPath, directoryName);
        currentDirectory.AddSubComponents(depth, this);

        return currentDirectory;
    }

    public IEnumerable<string> FileShow(IPath path)
    {
        return File.ReadLines(Path.Combine(path.PathParts.ToArray()));
    }

    public void FileMove(IPath sourcePath, IPath destinationPath)
    {
        File.Move(Path.Combine(sourcePath.PathParts.ToArray()), Path.Combine(destinationPath.PathParts.ToArray()));
    }

    public void FileCopy(IPath sourcePath, IPath destinationPath)
    {
        File.Copy(Path.Combine(sourcePath.PathParts.ToArray()), Path.Combine(destinationPath.PathParts.ToArray()));
    }

    public void FileDelete(IPath path)
    {
        File.Delete(Path.Combine(path.PathParts.ToArray()));
    }

    public void FileRename(IPath path, string name)
    {
        string? directory = Path.GetDirectoryName(Path.Combine(path.PathParts.ToArray()));
        if (directory == null)
            return;
        string newAbsolutePath = Path.Combine(directory, name);
        File.Move(Path.Combine(path.PathParts.ToArray()), newAbsolutePath);
    }

    public IPath? FindFileAbsolutePath(IPath currentPath, IPath path)
    {
        if (Path.Combine(path.PathParts.ToArray())
                .StartsWith(Path.Combine(_systemAbsolutePath.PathParts.ToArray()), StringComparison.OrdinalIgnoreCase)
            && File.Exists(Path.Combine(path.PathParts.ToArray())))
            return path;

        var absolutePath = new SystemAdaptivePath(currentPath, path);
        return File.Exists(Path.Combine(absolutePath.PathParts.ToArray()))
            ? absolutePath
            : null;
    }

    public IEnumerable<IPath> GetDirectories(IPath path)
    {
        string[] directoriesAbsolutePaths = Directory.GetDirectories(Path.Combine(path.PathParts.ToArray()));
        var directories = new List<IPath>();

        foreach (string directoryAbsolutePath in directoriesAbsolutePaths)
        {
            directories.Add(new SystemAdaptivePath(directoryAbsolutePath));
        }

        return directories;
    }

    public IEnumerable<IPath> GetFiles(IPath path)
    {
        string[] filesAbsolutePaths = Directory.GetFiles(Path.Combine(path.PathParts.ToArray()));
        var files = new List<IPath>();

        foreach (string fileAbsolutePath in filesAbsolutePaths)
        {
            files.Add(new SystemAdaptivePath(fileAbsolutePath));
        }

        return files;
    }

    public string? FindComponentName(IPath path)
    {
        return Path.GetFileName(Path.Combine(path.PathParts.ToArray()));
    }

    public IPath? FindDirectoryAbsolutePath(IPath currentPath, IPath path)
    {
        if (Path.Combine(path.PathParts.ToArray())
                .StartsWith(Path.Combine(_systemAbsolutePath.PathParts.ToArray()), StringComparison.OrdinalIgnoreCase)
            && Directory.Exists(Path.Combine(path.PathParts.ToArray())))
            return path;

        var absolutePath = new SystemAdaptivePath(currentPath, path);
        return Directory.Exists(Path.Combine(absolutePath.PathParts.ToArray()))
            ? absolutePath
            : null;
    }
}