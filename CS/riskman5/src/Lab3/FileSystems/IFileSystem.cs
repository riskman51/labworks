﻿using System.Collections.Generic;
using Itmo.ObjectOrientedProgramming.Lab4.FileSystems.FileSystemComponents;
using Itmo.ObjectOrientedProgramming.Lab4.Paths;

namespace Itmo.ObjectOrientedProgramming.Lab4.FileSystems;

public interface IFileSystem
{
    IFileSystemComponent? TreeList(int depth, IPath currenPath);
    IEnumerable<string> FileShow(IPath path);
    void FileMove(IPath sourcePath, IPath destinationPath);
    void FileCopy(IPath sourcePath, IPath destinationPath);
    void FileDelete(IPath path);
    void FileRename(IPath path, string name);
    IPath? FindDirectoryAbsolutePath(IPath currentPath, IPath path);
    IPath? FindFileAbsolutePath(IPath currentPath, IPath path);
    IEnumerable<IPath> GetDirectories(IPath path);
    IEnumerable<IPath> GetFiles(IPath path);
    string? FindComponentName(IPath path);
}