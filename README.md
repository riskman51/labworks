# Лабораторные работы

Привет! Я, Бабенко Сергей, ученик второго курса университета ИТМО,
направления Информационные системы. Этот репозиторий содержит
мои лабораторные работы за 3 и 4 семестр обучения и обновляется
по мере их выполнения.

## Объектно-ориентированное программирование на C#.
**Что проходили**:
1. Основы ООП
2. SOLID и GRASP
3. Порождающие паттерны
4. Структурные паттерны
5. Поведенческие паттерны
6. Архитектуры

**Лабораторные работы**:
1. [Конифигуратор. Порождаующие паттерны](CS/riskman5/src/Lab1/README.MD)
2. [Cистему распределения сообщений. Cтруктурные паттерны, mock-тестирование](CS/riskman5/src/Lab2/README.MD)
3. [Файловая система. Поведенческие паттерны](CS/riskman5/src/Lab3/README.MD)
4. [Банкомат. Луковая архитектура, Dependency Injection.](CS/riskman5/src/Lab4/README.MD)

## Технологии прогаммирования на Java
**Что проходили**:
1. Collection framework & Stream API
2. Системы сборок
3. Сборка мусора
4. Базы (JPA JDBC Hibernate)
5. Spring
6. Spring MVC
7. Spring Boot
8. Spring Data JPA
9. Spring Security
10. Spring AOP

**Лабораторные работы**:
1. [Банки. Знакомство с Java. IoC-контейнер](Java/riskman5/Lab1/README.MD)
2. [Сервис по учету котов и их владельцев. Hibernate](Java/riskman5/Cats/README.MD)
3. [К созданному в прошлой лабораторной сервису добавляется Spring.](Java/riskman5/Cats/README.MD)
4. [Добавление авторизации к сервису.](Java/riskman5/Cats/README.MD)
5. [Микросервисы. Брокеры сообщений.](Java/riskman5/Cats/README.MD)